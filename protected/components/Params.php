<?php
Class Params{
    const DATE_FORMAT = 'dd M yy';      //format default date untuk datepicker
    const TIME_FORMAT = 'H:i:s';        //format default time untuk datepicker
    const MONTH_FORMAT = 'M yy';        //format untuk monthpicker

    const TOOLTIP_PLACEMENT = 'bottom';                 //nilai konstanta tooltip placement untuk bootstrap tooltip
    const TOOLTIP_SELECTOR = 'a[rel="tooltip"],button[rel="tooltip"],input[rel="tooltip"]';        //nilai konstanta tooltip selector untuk bootstrap tooltip

    const ROLE_ADMIN = 1;
    const ROLE_OWNER = 2;
    const ROLE_GUDANG = 3;
    const ROLE_KASIR = 4;
    
    const DEFAULT_PROFIL_MARKET = 1;

    public static function menu()
    {
        if (Yii::app()->user->getState('role_id') == Params::ROLE_ADMIN) {
            return array(
                array(
                    'label'=>'Jam Kerja',
                    'url'=>Yii::app()->createUrl('shift/admin'),
                    'icon'=>'fa fa-clock-o',
                    'active'=>(Yii::app()->controller->id=='shift')?true:false
                ),
                array(
                    'label'=>'Posisi Pegawai',
                    'url'=>Yii::app()->createUrl('role/admin'),
                    'icon'=>'fa fa-list-ol',
                    'active'=>(Yii::app()->controller->id=='role')?true:false
                ),
                array(
                    'label'=>'Pegawai',
                    'url'=>Yii::app()->createUrl('employee/admin'),
                    'icon'=>'fa fa-user',
                    'active'=>(Yii::app()->controller->id=='employee')?true:false
                ),
                array(
                    'label'=>'Pengguna Sistem',
                    'url'=>Yii::app()->createUrl('user/admin'),
                    'icon'=>'fa fa-users',
                    'active'=>(Yii::app()->controller->id=='user')?true:false
                ),
                array(
                    'label'=>'Daftar Member',
                    'url'=>Yii::app()->createUrl('member/admin'),
                    'icon'=>'fa fa-users',
                    'active'=>((Yii::app()->controller->id=='member' && Yii::app()->controller->action->id=='admin') || (Yii::app()->controller->id=='member' && Yii::app()->controller->action->id=='create'))?true:false
                ),
                array(
                    'label'=>'Konfigurasi Sistem',
                    'url'=>Yii::app()->createUrl('systemConfiguration/update&id='.Params::DEFAULT_PROFIL_MARKET),
                    'icon'=>'fa fa-gear',
                    'active'=>(Yii::app()->controller->id=='systemConfiguration')?true:false
                ),
                array(
                    'label'=>'Laporan Presensi',
                    'url'=>Yii::app()->createUrl('presences/admin'),
                    'icon'=>'fa fa-calendar',
                    'active'=>(Yii::app()->controller->id=='presences')?true:false
                )
            );
        }

        if (Yii::app()->user->getState('role_id') == Params::ROLE_OWNER) {
            return array(
                array(
                    'label'=>'Statistik',
                    'url'=>Yii::app()->createUrl('product/dashboard'),
                    'icon'=>'fa fa-dashboard',
                    'active'=>(Yii::app()->controller->id=='product' && Yii::app()->controller->action->id=='dashboard')?true:false
                ),
                array(
                    'label'=>'Produk',
                    'url'=>Yii::app()->createUrl('product/admin'),
                    'icon'=>'fa fa-book',
                    'active'=>((Yii::app()->controller->id=='product' && Yii::app()->controller->action->id=='admin') || (Yii::app()->controller->id=='product' && Yii::app()->controller->action->id=='create'))?true:false
                ),
                array(
                    'label'=>'Daftar Member',
                    'url'=>Yii::app()->createUrl('member/admin'),
                    'icon'=>'fa fa-users',
                    'active'=>((Yii::app()->controller->id=='member' && Yii::app()->controller->action->id=='admin') || (Yii::app()->controller->id=='member' && Yii::app()->controller->action->id=='create'))?true:false
                ),
                array(
                    'label'=>'Laporan Penjualan',
                    'url'=>Yii::app()->createUrl('saleProducts/report'),
                    'icon'=>'fa fa-file-text',
                    'active'=>(Yii::app()->controller->id=='saleProducts' && Yii::app()->controller->action->id=='report')?true:false
                ),
                array(
                    'label'=>'Laporan Pembelian',
                    'url'=>Yii::app()->createUrl('orderProducts/report'),
                    'icon'=>'fa fa-file-text-o',
                    'active'=>(Yii::app()->controller->id=='orderProducts' && Yii::app()->controller->action->id=='report')?true:false
                ),
                array(
                    'label'=>'Laporan Presensi',
                    'url'=>Yii::app()->createUrl('presences/admin'),
                    'icon'=>'fa fa-calendar',
                    'active'=>(Yii::app()->controller->id=='presences')?true:false
                )
            );
        }

        if (Yii::app()->user->getState('role_id') == Params::ROLE_GUDANG) {
            return array(
                array(
                    'label'=>'Kategori Produk',
                    'url'=>Yii::app()->createUrl('category/admin'),
                    'icon'=>'fa fa-tags',
                    'active'=>(Yii::app()->controller->id=='category')?true:false
                ),
                array(
                    'label'=>'Produk',
                    'url'=>Yii::app()->createUrl('product/admin'),
                    'icon'=>'fa fa-book',
                    'active'=>(Yii::app()->controller->id=='product')?true:false
                ),
                array(
                    'label'=>'Daftar Member',
                    'url'=>Yii::app()->createUrl('member/admin'),
                    'icon'=>'fa fa-users',
                    'active'=>((Yii::app()->controller->id=='member' && Yii::app()->controller->action->id=='admin') || (Yii::app()->controller->id=='member' && Yii::app()->controller->action->id=='create'))?true:false
                ),
                array(
                    'label'=>'Transaksi Pembelian',
                    'url'=>Yii::app()->createUrl('orderProducts/index'),
                    'icon'=>'fa fa-shopping-cart',
                    'active'=>(Yii::app()->controller->id=='orderProducts' && Yii::app()->controller->action->id=='index')?true:false
                ),
                array(
                    'label'=>'Laporan Pembelian',
                    'url'=>Yii::app()->createUrl('orderProducts/report'),
                    'icon'=>'fa fa-file-text-o',
                    'active'=>(Yii::app()->controller->id=='orderProducts' && Yii::app()->controller->action->id=='report')?true:false
                ),
            );
        }

        if (Yii::app()->user->getState('role_id') == Params::ROLE_KASIR) {
            return array(
                array(
                    'label'=>'Daftar Member',
                    'url'=>Yii::app()->createUrl('member/admin'),
                    'icon'=>'fa fa-users',
                    'active'=>((Yii::app()->controller->id=='member' && Yii::app()->controller->action->id=='admin') || (Yii::app()->controller->id=='member' && Yii::app()->controller->action->id=='create'))?true:false
                ),
                array(
                    'label'=>'Transaksi Penjualan',
                    'url'=>Yii::app()->createUrl('saleProducts/index'),
                    'icon'=>'fa fa-shopping-cart',
                    'active'=>(Yii::app()->controller->id=='saleProducts' && Yii::app()->controller->action->id=='index')?true:false
                ),
                array(
                    'label'=>'Laporan Penjualan',
                    'url'=>Yii::app()->createUrl('saleProducts/report'),
                    'icon'=>'fa fa-file-text',
                    'active'=>(Yii::app()->controller->id=='saleProducts' && Yii::app()->controller->action->id=='report')?true:false
                ),
            );
        }
    }
}
?>