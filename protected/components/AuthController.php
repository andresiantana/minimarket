<?php
class AuthController extends CController
{
    protected function beforeAction($action)
    {
        if(!isset(Yii::app()->user->id)){
                if(Yii::app()->request->isAjaxRequest){
                        die('403, Anda tidak diperkenankan mengakses halaman ini! Silahkan hubungi administrator!');
                }else{
                        $this->redirect(array('/site/login'));
                }
        }
        if($this->checkAccess()){
                return true;
        }else{
                if(Yii::app()->request->isAjaxRequest){
                        $data['pesan'] = "Anda tidak diperkenankan melakukan aksi ini! Silahkan hubungi administrator (Kode: 403)!";
                        $data['sukses'] = 0;
                        echo json_encode($data);
                        Yii::app()->end();
                }else{
                        throw new CHttpException(403, 'Anda tidak diperkenankan mengakses halaman ini! Silahkan hubungi administrator!');
                }
                return false;
        }
    }
	/**
	 * untuk mengecek hak akses
	 * @param type array(action, controller, module)
	 * @return boolean
	 * @throws CHttpException
	 */
	public function checkAccess($params = array()){
		$id = Yii::app()->user->id;
		
		if(isset($params['id'])){
			$id = $params['id'];
                }
                $sql = "SELECT username
                       FROM user 
                       WHERE id = '".$id."'";
                $loadData = Yii::app()->db->createCommand($sql)->queryRow();
            if($loadData){
                return true;
            }else{
                return false;
            }   
                
	}
}

