<?php
Class MyGenerator{
    /**
     * Generate number untuk sales
     * @return string
     */
    public static function salesNumber()
    {
        $default = "0001";
        $prefix = "CHM".date("ymd");
        $sql = "SELECT COUNT(number) AS nomaksimal FROM sales WHERE number LIKE ('".$prefix."%')";
        $saleProducts= Yii::app()->db->createCommand($sql)->queryRow();
        $numberSale = $prefix.(isset($saleProducts['nomaksimal']) ? (str_pad($saleProducts['nomaksimal']+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $numberSale;
    }
    
    /**
     * Generate number untuk orders
     * @return string
     */
    public static function ordersNumber()
    {
        $default = "0001";
        $prefix = "FAKTUR".date("ymd");
        $sql = "SELECT COUNT(number) AS nomaksimal FROM orders WHERE number LIKE ('".$prefix."%')";
        $orderProducts= Yii::app()->db->createCommand($sql)->queryRow();
        $numberOrder = $prefix.(isset($orderProducts['nomaksimal']) ? (str_pad($orderProducts['nomaksimal']+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $numberOrder;
    }
    
    /**
     * Generate number untuk kode nik pegawai
     * @return string
     */
    public static function employeeCode()
    {
        $default = "01";
        $prefix = date("Ym");
        $sql = "SELECT COUNT(code) AS nomaksimal FROM employees WHERE code LIKE ('".$prefix."%')";
        $employee = Yii::app()->db->createCommand($sql)->queryRow();
        $employeeCode = $prefix.(isset($employee['nomaksimal']) ? (str_pad($employee['nomaksimal']+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $employeeCode;
    }

    /**
     * Generate number untuk members
     * @return string
     */
    public static function memberNumbers()
    {
        $default = "0001";
        $prefix = date("Ymd");
        $sql = "SELECT COUNT(number) AS nomaksimal FROM members WHERE number LIKE ('".$prefix."%')";
        $members= Yii::app()->db->createCommand($sql)->queryRow();
        $numberMember = $prefix.(isset($members['nomaksimal']) ? (str_pad($members['nomaksimal']+1, strlen($default), 0,STR_PAD_LEFT)) : $default);
        return $numberMember;
    }
}

