<?php

class UserController extends Controller
{
    /**
    * @var string the default layout for the views. Defaults to '//layouts/default', meaning
    * using two-column layout. See 'protected/views/layouts/default.php'.
    */
    public $layout='//layouts/default';
    public $defaultAction='Admin';
    public $accordionIndex = 0;
    

    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
    public function actionCreate()
    {
        $model=new User;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            $model->password = $_POST['User']['password'];
            $model->create_time = date('Y-m-d H:i:s');
            $model->create_user_id = Yii::app()->user->id;
            $model->setScenario('insert');
            
            $criteria = new CDbCriteria();
            $criteria->compare('LOWER(username)',strtolower($model->username),true);
            $criteria->addCondition('is_active = 1');
            $criteria->addCondition('is_delete = 0');
            $modUser = User::model()->findAll($criteria);
            if(count($modUser) > 0){
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Pengguna Sistem dengan Username <b>'.$model->username.'</b> sudah ada pada database.');
                $this->redirect(array('create'));
            }else{
                if($model->save()){
                    $modPegawai = Employee::model()->updateByPk($model->employee_id,array('is_have_user'=>1));
                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                    $this->redirect(array('admin','id'=>$model->id));
                }else{
                    Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');
                }
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
    * Updates a particular model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id the ID of the model to be updated
    */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $is_available = true;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['User']))
        {
            if(strtolower($model->username) == strtolower($_POST['User']['username'])){
                $is_available = false;
            }
            $model->attributes=$_POST['User'];            
            $model->update_time = date('Y-m-d H:i:s');
            $model->update_user_id = Yii::app()->user->id;
            $model->password = $model->password;
            $model->setScenario('changePassword');
            
            if($is_available == true){
                $criteria = new CDbCriteria();
                $criteria->compare('LOWER(username)',strtolower($_POST['User']['username']),true);
                $criteria->addCondition('is_active = 1');
                $criteria->addCondition('is_delete = 0');
                $modUser = User::model()->findAll($criteria);
                if(count($modUser) > 0){
                    Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Pengguna Sistem dengan Username <b>'.$model->username.'</b> sudah ada pada database.');
                    $this->redirect(array('update','id'=>$model->id));
                }
            }else{
                if($model->save()){
                    $modPegawai = Employee::model()->updateByPk($model->employee_id,array('is_have_user'=>1));
                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                    $this->redirect(array('admin','id'=>$model->id));
                }else{
                    Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');
                }
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
    * Deletes a particular model.
    * If deletion is successful, the browser will be redirected to the 'admin' page.
    * @param integer $id the ID of the model to be deleted
    */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }
    
    /**
    * NonActives a particular model.
    * If deletion is successful, the browser will be redirected to the 'admin' page.
    * @param integer $id the ID of the model to be deleted
    */
    public function actionNonActive($id)
    {
        $model = $this->loadModel($id);
        $model->is_delete = 1;
        if($model->save()){
            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil dihapus.');
            $this->redirect(array('admin'));
        }else{
            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal dihapus.');
            $this->redirect(array('admin'));
        }
    }
    
    /**
    * Lists all models.
    */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('User');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
    * Manages all models.
    */
    public function actionAdmin()
    {
        $model=new User('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['User'])){
            $model->attributes=$_GET['User'];
            $model->role_name = isset($_GET['User']['role_name']) ? $_GET['User']['role_name'] : null;
            $model->employee_name = isset($_GET['User']['employee_name']) ? $_GET['User']['employee_name'] : null;
            $model->is_active = isset($_GET['User']['is_active']) ? $_GET['User']['is_active'] : null;
        }

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    * @param integer the ID of the model to be loaded
    */
    public function loadModel($id)
    {
        $model=User::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    /**
     * untuk mengambil nama role
     */
    public function actionSetRoleName()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $role_id = isset($_POST['role_id']) ? $_POST['role_id'] : null;
            $modRole = Role::model()->findByPk($role_id);
            if(isset($modRole)){
                $data['name'] = $modRole->name;
            }   
            echo json_encode($data);
            Yii::app()->end();
        }
    }
}
