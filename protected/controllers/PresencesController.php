<?php

class PresencesController extends Controller
{
    /**
    * @var string the default layout for the views. Defaults to '//layouts/default', meaning
    * using two-column layout. See 'protected/views/layouts/default.php'.
    */
    public $layout='//layouts/default';
    public $defaultAction='Admin';
    public $saleproductsave = false;
    public $saleproductDetailsave = true; // for looping
    public $accordionIndex = 0;
    
    
    public function actionAdmin()
    {
        $format = new Formatter();
        $model=new Presence('searchReport');
        $model->start_date = date('Y-m-d');
        $model->end_date = date('Y-m-d');
        $model->unsetAttributes();  // clear any default values
        
        if(isset($_GET['Presence'])){
            $model->attributes=$_GET['Presence'];
            $model->start_date = isset($_GET['Presence']['start_date']) ? $format->formatDateTimeForDb($_GET['Presence']['start_date']) : null;
            $model->end_date = isset($_GET['Presence']['end_date']) ? $format->formatDateTimeForDb($_GET['Presence']['end_date']) : null;
            $model->employee_id = isset($_GET['Presence']['employee_id']) ? $_GET['Presence']['employee_id'] : null;
        }

        $this->render('admin',array(
            'model'=>$model,
            'format'=>$format
        ));
    }
    
    public function actionPrintReport() 
    {
        $format = new Formatter;
        $model = new Presence('searchReportPrint');
        $model->start_date = date("d M Y");
        $model->end_date = date("d M Y");

        if(isset($_REQUEST['Presence']))
        {
            $model->attributes = $_REQUEST['Presence'];
            $model->start_date = $format->formatDateTimeForDb($_REQUEST['Presence']['start_date']);
            $model->end_date = $format->formatDateTimeForDb($_REQUEST['Presence']['end_date']);
            $model->employee_id = isset($_REQUEST['Presence']['employee_id']) ? $_REQUEST['Presence']['employee_id'] : null;
            
            $periode = $format->formatDateTimeForUser($model->start_date).' s/d '.$format->formatDateTimeForUser($model->end_date);            
        }
        $judulLaporan = 'Laporan Presensi Pegawai';
        $caraPrint = isset($_REQUEST['caraPrint']) ? $_REQUEST['caraPrint'] : null;
        $target = 'Print';

        $this->printFunction($model, $periode, $caraPrint, $judulLaporan, $target);

    } 
    
    protected function printFunction($model, $periode, $caraPrint, $judulLaporan, $target){
        $format = new Formatter();
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
}