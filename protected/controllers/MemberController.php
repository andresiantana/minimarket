<?php

class MemberController extends Controller
{
    /**
    * @var string the default layout for the views. Defaults to '//layouts/default', meaning
    * using two-column layout. See 'protected/views/layouts/default.php'.
    */
    public $layout='//layouts/default';
    public $defaultAction='Admin';

    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
    public function actionCreate()
    {
        $model=new Member;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Member']))
        {
            $model->attributes=$_POST['Member'];
            if($model->save()){
                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                $this->redirect(array('view','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
    * Updates a particular model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id the ID of the model to be updated
    */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Member']))
        {
            $model->attributes=$_POST['Member'];
            if($model->save()){
                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                $this->redirect(array('view','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
    * Deletes a particular model.
    * If deletion is successful, the browser will be redirected to the 'admin' page.
    * @param integer $id the ID of the model to be deleted
    */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }
    
    /**
    * NonActives a particular model.
    * If deletion is successful, the browser will be redirected to the 'admin' page.
    * @param integer $id the ID of the model to be deleted
    */
    public function actionNonActive($id)
    {
        $model = $this->loadModel($id);
        $model->is_delete = 1;
        if($model->save()){
            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil dihapus.');
            $this->redirect(array('admin'));
        }else{
            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal dihapus.');
            $this->redirect(array('admin'));
        }
    }
    
    /**
    * Lists all models.
    */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Member');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
    * Manages all models.
    */
    public function actionAdmin()
    {
        $model=new Member('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Member']))
        $model->attributes=$_GET['Member'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    * @param integer the ID of the model to be loaded
    */
    public function loadModel($id)
    {
        $model=Member::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='member-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
