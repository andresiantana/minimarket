<?php

class SaleProductsController extends Controller
{
    /**
    * @var string the default layout for the views. Defaults to '//layouts/default', meaning
    * using two-column layout. See 'protected/views/layouts/default.php'.
    */
    public $layout='//layouts/default';
    public $defaultAction='Index';
    public $saleproductsave = false;
    public $saleproductDetailsave = true; // for looping
    public $accordionIndex = 0;
    
    /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
    public function actionIndex($id = null)
    {
        $format = new Formatter();
        $modMember = new Member();
        $modMember->number = MyGenerator::memberNumbers();
        $model=new Sale();
        $model->number = MyGenerator::salesNumber();
        $model->date = date('Y-m-d H:i:s');
        $modDetails = array();        
        $modKonfig = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET);
        if(!empty($id)){
            $model= Sale::model()->findByPk($id);
            $modDetails = SaleDetails::model()->findAllByAttributes(array('sale_id'=>$model->id));
        }
        if(isset($_POST['Sale'])){
             $transaction = Yii::app()->db->beginTransaction();
             try {
                    $model->attributes=$_POST['Sale'];
                    $model->number= MyGenerator::salesNumber();
                    $model->date= isset($model->date) ? $format->formatDateTimeForDb($model->date) : null;
                    $model->create_time = date('Y-m-d H:i:s');
                    $model->create_user_id = Yii::app()->user->id;
                    $model->employee_id = Yii::app()->user->getState('employee_id');
                    $model->role_id = Yii::app()->user->getState('role_id');
                    $model->shift_id = Yii::app()->user->getState('shift_id');
                                      
                    if($model->save()){
                        if(!empty($model->member_id)){
                            $member = Member::model()->findByPk($model->member_id);
                            $out_point = isset($_POST['Member']['out_point']) ? $_POST['Member']['out_point'] : 0;
                            $sub_total = isset($model->sub_total) ? $model->sub_total : 0;
                            $in_point = floor($sub_total / $modKonfig->point_to_idr);
                            if($in_point < 0){
                                $in_point = 0;
                            }
                            $member->in_point = $member->in_point + $in_point;
                            $member->out_point = $member->out_point + $out_point;
                            $member->current_point = $member->in_point - $member->out_point;
                            $member->update();
                        }
                        $this->saleproductsave = true;
                        if(count($_POST['SaleDetails']) > 0){
                            foreach($_POST['SaleDetails'] AS $i => $postProduct){
                                $modDetails[$i] = $this->simpanDetailProducts($model,$postProduct);
                            }
                        }
                    }
                    
                    if($this->saleproductsave && $this->saleproductDetailsave){                        
                        $transaction->commit();
                        $model->isNewRecord = FALSE;
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Transaksi Penjualan berhasil disimpan.');
                        $this->redirect(array('index','id'=>$model->id,'sukses'=>1));
                    }else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Transaksi Penjualan gagal disimpan.');
                    }

            }catch(Exception $exc){
                $model->isNewRecord = true;
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
            }    
        }

        $this->render('index',array(
            'model'=>$model,
            'modDetails'=>$modDetails,
            'format'=>$format,
            'modMember'=>$modMember,
        ));
    }
    
    /**
     * simpan SaleDetails
     * @param type $model
     * @param type $post
     */
    public function simpanDetailProducts($model ,$post){
        $format = new Formatter();
        $modDetails = new SaleDetails;
        $modProduct = Product::model()->findByPk($post['product_id']);
        
        $modDetails->attributes = $post;
        $modDetails->sale_id = $model->id;
        $modDetails->create_time = date('Y-m-d H:i:s');
        $modDetails->create_user_id = Yii::app()->user->id;
        $modDetails->product_name = $modProduct->name;
        $modDetails->product_barcode = $modProduct->barcode;
        $modDetails->product_unit = $modProduct->unit;
        
        if($modDetails->validate()) {
            $modDetails->save();            
            $stok = $modProduct->stock;
            if($modDetails->quantity > 0){
                $modProduct->stock = $stok - $modDetails->quantity;
            }            
            $modProduct->save();
            $this->saleproductDetailsave &= true;
        } else {
            $this->saleproductDetailsave  &= false;
        }
        return $modDetails;
    }
    
    public function actionReport()
    {
        $format = new Formatter();
        $model=new Sale('searchReport');
        $model->start_date = date('Y-m-d');
        $model->end_date = date('Y-m-d');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Sale'])){
            $model->attributes=$_GET['Sale'];
            $model->start_date = isset($_GET['Sale']['start_date']) ? $format->formatDateTimeForDb($_GET['Sale']['start_date']) : null;
            $model->end_date = isset($_GET['Sale']['end_date']) ? $format->formatDateTimeForDb($_GET['Sale']['end_date']) : null;
        }

        $this->render('report',array(
            'model'=>$model,
            'format'=>$format
        ));
    }

    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    * @param integer the ID of the model to be loaded
    */
    public function loadModel($id)
    {
        $model=Sale::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='sale-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function actionAutocompleteProduct()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $name = isset($_GET['name']) ? $_GET['name'] : null;
            $barcode = isset($_GET['barcode']) ? $_GET['barcode'] : null;
            
            $criteria = new CDbCriteria();
            if(!empty($name)){
                $criteria->compare('LOWER(name)', strtolower($name), true);
            }
            if(!empty($barcode)){
                $criteria->compare('LOWER(barcode)', strtolower($barcode), true);
            }
            $criteria->order = 'name ASC';
            $criteria->limit = 5;
            $models = Product::model()->findAll($criteria);
            $returnVal=array();
            foreach($models as $i=>$model)
            {
                $attributes = $model->attributeNames();
                foreach($attributes as $j=>$attribute) {
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->name;
                $returnVal[$i]['value'] = $model->id;
            }

            echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }
    
    /**
    * menampilkan product
    * @return row table 
    */
    public function actionLoadFormSaleProducts()
    {
        if(Yii::app()->request->isAjaxRequest) { 
            $id = isset($_POST['id']) ? $_POST['id'] : null;
            $quantity = isset($_POST['quantity']) ? $_POST['quantity'] : 1;
            $barcode = isset($_POST['barcode']) ? $_POST['barcode'] : null;
            
            $pesan = '';
            $form = '';
            $format = new Formatter();
            $model = new Sale();
            $modDetails = new SaleDetails();
            $criteria = new CDbCriteria();
            if(!empty($id)){
                $criteria->addCondition('id ='.$id);
            }
            if(!empty($barcode)){
                $criteria->compare('LOWER(barcode)',strtolower($barcode),true);
            }
            $modProduct = Product::model()->find($criteria);
            if(!empty($modProduct)) {
                $stok = $modProduct->stock;            
                if($stok >= $quantity){
                    $modDetails->quantity = $quantity;
                    $modDetails->product_id = $modProduct->id;
                    $modDetails->barcode = $modProduct->barcode;
                    $modDetails->prices_sale = $modProduct->price_sale;
                    $modDetails->name = $modProduct->name;
                    $modDetails->discount_ori = $modProduct->discount;
                    $modDetails->discount = $modProduct->discount;
                    $modDetails->min_purchase = $modProduct->min_purchase;
                    $modDetails->total_discount = 0;
                    $modDetails->sub_total = 0;

                    $form = $this->renderPartial('_rowProductDetails', array(
                            'model'=>$model,
                            'modDetails'=>$modDetails,
                            'modProduct'=>$modProduct,
                        ),true);
                }else{
                    $pesan = 'Stok produk tidak mencukupi.';
                }
            }else{
                $pesan = 'produk tidak ditemukan pada database, silahkan menghubungi bagian Gudang.';
            }

            echo CJSON::encode(array(
                'status'=>'create_form', 
                'pesan'=>$pesan,
                'form'=>$form ,
            ));
            exit;  
        }
    }
    
    /**
    * @param type $id
    */
    public function actionPrintStruk($id) 
    {
        $this->layout='//layouts/printWindows';
        $format = new Formatter();
        $model = Sale::model()->findByPk($id);
        $modDetail = SaleDetails::model()->findAllByAttributes(array('sale_id'=>$model->id));
        $this->render('printStruk', array(
            'format'=>$format,
            'model'=>$model,
            'modDetail'=>$modDetail,
        ));
    }
    
    public function actionPrintStrukTerakhir() 
    {
        $this->layout='//layouts/printWindows';
        $format = new Formatter();
        // $search = new CDbCriteria();
        // $search->order = 'id DESC';
        // $search->limit = 1;
        // $model = Sale::model()->find($search);
        $model = Sale::model()->findByAttributes(
            array(
                'create_user_id' => Yii::app()->user->id
            ),
            array(
                'order' => 'id DESC',
                'limit' => 1
            ));
        $modDetail = SaleDetails::model()->findAllByAttributes(
            array(
                'sale_id'=> $model->id
            )
        );
        $this->render('printStruk', array(
            'format'=>$format,
            'model'=>$model,
            'modDetail'=>$modDetail,
        ));
    }

    public function actionDetailPenjualan($id,$caraPrint = null){
        $this->layout = '//layouts/iframe';
        $model = Sale::model()->findByPk($id);
        $modDetails = SaleDetails::model()->findAllByAttributes(array('sale_id'=>$id));
        $judulLaporan='Detail Penjualan';
        $caraPrint = isset($_GET['caraPrint']) ? $_GET['caraPrint'] : null;
        if($caraPrint == 'PRINT'){
            $this->layout = '//layouts/printWindows';
        }
        $this->render('PrintDetail',array('model'=>$model, 'modDetails'=>$modDetails, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
    }
    
    public function actionPrintReport() 
    {
        $format = new Formatter;
        $model = new Sale;
        $model->start_date = date("d M Y");
        $model->end_date = date("d M Y");

        if(isset($_REQUEST['Sale']))
        {
            $model->attributes = $_REQUEST['Sale'];
            $model->start_date = $format->formatDateTimeForDb($_REQUEST['Sale']['start_date']);
            $model->end_date = $format->formatDateTimeForDb($_REQUEST['Sale']['end_date']);
            $model->shift_id = isset($_REQUEST['Sale']['shift_id']) ? $_REQUEST['Sale']['shift_id'] : null;
            $model->employee_id = isset($_REQUEST['Sale']['employee_id']) ? $_REQUEST['Sale']['employee_id'] : null;
            $model->number = isset($_REQUEST['Sale']['number']) ? $_REQUEST['Sale']['number'] : null;
            
            $periode = $format->formatDateTimeForUser($model->start_date).' s/d '.$format->formatDateTimeForUser($model->end_date);
            
            $criteria = new CDbCriteria();
            $criteria->addBetweenCondition('DATE(date)', $model->start_date, $model->end_date,true);
            $criteria->compare('LOWER(date)',strtolower($model->date),true);
            $criteria->compare('LOWER(number)',strtolower($model->number),true);
            if(!empty($model->shift_id)){
                $criteria->addCondition('shift_id = '.$model->shift_id);
            }
            if(!empty($model->employee_id)){
                $criteria->addCondition('employee_id = '.$model->employee_id);
            }
            if(Yii::app()->user->role_id != Params::ROLE_OWNER){
                $criteria->compare('role_id',Yii::app()->user->role_id);
                //$criteria->compare('create_user_id',Yii::app()->user->id);
            }
            $model = Sale::model()->findAll($criteria);
        }
        $judulLaporan = 'Laporan Penjualan Produk';
        $caraPrint = isset($_REQUEST['caraPrint']) ? $_REQUEST['caraPrint'] : null;
        $target = 'PrintReport';

        $this->printFunction($model, $periode, $caraPrint, $judulLaporan, $target);

    } 
    
    public function actionPrintReportTotal() 
    {
        $format = new Formatter;
        $model = new Sale;
		$modelOrder = new Order();
		
        $model->start_date = date("d M Y");
        $model->end_date = date("d M Y");
		
		$modelOrder->start_date = date("d M Y");
        $modelOrder->end_date = date("d M Y");

        if(isset($_REQUEST['Sale']))
        {
            $model->attributes = $_REQUEST['Sale'];
            $model->start_date = $format->formatDateTimeForDb($_REQUEST['Sale']['start_date']);
            $model->end_date = $format->formatDateTimeForDb($_REQUEST['Sale']['end_date']);
            $model->shift_id = isset($_REQUEST['Sale']['shift_id']) ? $_REQUEST['Sale']['shift_id'] : null;
            $model->employee_id = isset($_REQUEST['Sale']['employee_id']) ? $_REQUEST['Sale']['employee_id'] : null;
            $model->number = isset($_REQUEST['Sale']['number']) ? $_REQUEST['Sale']['number'] : null;
            
            $periode = $format->formatDateTimeForUser($model->start_date).' s/d '.$format->formatDateTimeForUser($model->end_date);
            
            $criteria = new CDbCriteria();
            $criteria->addBetweenCondition('DATE(date)', $model->start_date, $model->end_date,true);
            $criteria->compare('LOWER(date)',strtolower($model->date),true);
            $criteria->compare('LOWER(number)',strtolower($model->number),true);
            if(!empty($model->shift_id)){
                $criteria->addCondition('shift_id = '.$model->shift_id);
            }
            if(!empty($model->employee_id)){
                $criteria->addCondition('employee_id = '.$model->employee_id);
            }
            if(Yii::app()->user->role_id != Params::ROLE_OWNER){
                $criteria->compare('role_id',Yii::app()->user->role_id);
            }
            $model = Sale::model()->findAll($criteria);
			
			$modelOrder->start_date = $format->formatDateTimeForDb($_REQUEST['Sale']['start_date']);
            $modelOrder->end_date = $format->formatDateTimeForDb($_REQUEST['Sale']['end_date']);
            $modelOrder->shift_id = isset($_REQUEST['Sale']['shift_id']) ? $_REQUEST['Sale']['shift_id'] : null;
			
            $criteria_order = new CDbCriteria();
			$criteria_order->select = 'sum(sub_total) as sub_total';
            $criteria_order->addBetweenCondition('DATE(date)', $modelOrder->start_date, $modelOrder->end_date,true);
            if(!empty($model->shift_id)){
                $criteria->addCondition('shift_id = '.$modelOrder->shift_id);
            }
            if(Yii::app()->user->role_id != Params::ROLE_OWNER){
                $criteria->compare('role_id',Yii::app()->user->role_id);
            }
			
			$modelOrder = Order::model()->find($criteria_order);
        }
        $judulLaporan = 'Laporan Total Penjualan';
        $caraPrint = isset($_REQUEST['caraPrint']) ? $_REQUEST['caraPrint'] : null;
        $target = 'PrintReportTotal';

        $this->printFunctionTotal($model, $modelOrder, $periode, $caraPrint, $judulLaporan, $target);

    } 

    public function actionPrintReportTotalHari() 
    {
        $format = new Formatter;
        $model = new Sale;
		$modelOrder = new Order;
        $model->start_date = date("d M Y");
        $model->end_date = date("d M Y");

        if(isset($_REQUEST['Sale']))
        {
            $model->attributes = $_REQUEST['Sale'];
            $model->start_date = $format->formatDateTimeForDb($_REQUEST['Sale']['start_date']);
            $model->end_date = $format->formatDateTimeForDb($_REQUEST['Sale']['end_date']);
            $model->shift_id = isset($_REQUEST['Sale']['shift_id']) ? $_REQUEST['Sale']['shift_id'] : null;
            $model->employee_id = isset($_REQUEST['Sale']['employee_id']) ? $_REQUEST['Sale']['employee_id'] : null;
            $model->number = isset($_REQUEST['Sale']['number']) ? $_REQUEST['Sale']['number'] : null;
            
            $periode = $format->formatDateTimeForUser($model->start_date).' s/d '.$format->formatDateTimeForUser($model->end_date);
            
            $criteria = new CDbCriteria();
            $criteria->select = 'DATE(date) as date, sum(sub_total) as sub_total, sum(total_discount) as total_discount';
            $criteria->group = 'DATE(date)';
            $criteria->addBetweenCondition('DATE(date)', $model->start_date, $model->end_date,true);
            $criteria->compare('LOWER(date)',strtolower($model->date),true);
            $criteria->compare('LOWER(number)',strtolower($model->number),true);
            if(!empty($model->shift_id)){
                $criteria->addCondition('shift_id = '.$model->shift_id);
            }
            if(!empty($model->employee_id)){
                $criteria->addCondition('employee_id = '.$model->employee_id);
            }
            if(Yii::app()->user->role_id != Params::ROLE_OWNER){
                $criteria->compare('role_id',Yii::app()->user->role_id);
            }
            $model = Sale::model()->findAll($criteria);
			
			$modelOrder->start_date = $format->formatDateTimeForDb($_REQUEST['Sale']['start_date']);
            $modelOrder->end_date = $format->formatDateTimeForDb($_REQUEST['Sale']['end_date']);
            $modelOrder->shift_id = isset($_REQUEST['Sale']['shift_id']) ? $_REQUEST['Sale']['shift_id'] : null;
			
            $criteria_order = new CDbCriteria();
			$criteria_order->select = 'sum(sub_total) as sub_total';
            $criteria_order->addBetweenCondition('DATE(date)', $modelOrder->start_date, $modelOrder->end_date,true);
            if(!empty($model->shift_id)){
                $criteria->addCondition('shift_id = '.$modelOrder->shift_id);
            }
            if(Yii::app()->user->role_id != Params::ROLE_OWNER){
                $criteria->compare('role_id',Yii::app()->user->role_id);
            }
			
			$modelOrder = Order::model()->find($criteria_order);
        }
        $judulLaporan = 'Laporan Total Penjualan Per Hari';
        $caraPrint = isset($_REQUEST['caraPrint']) ? $_REQUEST['caraPrint'] : null;
        $target = 'PrintReportTotalHari';

        $this->printFunctionTotal($model, $modelOrder, $periode, $caraPrint, $judulLaporan, $target);

    }

    public function actionPrintPerKategori() 
    {
        $format = new Formatter;
        $model = new Sale;
        $model->start_date = date("d M Y");
        $model->end_date = date("d M Y");

        if(isset($_REQUEST['Sale']))
        {
            $model->attributes = $_REQUEST['Sale'];
            $model->start_date = $format->formatDateTimeForDb($_REQUEST['Sale']['start_date']);
            $model->end_date = $format->formatDateTimeForDb($_REQUEST['Sale']['end_date']);
            $model->shift_id = isset($_REQUEST['Sale']['shift_id']) ? $_REQUEST['Sale']['shift_id'] : null;
            $model->employee_id = isset($_REQUEST['Sale']['employee_id']) ? $_REQUEST['Sale']['employee_id'] : null;
            $model->number = isset($_REQUEST['Sale']['number']) ? $_REQUEST['Sale']['number'] : null;
            
            $periode = $format->formatDateTimeForUser($model->start_date).' s/d '.$format->formatDateTimeForUser($model->end_date);
            
            $criteria = new CDbCriteria();
            $criteria->select = "t.*, sales.number, sales.date";
            $criteria->join = 'JOIN sales ON sales.id = t.sale_id
                                JOIN products ON products.id = t.product_id';
            $criteria->addBetweenCondition('DATE(sales.date)', $model->start_date, $model->end_date,true);
            $criteria->compare('LOWER(sales.date)',strtolower($model->date),true);
            $criteria->compare('LOWER(sales.number)',strtolower($model->number),true);
            if(!empty($model->shift_id)){
                $criteria->addCondition('sales.shift_id = '.$model->shift_id);
            }
            if(!empty($model->employee_id)){
                $criteria->addCondition('sales.employee_id = '.$model->employee_id);
            }
            if(Yii::app()->user->role_id != Params::ROLE_OWNER){
                $criteria->compare('sales.role_id',Yii::app()->user->role_id);
            }
            $criteria->order ='products.category_id ASC, products.name ASC, sales.date ASC';
            $modelOrder = SaleDetails::model()->findAll($criteria);
        }
        $judulLaporan = 'Laporan Penjualan Per Kategori';
        $caraPrint = isset($_REQUEST['caraPrint']) ? $_REQUEST['caraPrint'] : null;
        $target = 'PrintReportPerKategori';

        $this->printFunctionTotal($model, $modelOrder, $periode, $caraPrint, $judulLaporan, $target);

    }

    protected function printFunction($model, $periode, $caraPrint, $judulLaporan, $target){
        $format = new Formatter();
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
	
	protected function printFunctionTotal($model, $modelOrder, $periode, $caraPrint, $judulLaporan, $target){
        $format = new Formatter();
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'modelOrder' => $modelOrder, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'modelOrder' => $modelOrder, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'modelOrder' => $modelOrder, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
	
    public function actionAutocompleteMember()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $no_member = isset($_GET['no_member']) ? $_GET['no_member'] : null;
            
            $criteria = new CDbCriteria();
            if(!empty($no_member)){
                $criteria->compare('LOWER(number)', strtolower($no_member), true);
            }
            $criteria->order = 'number ASC';
            $criteria->limit = 5;
            $models = Member::model()->findAll($criteria);
            $returnVal=array();
            foreach($models as $i=>$model)
            {
                $attributes = $model->attributeNames();
                foreach($attributes as $j=>$attribute) {
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->number."-".$model->name;
                $returnVal[$i]['value'] = $model->number;
                $returnVal[$i]['id'] = $model->id;
                $returnVal[$i]['in_point'] = $model->in_point;
                $returnVal[$i]['out_point'] = $model->out_point;
                $returnVal[$i]['current_point'] = $model->current_point;
            }

            echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }

    public function actionSimpanMember()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $pesan = '';

            $number = isset($_POST['number']) ? $_POST['number'] : null;
            $name = isset($_POST['nama_member']) ? $_POST['nama_member'] : null;
            $address = isset($_POST['address']) ? $_POST['address'] : null;
            $telp_handphone = isset($_POST['telp_handphone']) ? $_POST['telp_handphone'] : null;
            $date = date('Y-m-d');

            $model = new Member();
            $model->number = $number;
            $model->name = $name;
            $model->address = $address;
            $model->phone_handphone = $telp_handphone;
            $model->date = $date;

            if($model->validate()){
                if($model->save()){
                    $pesan = 'Member berhasil di daftarkan';
                }else{
                    $pesan = 'Member gagal di daftarkan';
                }
            }else{
                $pesan = 'Member gagal di daftarkan';
            }
            $data['pesan'] = $pesan;

            echo CJSON::encode($data);
        }
        Yii::app()->end();
    }
}
