<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
            return array(
                    // captcha action renders the CAPTCHA image displayed on the contact page
                    'captcha'=>array(
                            'class'=>'CCaptchaAction',
                            'backColor'=>0xFFFFFF,
                    ),
                    // page action renders "static" pages stored under 'protected/views/site/pages'
                    // They can be accessed via: index.php?r=site/page&view=FileName
                    'page'=>array(
                            'class'=>'CViewAction',
                    ),
            );
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $modKonfig = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET);
        if (isset($modKonfig)) {
            if ($modKonfig->is_absensi == 1 && $modKonfig->is_active == 1) {
                $this->redirect(Yii::app()->baseUrl.'/index.php?r=site/menu');
            } else {
                $this->redirect(Yii::app()->baseUrl.'/index.php?r=site/login');
            }
        } else {
            $this->redirect(Yii::app()->baseUrl.'/index.php?r=site/login');
        }   
	}
        
    public function actionMenu()
    {
        $this->layout = '//layouts/menu';
        
        $this->render('menu',array());
    }
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
            if($error=Yii::app()->errorHandler->error)
            {
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{           
            $model=new LoginForm;
            $this->layout = '//layouts/login';
            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }

            // collect user input data
            if (isset($_POST['LoginForm'])) {
                $model->attributes=$_POST['LoginForm'];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login()) {
                    if (Yii::app()->user->getState('role_id') == Params::ROLE_ADMIN) {
                        $url = 'user/admin';
//                        $this->redirect('index.php?r=user/admin');
                    } elseif (Yii::app()->user->getState('role_id') == Params::ROLE_GUDANG) {
                        $url = 'product/admin';
//                         $this->redirect('index.php?r=product/admin');
                    } elseif (Yii::app()->user->getState('role_id') == Params::ROLE_KASIR) {
                        $url = 'saleProducts/index';
//                        $this->redirect('index.php?r=saleProducts/index');
                    } elseif (Yii::app()->user->getState('role_id') == Params::ROLE_OWNER) {
                        $url = 'product/dashboard';
//                        $this->redirect('index.php?r=product/dashboard');
                    }                    
                    if(isset($_GET['frame']) && $_GET['frame'] == 1){
                        echo "<script>
                            window.top.location.href='".Yii::app()->createUrl($url)."';
                            </script>";
                    }else{
                        echo "<script>
                            window.location.href='".Yii::app()->createUrl($url)."';
                            </script>"; 
                    }
                }
            }
            // display the login form
            $this->render('login',array('model'=>$model));
	}

    /**
	 * Displays the update user
	 */
	public function actionUpdatePassword($id = null)
	{
            $this->layout = '//layouts/default';
            $model = User::model()->findByPk($id);
            $prevUrl = Yii::app()->request->getUrlReferrer();
            $format = new Formatter();
            if(isset ($_POST['User'])){
                $model->attributes=$_POST['User'];
                $model->old_password = $_POST['User']['old_password'];
                $model->new_password = $_POST['User']['new_password'];
                $model->last_login = date('Y-m-d');
                $model->update_time = date('Y-m-d');
                $model->update_user_id = Yii::app()->user->id;
               
                if ($model->validate())
                {
                    if ($model->new_password !== '' && $model->old_password !=='')
                    {
                        if($model->password == md5($model->old_password)){
                            $model->password = md5($model->new_password);
                        }else{
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Kata sandi yang anda inputkan tidak sesuai dengan database.');
                            $this->redirect(array('updatePassword','id'=>$model->id));
                        }
                     }
                    if($model->update())
                    {
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Kata sandi berhasil disimpan.');
                        if ($model->role_id == Params::ROLE_GUDANG) {
                            $this->redirect(Yii::app()->createUrl('/product/admin'));
                        } else if ($model->role_id == Params::ROLE_KASIR) {
                            $this->redirect(Yii::app()->createUrl('/saleProducts/index'));
                        } else if ($model->role_id == Params::ROLE_OWNER) {
                            $this->redirect(Yii::app()->createUrl('/product/dashboard'));
                        }
                    }
                    else
                    {
                        Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data Gagal Disimpan.');
                         $this->redirect(array('updatePasswod','id'=>$model->id));
                    }
                }
            }
                $this->render('updatePassword',array(
                    'model'=>$model,'prevUrl'=>$prevUrl,
            ));
	}
        
        /**
         *  viewUser
         */
        public function actionViewUser($id = null){
            $this->layout = '//layouts/default';            
            $format = new Formatter();
            $modEmployee = new Employee();
            if(!empty($id)){
                $model = User::model()->findByPk($id);
                if(!empty($model->employee_id)){
                    $modEmployee = Employee::model()->findByPk($model->employee_id);
                }
            }else{
                $model = new User();
            }
            
            $this->render('viewUser',array('model'=>$model,'modEmployee'=>$modEmployee,'format'=>$format));
            
        }
        
        /**
         *  updateUser
         */
        public function actionUpdateUser($id = null){
            $this->layout = '//layouts/default';
            $prevUrl = Yii::app()->request->getUrlReferrer();
            $format = new Formatter();
            if(!empty($id)){
                $model = User::model()->findByPk($id);
                $model->attributes = $model;
                if(!empty($model->employee_id)){
                    $modEmployee = Employee::model()->findByPk($model->employee_id);
                }else{
                    $modEmployee = new Employee();
                }
            }
            
            if(isset($_POST['User'])){
                $model->attributes=$_POST['User'];
                $model->update_time = date('Y-m-d');
                $model->update_user_id = Yii::app()->user->id;
                
                if($model->validate()){
                    $modEmployee->attributes=$_POST['Employee'];
                    $modEmployee->update_time = date('Y-m-d');
                    $modEmployee->update_user_id = Yii::app()->user->id;
                    if($model->update() && $modEmployee->update())
                    {
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Password berhasil disimpan.');
                        if ($model->role_id == Params::ROLE_GUDANG) {
                            $this->redirect(Yii::app()->createUrl('/product/admin'));
                        } else if ($model->role_id == Params::ROLE_KASIR) {
                            $this->redirect(Yii::app()->createUrl('/saleProducts/index'));
                        } else if ($model->role_id == Params::ROLE_OWNER) {
                            $this->redirect(Yii::app()->createUrl('/product/dashboard'));
                        }
                    }else{
                        Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data Gagal Disimpan.');
                        $this->redirect(array('updateUser','id'=>$model->id));
                    }
                }
            }
            $this->render('updateUser',array(
                'model'=>$model,
                'modEmployee'=>$modEmployee,
                'format'=>$format,
                'prevUrl'=>$prevUrl,
            ));
            
        }
        
        /**
        *  updateShift
        */
        public function actionUpdateShift($id = null){
            $this->layout = '//layouts/default';
            $prevUrl = Yii::app()->request->getUrlReferrer();
            $format = new Formatter();
            if(!empty($id)){
                $model = User::model()->findByPk($id);
                $model->attributes = $model;
                if(!empty($model->employee_id)){
                    $modEmployee = Employee::model()->findByPk($model->employee_id);
                }else{
                    $modEmployee = new Employee();
                }
            }
            
            if(isset($_POST['Employee'])){
                $modEmployee->attributes=$_POST['Employee'];
                $modEmployee->update_time = date('Y-m-d');
                $modEmployee->update_user_id = Yii::app()->user->id;
                
                if($modEmployee->validate()){
                    if($modEmployee->save())
                    {
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Jam Kerja berhasil disimpan.');
                        if ($model->role_id == Params::ROLE_GUDANG) {
                            $this->redirect(Yii::app()->createUrl('/product/admin'));
                        } else if ($model->role_id == Params::ROLE_KASIR) {
                            $this->redirect(Yii::app()->createUrl('/saleProducts/index'));
                        } else if ($model->role_id == Params::ROLE_OWNER) {
                            $this->redirect(Yii::app()->createUrl('/product/dashboard'));
                        }
                    }else{
                        Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data Gagal Disimpan.');
                        $this->redirect(array('updateShift','id'=>$model->id));
                    }
                }
            }
            $this->render('updateShift',array(
                'model'=>$model,
                'modEmployee'=>$modEmployee,
                'format'=>$format,
                'prevUrl'=>$prevUrl,
            ));
            
        }
        
        public function actionAbsensi(){
            $this->layout = '//layouts/login';
            $format = new Formatter();
            $model = new Presence();
            
            $this->render('absensi',array(
               'model'=>$model,
               'format'=>$format,
            ));
        }
        
        /**
        * menampilkan id pegawai
        * @return row table 
        */
        public function actionLoadIdPegawai()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $code = isset($_POST['nik']) ? $_POST['nik'] : null;
                
                $employee_id = null;
                $pesan = '';
                $format = new Formatter();
                $criteria = new CDbCriteria();
                if(!empty($code)){
                    $criteria->addCondition('code ='.$code);
                }
                $modEmployee = Employee::model()->find($criteria);
                $employee_id = isset($modEmployee->id) ? $modEmployee->id : null;
                if(count($modEmployee) > 0){
                    $modUser = User::model()->findByAttributes(array('employee_id'=>$employee_id,'is_active'=>1));
                    if (count($modUser) > 0) {
                        $employee_id = $modEmployee->id;
                    } else {
                        $pesan = 'status user tidak aktif.';
                    }
                }else{
                    $pesan = 'data pegawai tidak ditemukan.';
                }            

                echo CJSON::encode(array(
                    'status'=>'create_form', 
                    'pesan'=>$pesan,
                    'employee_id'=>$employee_id ,
                ));
                exit;  
            }
        }
        
        public function actionSavePresenceDatang()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $code = isset($_POST['$code']) ? $_POST['$code'] : null;
                $employee_id = isset($_POST['employee_id']) ? $_POST['employee_id'] : null;
                
                $modPresences = new Presence();
                $pesan = '';
                $status_absen = '';
                $criteria = new CDbCriteria();
                if(!empty($code)){
                    $criteria->addCondition('code ='.$code);
                }
                if(!empty($employee_id)){
                    $criteria->addCondition('employee_id ='.$employee_id);
                }
                $criteria->addCondition(" DATE(in_hours) ='".date('Y-m-d')."' OR out_hours ='".date('Y-m-d')."'");
                $dataPresence = Presence::model()->find($criteria);
                if(count($dataPresence) > 0){
                    $dataPresence->employee_id = $employee_id;
                    if(!empty($dataPresence->in_hours)){
                        $status_absen = ' <font style="color:red;"> ANDA TELAH ABSEN DATANG PADA PUKUL <b>'.date('H:i:s',  strtotime($dataPresence->in_hours)).'</b></font>';
                    }else{
                        $dataPresence->in_hours = date('Y-m-d H:i:s');
                        $status_absen = ' <font style="color:red;">TERIMA KASIH ANDA TELAH ABSEN DATANG PADA PUKUL <b>'.date('H:i:s',  strtotime($dataPresence->in_hours)).'</b></font>';
                    }
                    if($dataPresence->update()){
                        $pesan = $status_absen;
                    }
                }else{
                    $modPresences->employee_id = $employee_id;
                    if(!empty($dataPresence->in_hours)){
                        $status_absen = ' <font style="color:red;"> ANDA TELAH ABSEN DATANG PADA PUKUL <b>'.date('H:i:s',  strtotime($dataPresence->in_hours)).'</b></font>';
                    }else{
                        $modPresences->in_hours = date('Y-m-d H:i:s');
                        $status_absen = ' <font style="color:green;"> TERIMA KASIH ANDA TELAH ABSEN DATANG PADA PUKUL <b>'.date('H:i:s',  strtotime($modPresences->in_hours)).'</b></font>';
                    }
                    if($modPresences->save()){
                        $pesan = $status_absen;
                    }                    
                }            

                echo CJSON::encode(array(
                    'status'=>'create_form', 
                    'pesan'=>$pesan,
                    'employee_id'=>$employee_id ,
                ));
                exit;  
            }
        }

    public function actionSavePresencePulang()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $code = isset($_POST['$code']) ? $_POST['$code'] : null;
                $employee_id = isset($_POST['employee_id']) ? $_POST['employee_id'] : null;
                
                $modPresences = new Presence();
                $pesan = '';
                $status_absen = '';
                $criteria = new CDbCriteria();
                if(!empty($code)){
                    $criteria->addCondition('code ='.$code);
                }
                if(!empty($employee_id)){
                    $criteria->addCondition('employee_id ='.$employee_id);
                }
                $criteria->addCondition(" DATE(in_hours) ='".date('Y-m-d')."' OR out_hours ='".date('Y-m-d')."'");
                $dataPresence = Presence::model()->find($criteria);
                if(count($dataPresence) > 0){
                    $dataPresence->employee_id = $employee_id;
                    if(!empty($dataPresence->out_hours)){
                        $status_absen = ' <font style="color:red;">ANDA TELAH ABSEN PULANG PADA PUKUL <b>'.date('H:i:s',  strtotime($dataPresence->out_hours)).'</b></font>';
                    }else{
                        $dataPresence->out_hours = date('Y-m-d H:i:s');
                        $status_absen = ' <font style="color:green;">TERIMA KASIH ANDA TELAH ABSEN PULANG PADA PUKUL <b>'.date('H:i:s',  strtotime($dataPresence->out_hours)).'</b></font>';
                    }
                    if($dataPresence->update()){
                        $pesan = $status_absen;
                    }
                }else{
                    $modPresences->employee_id = $employee_id;
                    if(!empty($dataPresence->out_hours)){
                        $status_absen = '<font style="color:red;"> ANDA TELAH ABSEN PULANG PADA PUKUL <b>'.date('H:i:s',  strtotime($modPresences->out_hours)).'</b></font>';
                    }else{
                        $modPresences->out_hours = date('Y-m-d H:i:s');
                        $status_absen = '<font style="color:green;">TERIMA KASIH ANDA TELAH ABSEN PULANG PADA PUKUL <b>'.date('H:i:s',  strtotime($modPresences->out_hours)).'</b></font>';
                    }
                    if($modPresences->save()){
                        $pesan = $status_absen;
                    }                    
                }            

                echo CJSON::encode(array(
                    'status'=>'create_form', 
                    'pesan'=>$pesan,
                    'employee_id'=>$employee_id ,
                ));
                exit;  
            }
        }

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
        // $this->redirect(Yii::app()->baseUrl);
		$this->redirect(Yii::app()->homeUrl);
	}
}