<?php

class ProductController extends Controller
{
    /**
    * @var string the default layout for the views. Defaults to '//layouts/default', meaning
    * using two-column layout. See 'protected/views/layouts/default.php'.
    */
    public $layout='//layouts/default';
    public $defaultAction='Admin';
    public $accordionIndex = 0;

    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionView($id)
    {
        $format = new Formatter();

        $this->render('view',array(
            'model'=>$this->loadModel($id),
            'format'=>$format
        ));
    }

    /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
    public function actionCreate()
    {
        $model=new Product;
        $format = new Formatter();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Product']))
        {
            $model->attributes=$_POST['Product'];
            $model->expire_date = isset($model->expire_date) ? $format->formatDateTimeForDb($model->expire_date) : null;
            $model->create_time = date('Y-m-d H:i:s');
            $model->create_user_id = Yii::app()->user->id;
            if($model->save()){
                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                $this->redirect(array('admin','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');
            }
        }

        $this->render('create',array(
            'model'=>$model,
            'format'=>$format,
        ));
    }

    /**
    * Updates a particular model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id the ID of the model to be updated
    */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $format = new Formatter();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Product']))
        {
            $model->attributes=$_POST['Product'];
            $model->expire_date = isset($model->expire_date) ? $format->formatDateTimeForDb($model->expire_date) : null;
            $model->update_time = date('Y-m-d H:i:s');
            $model->update_user_id = Yii::app()->user->id;

            if($model->save()){
                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                $this->redirect(array('admin','id'=>$model->id));
            }else{
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');
            }
        }

        $this->render('update',array(
            'model'=>$model,
            'format'=>$format,
        ));
    }

    /**
    * Deletes a particular model.
    * If deletion is successful, the browser will be redirected to the 'admin' page.
    * @param integer $id the ID of the model to be deleted
    */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_POST['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    /**
    * NonActives a particular model.
    * If deletion is successful, the browser will be redirected to the 'admin' page.
    * @param integer $id the ID of the model to be deleted
    */
    public function actionNonActive($id)
    {
        $model = $this->loadModel($id);
        $model->is_delete = 1;
        if($model->save()){
            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil dihapus.');
            $this->redirect(array('admin'));
        }else{
            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal dihapus.');
            $this->redirect(array('admin'));
        }
    }

    /**
    * Lists all models.
    */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Product');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
    * Manages all models.
    */
    public function actionAdmin()
    {
        $format = new Formatter();
        $model=new Product('searchProduct');
        $modProduct = Product::model()->findAllByAttributes(array('is_delete'=>0));
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Product'])){
            $model->attributes=$_GET['Product'];
            $model->expire_date=isset($_GET['Product']['expire_date']) ? $format->formatDateTimeForDb($_GET['Product']['expire_date']) : null;
            $model->category_name=isset($_GET['Product']['category_name']) ? $_GET['Product']['category_name'] : null;
            $model->periode_kadaluarsa=isset($_GET['Product']['periode_kadaluarsa']) ? $_GET['Product']['periode_kadaluarsa'] : null;
        }

        $this->render('admin',array(
            'model'=>$model,
            'modProduct'=>$modProduct
        ));
    }

    /**
    * Dashboard.
    */
    public function actionDashboard()
    {
        $format = new Formatter();
        $tgl = date('Y-m-d');
        //get total produk > 3 bulan kadaluarsa        
        $tgl_kadaluarsa_awal_1 = strtotime($tgl.' + 3 month');
        $tgl_kadaluarsa_awal_1 = date('Y-m-d',$tgl_kadaluarsa_awal_1);
        
        $criteria_1 = new CDbCriteria();
        $criteria_1->addCondition("DATE(expire_date) > '".$tgl_kadaluarsa_awal_1."'");
        $criteria_1->addCondition("is_delete = 0");
        
        $product_1 = Product::model()->findAll($criteria_1);
        $total_1 = count($product_1);
        if (count($product_1) > 0) {
            $total_1 = $total_1;
        }else{
            $total_1 = 0;
        }
        
        //get total produk 1 - 3 bulan kadaluarsa
        $tgl_kadaluarsa_akhir_2 = strtotime($tgl.' + 3 month');
        $tgl_kadaluarsa_akhir_2 = date('Y-m-d',$tgl_kadaluarsa_akhir_2);
        $tgl_kadaluarsa_awal_2 = strtotime($tgl.' + 1 month');        
        $tgl_kadaluarsa_awal_2 = date('Y-m-d',$tgl_kadaluarsa_awal_2);
        
        $criteria_2 = new CDbCriteria();
        $criteria_2->addCondition("DATE(expire_date) <= '".$tgl_kadaluarsa_awal_2."' && DATE(expire_date) >= '".$tgl_kadaluarsa_akhir_2."'");
        $criteria_2->addCondition("is_delete = 0");
                
        $product_2 = Product::model()->findAll($criteria_2);
        $total_2 = count($product_2);
        if (count($product_2) > 0) {
            $total_2 = $total_2;
        }else{
            $total_2 = 0;
        }
        
        //get total produk < 1 bulan kadaluarsa
        $tgl_kadaluarsa_awal_3 = strtotime($tgl.' + 1 month');
        $tgl_kadaluarsa_awal_3 = date('Y-m-d',$tgl_kadaluarsa_awal_3);
        
        $criteria_3 = new CDbCriteria();
        $criteria_3->addCondition("DATE(expire_date) < '".$tgl_kadaluarsa_awal_3."'");
        $criteria_3->addCondition("is_delete = 0");
       
        $product_3 = Product::model()->findAll($criteria_3);
        $total_3 = count($product_3);
        if (count($product_3) > 0) {
            $total_3 = $total_3;
        }else{
            $total_3 = 0;
        }
        
        //get total sales
        $sales = Sale::model()->findAll(array("select"=>"sub_total"));
        $totalSale = 0;
        if (count($sales) > 0) {
            foreach ($sales as $i => $sale) {
                $totalSale += $sale->sub_total;
            }
        }

        //get total order
        $orders = Order::model()->findAll(array("select"=>"sub_total"));
        $totalOrder = 0;
        if (count($orders) > 0) {
            foreach ($orders as $ii => $order) {
                $totalOrder += $order->sub_total;
            }
        }
        
        /**
         * grafik
         */
        //=== start 4 kolom ===
        $dataPie = array();
        $dataPieChart = array();
        $dataBarLineChart = array();
        $dataStackChart = array();
        $graphs = null;
        
        $model = new Sale();
        $model->unsetAttributes();
        $model->periode = "hari";
        $model->start_date = date('Y-m-d', strtotime('first day of this month'));
        $model->end_date = date('Y-m-d');
        $model->start_month = date('Y-m', strtotime('first day of january'));
        $model->end_month = date('Y-m');
        $model->start_year = date('Y');
        $model->end_year = date('Y');

        if (isset($_GET['Sale'])) {
            $model->attributes = $_GET['Sale'];
            $model->periode = $_GET['Sale']['periode'];
            $model->tipe = $_GET['Sale']['tipe'];
            $model->start_date = $format->formatDateTimeForDb($_GET['Sale']['start_date']);
            $model->end_date = $format->formatDateTimeForDb($_GET['Sale']['end_date']);
            $model->start_month = $format->formatMonthForDb($_GET['Sale']['start_month']);
            $model->end_month = $format->formatMonthForDb($_GET['Sale']['end_month']);
            $end_month = $model->end_month . "-" . date("t", strtotime($model->end_month));
            $model->start_year = $_GET['Sale']['start_year'];
            $model->end_year = $_GET['Sale']['end_year'];
            $end_year = $model->end_year . "-" . date("m-t", strtotime($model->end_year . "-12"));
            switch ($model->periode) {
                case 'bulan' : $model->start_date = $model->start_month . "-01";
                    $model->end_date = $end_month;
                    break;
                case 'tahun' : $model->start_date = $model->start_year . "-01-01";
                    $model->end_date = $end_year;
                    break;
                default : null;
            }
            $model->start_date = $model->start_date . " 00:00:00";
            $model->end_date = $model->end_date . " 23:59:59";
            
            //=== chart ===
        if($model->tipe == 'penjualan'){
            switch ($model->periode) {
                case 'bulan' : $sql = "
                    SELECT 
                    date(date) as periode, coalesce(sum(sub_total), 0)  as jumlah
                    FROM sales
                    WHERE DATE(date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    GROUP BY date
                    ORDER BY date ASC
                                                                            ";
                    break;
                case 'tahun' : $sql = "
                    SELECT 
                    date(date) as periode, coalesce(sum(sub_total), 0) as jumlah
                    FROM sales
                    WHERE DATE(date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    GROUP BY date
                    ORDER BY date ASC

                                                                            ";
                    break;
                default : $sql = "
                    SELECT 
                    date(date) as periode, coalesce(sum(sub_total), 0) as jumlah
                    FROM sales
                    WHERE DATE(date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    GROUP BY date
                    ORDER BY date ASC

                                                                            ";
            }
        }else if($model->tipe == 'pembelian'){
            switch ($model->periode) {
                case 'bulan' : $sql = "
                    SELECT 
                    date(date) as periode, coalesce(sum(sub_total), 0)  as jumlah
                    FROM orders
                    WHERE DATE(date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    GROUP BY date
                    ORDER BY date ASC
                                                                            ";
                    break;
                case 'tahun' : $sql = "
                    SELECT 
                    date(date) as periode, coalesce(sum(sub_total), 0) as jumlah
                    FROM orders
                    WHERE DATE(date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    GROUP BY date
                    ORDER BY date ASC

                                                                            ";
                    break;
                default : $sql = "
                    SELECT 
                    date(date) as periode, coalesce(sum(sub_total), 0) as jumlah
                    FROM orders
                    WHERE DATE(date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    GROUP BY date
                    ORDER BY date ASC

                                                                            ";
            }
        }else if($model->tipe == 'laba_rugi'){
            switch ($model->periode) {
                case 'bulan' : $sql = "
                    SELECT 
                    date(s.date) as periode, 
                    (SELECT coalesce(sum(sub_total), 0) as penjualan FROM sales sa  WHERE DATE(sa.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "') - 
                    (SELECT coalesce(sum(sub_total), 0) as pembelian FROM orders o  WHERE DATE(o.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "') as jumlah
                    FROM sales s
                    WHERE DATE(s.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    AND DATE(s.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    GROUP BY s.date
                    ORDER BY s.date ASC
                                                                            ";
                    break;
                case 'tahun' : $sql = "
                    SELECT 
                    date(s.date) as periode, 
                    (SELECT coalesce(sum(sub_total), 0) as penjualan FROM sales sa  WHERE DATE(sa.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "') - 
                    (SELECT coalesce(sum(sub_total), 0) as pembelian FROM orders o  WHERE DATE(o.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "') as jumlah
                    FROM sales s
                    WHERE DATE(s.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    AND DATE(s.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    GROUP BY s.date
                    ORDER BY s.date ASC

                                                                            ";
                    break;
                default : $sql = "
                    SELECT 
                    date(s.date) as periode, 
                    (SELECT coalesce(sum(sub_total), 0) as penjualan FROM sales sa  WHERE DATE(sa.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "') - 
                    (SELECT coalesce(sum(sub_total), 0) as pembelian FROM orders o  WHERE DATE(o.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "') as jumlah
                    FROM sales s
                    WHERE DATE(s.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    AND DATE(s.date) BETWEEN '" . $model->start_date . "' AND '" . $model->end_date . "'
                    GROUP BY s.date
                    ORDER BY s.date ASC

                                                                            ";
            }
        }
        
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $dataBarLineChart = $result;
        // generate_graph
        $graph = array();
        $graphs = array();
        foreach ($result as $data) {            
            unset($data['periode']);
            unset($data['jumlah']);
        }

        // make graphs
        foreach ($graph as $key => $data) {
            $temp['title'] = $data[0]['jenis'];
            $temp['valueField'] = "jumlah" . $data[0]['id'];
            $temp['balloonText'] = "[[title]]:[[value]]";
            $temp['lineAlpha'] = 0.5;
            $temp['fillAlphas'] = 0.5;
            array_push($graphs, $temp);
        }
        //=== end chart ===
        }
        
        
        $model->start_date = $format->formatDateTimeForUser(date('Y-m-d', (strtotime($model->start_date))));
        $model->end_date = $format->formatDateTimeForUser(date('Y-m-d', (strtotime($model->end_date))));
        $model->start_month = $format->formatMonthForUser(date('Y-m', (strtotime($model->start_month))));
        $model->end_month = $format->formatMonthForUser(date('Y-m', (strtotime($model->end_month))));
        
        $this->render('dashboard',array(
            'sale'=>$totalSale,
            'order'=>$totalOrder,
            'total_1'=>$total_1,
            'total_2'=>$total_2,
            'total_3'=>$total_3,
            'model' => $model,
            'dataBarLineChart' => $dataBarLineChart,
            'graphs' => $graphs
        ));
    }
  
    
    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    * @param integer the ID of the model to be loaded
    */
    public function loadModel($id)
    {
        $model=Product::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    /**
    * mencari barcode
    * @return barcode 
    */
    public function actionSetBarcode()
    {
        if(Yii::app()->request->isAjaxRequest) { 
            $barcode = isset($_POST['barcode']) ? $_POST['barcode'] : null;
            $status = '';
            $pesan = '';
            
            $criteria = new CDbCriteria();
            if(!empty($barcode)){
                $criteria->compare('LOWER(barcode)',strtolower($barcode),true);
            }
            $model = Product::model()->find($criteria);
            if(!isset($model)){
                $status = '';
                $pesan = '';
            }else{
                $status = 'ada';
                $pesan = 'Barcode sudah ada pada database';
            }
            echo CJSON::encode(array(
                'pesan'=>$pesan,
                'status'=>$status,
            ));
            exit;  
        }
    }
    
    public function actionPrintLabel()
    {
        $model= new Product('searchPrint');
        if(isset($_REQUEST['Product'])){
            $model->attributes=$_REQUEST['Product'];
            $model->id=isset($_REQUEST['Product']['id']) ? $_REQUEST['Product']['id'] : null;
        }
        
        $criteria = new CDbCriteria();
        if(is_array($model->id)){
            $criteria->addInCondition('id', $model->id);
        }
        $model = Product::model()->findAll($criteria);
        if(count($model) > 0){
            $model = $model;
        }else{
            $model = array();
        }
        $judulLaporan='Cetak Label ';
        $caraPrint=$_REQUEST['caraPrint'];
        if($caraPrint=='PRINT') {
            $this->layout='cetakLabel';
            $this->render('cetakLabel',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }                  
    }

    public function actionPrintStok()
    {
        $format = new Formatter();
        $model=new Product('searchPrintStok');
        $model->unsetAttributes();  // clear any default values
        $start_date = date("d M Y");
        $end_date = date("d M Y");

        if(isset($_REQUEST['Product']))
        {
            $model->attributes=$_REQUEST['Product'];
            $model->expire_date=isset($_REQUEST['Product']['expire_date']) ? $format->formatDateTimeForDb($_REQUEST['Product']['expire_date']) : null;
            $model->category_name=isset($_REQUEST['Product']['category_name']) ? $_REQUEST['Product']['category_name'] : null;
            $model->periode_kadaluarsa=isset($_REQUEST['Product']['periode_kadaluarsa']) ? $_REQUEST['Product']['periode_kadaluarsa'] : null;
            
            $periode = $format->formatDateTimeForUser($start_date);
        }
        $judulLaporan = 'Laporan Stok';
        $caraPrint = isset($_REQUEST['caraPrint']) ? $_REQUEST['caraPrint'] : null;
        $target = 'printStock';

        $this->printFunction($model, $periode, $caraPrint, $judulLaporan, $target);

    }

    protected function printFunction($model, $periode, $caraPrint, $judulLaporan, $target){
        $format = new Formatter();
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
}
