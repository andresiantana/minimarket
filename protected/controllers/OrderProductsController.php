<?php

class OrderProductsController extends Controller
{
    /**
    * @var string the default layout for the views. Defaults to '//layouts/default', meaning
    * using two-column layout. See 'protected/views/layouts/default.php'.
    */
    public $layout='//layouts/default';
    public $defaultAction='Index';
    public $saleproductsave = false;
    public $saleproductDetailsave = true; // for looping
    public $accordionIndex = 0;
    

    /**
    * Creates a new model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    */
    public function actionIndex($id = null)
    {
        $format = new Formatter();
        $model=new Order();
        $model->number = '';
        $model->date = date('Y-m-d H:i:s');
        $modDetails = array();

        if(!empty($id)){
            $model= Order::model()->findByPk($id);
            $modDetails = OrderDetails::model()->findAllByAttributes(array('order_id'=>$model->id));
        }
        if(isset($_POST['Order'])){
             $transaction = Yii::app()->db->beginTransaction();
             try {
                    $modDetails = isset($_POST['OrderDetails']) ? $_POST['OrderDetails'] : null;
                    $model->attributes=$_POST['Order'];
                    $model->number= isset($model->number) ? $model->number : MyGenerator::ordersNumber();
                    $model->date= isset($model->date) ? $format->formatDateTimeForDb($model->date) : null;
                    $model->create_time = date('Y-m-d H:i:s');
                    $model->create_user_id = Yii::app()->user->id;
                    $model->employee_id = Yii::app()->user->getState('employee_id');
                    $model->role_id = Yii::app()->user->getState('role_id');
                    $model->shift_id = Yii::app()->user->getState('shift_id');
                                 
                    if($model->save()){
                        $this->saleproductsave = true;
                        if(count($_POST['OrderDetails']) > 0){
                            foreach($_POST['OrderDetails'] AS $i => $postProduct){
                                $modDetails[$i] = $this->simpanDetailProducts($model,$postProduct);
                            }
                        }
                    }
                    
                    if($this->saleproductsave && $this->saleproductDetailsave){                        
                        $transaction->commit();
                        $model->isNewRecord = FALSE;
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Transaksi Pembelian berhasil disimpan.');
//                        $this->redirect(array('index','id'=>$model->id,'sukses'=>1));
                        $this->redirect(array('index','sukses'=>1));
                    }else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"<strong>Gagal!</strong> Transaksi Pembelian gagal disimpan !");
                    }

            }catch(Exception $exc){
                $model->isNewRecord = true;
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
            }    
        }

        $this->render('index',array(
            'model'=>$model,
            'modDetails'=>$modDetails,
            'format'=>$format
        ));
    }
    
    /**
     * simpan OrderDetails
     * @param type $model
     * @param type $post
     */
    public function simpanDetailProducts($model ,$post){
        $format = new Formatter();
        $modDetails = new OrderDetails;
        $modProduct = Product::model()->findByPk($post['product_id']);
        
        $modDetails->attributes = $post;
        $modDetails->order_id = $model->id;
        $modDetails->create_time = date('Y-m-d H:i:s');
        $modDetails->create_user_id = Yii::app()->user->id;
        $modDetails->product_name = $modProduct->name;
        $modDetails->product_barcode = $modProduct->barcode;
        $modDetails->product_unit = $modProduct->unit;
        
        if($modDetails->validate()) {
            $modDetails->save();            
            $stok = $modProduct->stock;
            if($modDetails->quantity > 0){
                $modProduct->stock = $stok + $modDetails->quantity;
            }            
            $modProduct->save();
            $this->saleproductDetailsave &= true;
        } else {
            $this->saleproductDetailsave  &= false;
        }
        return $modDetails;
    }
    
    /**
    * Manages all models.
    */
    public function actionReport()
    {
        $format = new Formatter();
        $model=new Order('searchReport');
        $model->start_date = date('Y-m-d');
        $model->end_date = date('Y-m-d');
        $model->unsetAttributes();  // clear any default values
        
        if(isset($_GET['Order'])){
            $model->attributes=$_GET['Order'];
            $model->start_date = isset($_GET['Order']['start_date']) ? $format->formatDateTimeForDb($_GET['Order']['start_date']) : null;
            $model->end_date = isset($_GET['Order']['end_date']) ? $format->formatDateTimeForDb($_GET['Order']['end_date']) : null;
        }

        $this->render('report',array(
            'model'=>$model,
            'format'=>$format
        ));
    }

    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    * @param integer the ID of the model to be loaded
    */
    public function loadModel($id)
    {
        $model=Order::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='sale-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function actionAutocompleteProduct()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $name = isset($_GET['name']) ? $_GET['name'] : null;
            $barcode = isset($_GET['barcode']) ? $_GET['barcode'] : null;
            
            $criteria = new CDbCriteria();
            if(!empty($name)){
                $criteria->compare('LOWER(name)', strtolower($name), true);
            }
            if(!empty($barcode)){
                $criteria->compare('LOWER(barcode)', strtolower($barcode), true);
            }
            $criteria->order = 'name ASC';
            $criteria->limit = 5;
            $models = Product::model()->findAll($criteria);
            $returnVal=array();
            foreach($models as $i=>$model)
            {
                $attributes = $model->attributeNames();
                foreach($attributes as $j=>$attribute) {
                    $returnVal[$i]["$attribute"] = $model->$attribute;
                }
                $returnVal[$i]['label'] = $model->name;
                $returnVal[$i]['value'] = $model->id;
            }

            echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }
    
    /**
    * menampilkan product
    * @return row table 
    */
    public function actionLoadFormOrderProducts()
    {
        if(Yii::app()->request->isAjaxRequest) { 
            $id = isset($_POST['id']) ? $_POST['id'] : null;
            $quantity = isset($_POST['quantity']) ? $_POST['quantity'] : 1;
            $barcode = isset($_POST['barcode']) ? $_POST['barcode'] : null;
            
            $pesan = '';
            $form = '';
            $format = new Formatter();
            $model = new Order();
            $modDetails = new OrderDetails();
            $criteria = new CDbCriteria();
            if(!empty($id)){
                $criteria->addCondition('id ='.$id);
            }
            if(!empty($barcode)){
                $criteria->compare('LOWER(barcode)',strtolower($barcode),true);
            }
            $modProduct = Product::model()->find($criteria);
            if(count($modProduct) > 0){
                $stok = $modProduct->stock;
                if($stok >= $quantity){
                    $modDetails->quantity = $quantity;
                    $modDetails->product_id = $modProduct->id;
                    $modDetails->barcode = $modProduct->barcode;
                    $modDetails->prices_order = 0;
                    $modDetails->name = $modProduct->name;
                    $modDetails->discount = 0;
                    $modDetails->total_discount = 0;
                    $modDetails->sub_total = 0;

                    $form = $this->renderPartial('_rowProductDetails', array(
                            'model'=>$model,
                            'modDetails'=>$modDetails,
                            'modProduct'=>$modProduct,
                        ),true);
                }else{
                    $pesan = 'Stok produk tidak mencukupi.';
                }
            }else{
                $pesan = 'produk tidak ditemukan pada databse, silahkan menghubungi bagian Gudang.';
            }
            
            
            echo CJSON::encode(array(
                'status'=>'create_form', 
                'pesan'=>$pesan,
                'form'=>$form ,
            ));
            exit;  
        }
    }
    
    public function actionDetailPembelian($id,$caraPrint = null){
        $this->layout = '//layouts/iframe';
        $model = Order::model()->findByPk($id);
        $modDetails = OrderDetails::model()->findAllByAttributes(array('order_id'=>$id));
        $judulLaporan='Detail Pembelian';
        $caraPrint = isset($_GET['caraPrint']) ? $_GET['caraPrint'] : null;
        if($caraPrint == 'PRINT'){
            $this->layout = '//layouts/printWindows';
        }
        $this->render('PrintDetail',array('model'=>$model, 'modDetails'=>$modDetails, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
    }
    
    public function actionPrintReport() 
    {
        $format = new Formatter;
        $model = new Order;
        $model->start_date = date("d M Y");
        $model->end_date = date("d M Y");

        if(isset($_REQUEST['Order']))
        {
            $model->attributes = $_REQUEST['Order'];
            $model->start_date = $format->formatDateTimeForDb($_REQUEST['Order']['start_date']);
            $model->end_date = $format->formatDateTimeForDb($_REQUEST['Order']['end_date']);
            $model->shift_id = isset($_REQUEST['Order']['shift_id']) ? $_REQUEST['Order']['shift_id'] : null;
            $model->number = isset($_REQUEST['Order']['number']) ? $_REQUEST['Order']['number'] : null;
            
            $periode = $format->formatDateTimeForUser($model->start_date).' s/d '.$format->formatDateTimeForUser($model->end_date);
            
            $criteria = new CDbCriteria();
            $criteria->addBetweenCondition('DATE(date)', $model->start_date, $model->end_date,true);
            $criteria->compare('LOWER(date)',strtolower($model->date),true);
            $criteria->compare('LOWER(number)',strtolower($model->number),true);
            if(!empty($model->shift_id)){
                $criteria->addCondition('shift_id = '.$model->shift_id);
            }
            if(Yii::app()->user->role_id != Params::ROLE_OWNER){
                $criteria->compare('role_id',Yii::app()->user->role_id);
                //$criteria->compare('create_user_id',Yii::app()->user->id);
            }
            $model = Order::model()->findAll($criteria);
        }
        $judulLaporan = 'Laporan Pembelian Produk';
        $caraPrint = isset($_REQUEST['caraPrint']) ? $_REQUEST['caraPrint'] : null;
        $target = 'PrintReport';

        $this->printFunction($model, $periode, $caraPrint, $judulLaporan, $target);

    } 
    
    protected function printFunction($model, $periode, $caraPrint, $judulLaporan, $target){
        $format = new Formatter();
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode,'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
}
