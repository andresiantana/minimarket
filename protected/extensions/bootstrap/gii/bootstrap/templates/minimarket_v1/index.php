<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label',
);\n";
?>

$this->menu=array(
    array('label'=>'Create <?php echo $this->modelClass; ?>','url'=>array('create')),
    array('label'=>'Manage <?php echo $this->modelClass; ?>','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            <?php echo $label; ?>
        </h1>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo "<?php"; ?> $this->widget('booster.widgets.TbListView',array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>'_view',
                )); ?>
            </div>
            <div class="panel-footer">
                <?php echo "<?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan ".$this->modelClass."',array('{icon}'=>'<i class=\"fa fa-gear\"></i>')),\$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>\n"; ?>
            </div>
        </div>
    </div>
</div>
