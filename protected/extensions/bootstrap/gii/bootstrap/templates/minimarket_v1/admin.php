<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
    echo "<?php\n";
    $label = $this->pluralize($this->class2name($this->modelClass));
    echo "\$this->breadcrumbs=array(
            '$label'=>array('index'),
            'Manage',
    );\n";
?>

$this->menu=array(
    array('label'=>'List <?php echo $this->modelClass; ?>','url'=>array('index')),
    array('label'=>'Create <?php echo $this->modelClass; ?>','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Manage <small><?php echo $this->pluralize($this->class2name($this->modelClass)); ?></small>
        </h1>
        <?php echo "<?php \$this->widget('booster.widgets.TbAlert'); ?>\n"; ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo "<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>"; ?>
                <div class="search-form" style="display:none">
                        <?php echo "<?php \$this->renderPartial('_search',array(
                        'model'=>\$model,
                )); ?>\n"; ?>
                </div><!-- search-form -->
            </div>
            <div class="panel-body">
                <?php echo "<?php"; ?> $this->widget('booster.widgets.TbGridView',array(
                    'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
                    'dataProvider'=>$model->search(),
                    'filter'=>$model,
                    'template'=>"{summary}\n{items}\n{pager}",
                    'itemsCssClass'=>'table table-striped table-bordered table-hover',
                    'columns'=>array(
                        array(
                            'header'=>'No.',
                            'value' => '($this->grid->dataProvider->pagination) ? 
                                            ($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
                                            : ($row+1)',
                            'type'=>'raw',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        <?php
                            $count = 0;
                            foreach ($this->tableSchema->columns as $column) {
                                if (++$count == 7) {
                                    echo "\t\t/*\n";
                                }
                                echo "\t\t'" . $column->name . "',\n";
                            }
                            if ($count >= 7) {
                                echo "\t\t*/\n";
                            }
                        ?>
                        array(
                            'header'=>Yii::t('zii','View'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{view}',
                            'buttons'=>array(
                                    'view' => array(),
                             ),
			),
			array(
                            'header'=>Yii::t('zii','Update'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{update}',
                            'buttons'=>array(
                                    'update' => array(),
                             ),
			),
			array(
                            'header'=>Yii::t('zii','Delete'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{delete}',
                            'buttons'=>array(
                                'delete'=> array(
                                    'url'=>'Yii::app()->createUrl("/'.Yii::app()->controller->id.'/nonActive",array("id"=>$data-><?php echo $this->tableSchema->primaryKey; ?>))',
                                ),
                            )
			),
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                <?php echo "<?php echo Chtml::link('<i class=\"entypo-plus\"></i> Tambah',\$this->createUrl('/" . $this->controller . "/create'), array('class' => 'btn btn-success')); ?>"; ?>
            </div>
        </div>
    </div>
</div>
