<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'Update',
);\n";
?>

$this->menu=array(
    array('label'=>'List <?php echo $this->modelClass; ?>','url'=>array('index')),
    array('label'=>'Create <?php echo $this->modelClass; ?>','url'=>array('create')),
    array('label'=>'View <?php echo $this->modelClass; ?>','url'=>array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
    array('label'=>'Manage <?php echo $this->modelClass; ?>','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Ubah <small><?php echo $this->modelClass . " <?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></small>
        </h1>
        <?php echo "<?php \$this->widget('booster.widgets.TbAlert'); ?>\n"; ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo "<?php echo \$this->renderPartial('_form',array('model'=>\$model)); ?>"; ?>
            </div>
        </div>
    </div>
</div>