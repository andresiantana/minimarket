<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php echo "<?php \$form=\$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'" . $this->class2id($this->modelClass) . "-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus' => '#" . $this->class2id($this->modelClass) . "-form div.form-group:first-child div input',
        'htmlOptions'=>array('class'=>'form','onKeyPress'=>'return disableKeyPress(event);', 'enctype' => 'multipart/form-data'),
)); ?>\n"; ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<?php
foreach ($this->tableSchema->columns as $column) {
    if ($column->autoIncrement) {
            continue;
    }
    ?>
    <?php echo "<?php echo " . $this->generateActiveGroup($this->modelClass, $column) . "; ?>\n"; ?>
<?php
}
?>
<div class="form-group">
    <div class="col-lg-4">
        <?php echo "<?php \$this->widget('booster.widgets.TbButton', array(
                'buttonType'=>'submit',
                'context'=>'primary',
                'label'=>\$model->isNewRecord ? 'Create' : 'Save',
        )); ?>\n"; ?>
        <?php echo "<?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan ".$this->modelClass."',array('{icon}'=>'<i class=\"fa fa-gear\"></i>')),\$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>\n"; ?>
    </div>
</div>
<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
