<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn},
);\n";
?>

$this->menu=array(
    array('label'=>'List <?php echo $this->modelClass; ?>','url'=>array('index')),
    array('label'=>'Create <?php echo $this->modelClass; ?>','url'=>array('create')),
    array('label'=>'Update <?php echo $this->modelClass; ?>','url'=>array('update','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
    array('label'=>'Delete <?php echo $this->modelClass; ?>','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage <?php echo $this->modelClass; ?>','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Lihat <small><?php echo $this->modelClass . " #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></small>
        </h1>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo "<?php"; ?> $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        <?php
                        foreach ($this->tableSchema->columns as $column) {
                                echo "\t\t'" . $column->name . "',\n";
                        }
                        ?>
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                <?php echo "<?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan ".$this->modelClass."',array('{icon}'=>'<i class=\"fa fa-gear\"></i>')),\$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>\n"; ?>
            </div>
        </div>
    </div>
</div>
