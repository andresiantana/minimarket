<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Create',
);\n";
?>

$this->menu=array(
    array('label'=>'List <?php echo $this->modelClass; ?>','url'=>array('index')),
    array('label'=>'Manage <?php echo $this->modelClass; ?>','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Tambah <small><?php echo $this->modelClass; ?></small>
        </h1>
        <?php echo "<?php \$this->widget('booster.widgets.TbAlert'); ?>\n"; ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
            </div>
        </div>
    </div>
</div>
