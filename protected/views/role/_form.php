<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'role-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus' => '#role-form div.form-group:first-child div input',
        'htmlOptions'=>array('class'=>'form','onKeyPress'=>'return disableKeyPress(event);', 'enctype' => 'multipart/form-data'),
)); ?>

    <p class="help-block">Inputan dengan tanda <span class="required">*</span> harus diisi.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20,'onkeyup'=>'setLower(this);')))); ?>

    <?php echo $form->textFieldGroup($model,'role_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
    
    <?php if(isset($id)){ ?>
        <?php echo $form->checkBoxGroup($model,'is_delete',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
    <?php } ?>

<div class="form-group">
    <div class="col-lg-4">
        <?php $this->widget('booster.widgets.TbButton', array(
                'buttonType'=>'submit',
                'context'=>'primary',
                'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
        )); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan Posisi Pegawai',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    function setLower(obj){
        $('#<?php echo CHtml::activeId($model,'name'); ?>').val(obj.value.toLowerCase());
        $('#<?php echo CHtml::activeId($model,'role_name'); ?>').val(obj.value);
    }
    
    $.fn.capitalize = function() {
        return this.each(function() {
            var $field = $(this);

            $field.on('keyup change', function() {
                $field.val(function(i, old) {
                    if (old.indexOf(' ') > -1) {
                        var words = old.split(' ');
                        for (i = 0; i < words.length; i++) {
                            words[i] = caps(words[i]);
                        }
                        return words.join(' ');
                    } else {
                        return caps(old);
                    }
                });
            });
        });

        function caps(str) {
            return str.charAt(0).toUpperCase() + str.slice(1);
        }
    };
    
    $(document).ready(function(){
        $('#<?php echo CHtml::activeId($model,'role_name'); ?>').capitalize();
    });
</script>
