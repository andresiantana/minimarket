<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'category-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus' => '#category-form div.form-group:first-child div input',
        'htmlOptions'=>array('class'=>'form','onKeyPress'=>'return disableKeyPress(event);', 'enctype' => 'multipart/form-data'),
)); ?>

    <p class="help-block">Inputan dengan tanda <span class="required">*</span> harus diisi.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20, 'onkeyup'=>"return $(this).focusNextInputField(event)",)))); ?>

    <?php if(isset($id)){ ?>
        <?php echo $form->checkBoxGroup($model,'is_delete',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5', 'onkeyup'=>"return $(this).focusNextInputField(event)",)))); ?>
    <?php } ?>

    <div class="form-group">
        <div class="col-lg-4">
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'context'=>'primary',
                    'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
                    'htmlOptions'=>array(
                        'onkeypress'=>'formSubmit(this,event)',
                    )
            )); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Daftar Kategori Produk',array('{icon}'=>'<i class="fa fa-list"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
        </div>
    </div>
<?php $this->endWidget(); ?>
