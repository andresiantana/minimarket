<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('label'=>'List Category','url'=>array('index')),
    array('label'=>'Create Category','url'=>array('create')),
    array('label'=>'View Category','url'=>array('view','id'=>$model->id)),
    array('label'=>'Manage Category','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Ubah <small>Kategori Produk</small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
            </div>
        </div>
    </div>
</div>