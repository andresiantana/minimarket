<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('label'=>'List Category','url'=>array('index')),
    array('label'=>'Create Category','url'=>array('create')),
    array('label'=>'Update Category','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete Category','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage Category','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Lihat <small>Kategori #<?php echo $model->name; ?></small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <?php
                    $this->widget('booster.widgets.TbDetailView',array(
                        'data'=>$model,
                        'attributes'=>array(
                            'id',
                            'name',
                            // array(
                            //     'label'=>'Status Delete',
                            //     'value'=>isset($model->is_delete) ? "Aktif" : "Tidak Aktif",
                            // ),
                        ),
                    ));
                ?>
            </div>
            <div class='panel-footer'>
                    <?php // echo CHtml::link(Yii::t('mds','{icon} Ubah',array('{icon}'=>'<i class="fa fa-edit"></i>')),$this->createUrl('update',array('id'=>$model->id)), array('class'=>'btn btn-success')); ?>
                    <?php echo CHtml::link(Yii::t('mds','{icon} Daftar Kategori Produk',array('{icon}'=>'<i class="fa fa-list"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>
