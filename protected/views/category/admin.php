<?php
$this->breadcrumbs=array(
            'Categories'=>array('index'),
            'Manage',
    );

$this->menu=array(
    array('label'=>'List Category','url'=>array('index')),
    array('label'=>'Create Category','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('#search-form').submit(function(){
        $.fn.yiiGridView.update('category-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Daftar <small>Kategori Produk</small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo CHtml::link('Pencarian','#',array('class'=>'search-button btn')); ?>
                <div class="search-form" style="display:none">
                    <?php $this->renderPartial('_search',array(
                            'model'=>$model,
                            ));
                    ?>
                </div><!-- search-form -->
            </div>
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbGridView',array(
                    'id'=>'category-grid',
                    'dataProvider'=>$model->search(),
                    'filter'=>$model,
                    'template'=>"{summary}\n{items}\n{pager}",
                    'itemsCssClass'=>'table table-striped table-bordered table-hover',
                    'columns'=>array(
                        array(
                            'header'=>'No.',
                            'value' => '($this->grid->dataProvider->pagination) ?
                                            ($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
                                            : ($row+1)',
                            'type'=>'raw',
                            'htmlOptions'=>array('style'=>'text-align:right;width:5%;'),
                        ),
                        'name',
                        array(
                            'header'=>Yii::t('zii','Detail'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{view}',
                            'buttons'=>array(
                                    'view' => array(),
                             ),
			),
			array(
                            'header'=>Yii::t('zii','Ubah'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{update}',
                            'buttons'=>array(
                                    'update' => array(),
                             ),
			),
			array(
                            'header'=>Yii::t('zii','Hapus'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{delete}',
                            'buttons' => 
                            array('delete' =>
                                array(
                                    'url' => 'Yii::app()->createUrl("/'.Yii::app()->controller->id.'/nonActive",array("id"=>$data->id))',                                    
                                    'label' => 'delete', 
                                    'options' => array(// this is the 'html' array but we specify the 'ajax' element
                                        'confirm' => "Jika kategori ini dihapus, maka produk terkait akan terhapus juga.",
                                    ),
                                    
                                ),
                            ),
			),
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                <?php echo Chtml::link('<i class="entypo-plus"></i> Tambah',$this->createUrl('/category/create'), array('class' => 'btn btn-success')); ?></div>
            </div>
    </div>
</div>
