<?php
$this->breadcrumbs=array(
	'Categories',
);

$this->menu=array(
    array('label'=>'Create Category','url'=>array('create')),
    array('label'=>'Manage Category','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Daftar Kategori
        </h1>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbListView',array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>'_view',
                )); ?>
            </div>
            <div class="panel-footer">
                <?php echo CHtml::link(Yii::t('mds','{icon} Daftar Kategori Produk',array('{icon}'=>'<i class="fa fa-list"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>
