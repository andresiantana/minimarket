<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type'=>'horizontal',
        'id'=>'search-form',
	'htmlOptions'=>array('class'=>'form'),
)); ?>

        <?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

        <?php echo $form->textFieldGroup($model,'handphone',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

        <?php echo $form->textFieldGroup($model,'address',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

        <?php echo $form->dropDownListGroup($model,'shift_id', array('widgetOptions'=>array('data'=>CHtml::listData(Shift::model()->findAll(),'id','name'),'htmlOptions'=>array('class'=>'span5','empty'=>'--Pilih--')))); ?>

        <?php //echo $form->checkBoxGroup($model,'is_delete',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<div class="form-group">
            <div class="col-lg-4">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Cari',
		)); ?>
            </div>
	</div>

<?php $this->endWidget(); ?>
