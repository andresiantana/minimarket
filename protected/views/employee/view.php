<?php
$this->breadcrumbs=array(
	'Employees'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('label'=>'List Employee','url'=>array('index')),
    array('label'=>'Create Employee','url'=>array('create')),
    array('label'=>'Update Employee','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete Employee','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage Employee','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Lihat <small>Pegawai #<?php echo $model->name; ?></small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        'id',
                        'name',
                        'address',
                        'handphone',
                        // array(
                        //     'label'=>'Status Delete',
                        //     'value'=>isset($model->is_delete) ? "Aktif" : "Tidak Aktif",
                        // ),
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                <?php // echo CHtml::link(Yii::t('mds','{icon} Ubah',array('{icon}'=>'<i class="fa fa-edit"></i>')),$this->createUrl('update',array('id'=>$model->id)), array('class'=>'btn btn-success')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan Pegawai',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>
