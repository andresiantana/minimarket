<?php $profile = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET); ?>
<style>
    td, th{
        font-size: 8pt !important;
    }
    body{
        width:7.9cm;
    }
    .barcode{
        width:100px;
        border: 0px solid;
        margin:0px;
        padding:0px;
        overflow: hidden;
        position: absolute;
        filter: gray;
    }
</style>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td style="text-align:center;" colspan="5">
                        <b><?php echo isset($profile->name) ? strtoupper($profile->name) : "CAHAYA MUDA TOSERBA"; ?></b><br>
                        <?php echo isset($profile->address) ? $profile->address : "Jl. Raya Majalangu Watukumpul, Pemalang"; ?><br>
                        <?php echo "Telp. ".isset($profile->phone) ? $profile->phone : "082322965854"; ?> 
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 2px solid #000000"></td>
                </tr>
                <tr>
                    <td style="text-align:center;" colspan="5">
                        <b><u>KARTU PEGAWAI</u></b>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;" colspan="5">
                        <b><?php echo isset($model->name) ? strtoupper($model->name) : ""; ?> <br> 
                        <img src="index.php?r=barcode/myBarcode&code=<?php echo $model->code; ?>&is_text=">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>