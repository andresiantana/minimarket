<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'employee-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus' => '#employee-form div.form-group:first-child div input',
        'htmlOptions'=>array('class'=>'form','onKeyPress'=>'return disableKeyPress(event);', 'enctype' => 'multipart/form-data'),
)); ?>

    <p class="help-block">Inputan dengan tanda <span class="required">*</span> harus diisi.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

    <?php echo $form->textAreaGroup($model,'address', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

    <?php echo $form->textFieldGroup($model,'handphone',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>22)))); ?>

    <?php echo $form->dropDownListGroup($model,'shift_id', array('widgetOptions'=>array('data'=>CHtml::listData(Shift::model()->findAll(),'id','NameAndHours'),'htmlOptions'=>array('class'=>'span5','empty'=>'--Pilih Jam Kerja--')))); ?>

    <?php if(isset($id)){ ?>
        <?php echo $form->checkBoxGroup($model,'is_delete',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
    <?php } ?>
<div class="form-group">
    <div class="col-lg-4">
        <?php $this->widget('booster.widgets.TbButton', array(
                'buttonType'=>'submit',
                'context'=>'primary',
                'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
        )); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan Pegawai',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
