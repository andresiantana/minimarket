<?php
$this->breadcrumbs=array(
            'Employees'=>array('index'),
            'Manage',
    );

$this->menu=array(
    array('label'=>'List Employee','url'=>array('index')),
    array('label'=>'Create Employee','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('employee-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Pengaturan <small>Pegawai</small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo CHtml::link('Pencarian','#',array('class'=>'search-button btn')); ?>
                <div class="search-form" style="display:none">
                        <?php 
                            $this->renderPartial('_search',array(
                                'model'=>$model,
                            ));
                        ?>
                </div><!-- search-form -->
            </div>
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbGridView',array(
                    'id'=>'employee-grid',
                    'dataProvider'=>$model->search(),
                    'filter'=>$model,
                    'template'=>"{summary}\n{items}\n{pager}",
                    'itemsCssClass'=>'table table-striped table-bordered table-hover',
                    'columns'=>array(
                        array(
                            'header'=>'No.',
                            'value' => '($this->grid->dataProvider->pagination) ?
                                            ($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
                                            : ($row+1)',
                            'type'=>'raw',
                            'htmlOptions'=>array('style'=>'text-align:right;width:5%;'),
                        ),
                         array(
                            'header'=>'NIK',
                            'name'=>'code',
                            'type'=>'raw',
                            'value'=>'isset($data->code) ? ($data->code) : CHtml::Link("<i class=\"fa fa-gear\"></i>","javascript::void(0)",
                            array("class"=>"", 
                                  "onclick"=>"generateNIK($data->id)",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk Generate NIK Pegawai",
                            ))',
                            'htmlOptions'=>array(
                                'style'=>'text-align:center;'
                            )
                        ),
                        'name',
                        'address',
                        'handphone',
                        array(
                            'header'=>'Jam Kerja',
                            'name'=>'shift_name',
                            'type'=>'raw',
                            'value'=>'isset($data->shift_id) ? ucwords($data->shift->name) : "-"',
                        ),
                        array(
                            'header'=>Yii::t('zii','Detail'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{view}',
                            'buttons'=>array(
                                    'view' => array(),
                             ),
                        ),
                        array(
                            'header'=>'Cetak Kartu Pegawai',
                            'type'=>'raw',
                            'value'=>'!isset($data->code) ? "Generate NIK" : CHtml::Link("<i class=\"fa fa-print\"></i>","javascript::void(0)",
                            array("class"=>"", 
                                  "onclick"=>"printEmployeeCard($data->id)",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk Cetak Kartu Pegawai",
                            ))',
                            'htmlOptions'=>array(
                                'style'=>'text-align:center;'
                            )
                        ),
                        array(
                            'header'=>Yii::t('zii','Ubah'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{update}',
                            'buttons'=>array(
                                    'update' => array(),
                            ),
                        ),
                        array(
                            'header'=>Yii::t('zii','Hapus'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{delete}',
                            'buttons'=>array(
                                 'delete'=> array(
                                    'url'=>'Yii::app()->createUrl("/'.Yii::app()->controller->id.'/nonActive",array("id"=>$data->id))',
                                ),
                            )
                        ),
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                <?php echo Chtml::link('<i class="entypo-plus"></i> Tambah',$this->createUrl('/employee/create'), array('class' => 'btn btn-success')); ?>            </div>
        </div>
    </div>
</div>
<?php echo $this->renderPartial('_jsFunctions', array('model'=>$model)); ?>
