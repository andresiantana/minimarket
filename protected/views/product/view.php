<?php
$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('label'=>'List Product','url'=>array('index')),
    array('label'=>'Create Product','url'=>array('create')),
    array('label'=>'Update Product','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete Product','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage Product','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Lihat <small>Produk #<?php echo $model->name; ?></small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        'id',
                         array(
                             'label'=>'Kategori',
                             'value'=>isset($model->category_id) ? $model->category->name : "",
                         ),
                        'name',
                        'barcode',
                        'unit',
                        array(
                            'label'=>'Tanggal Kadaluarsa',
                            'value'=>isset($model->expire_date) ? $format->formatDateTimeForUser($model->expire_date) : "-",
                        ),
                        'stock',
                        'min_stock',
                        'min_purchase',
                        // 'price',
                        array(
                            'name'=>'price_sale',
                            'value'=>isset($model->price_sale) ? "Rp. ".Formatter::formatNumberForCurrency($model->price_sale) : "-",
                        ),                        
                        array(
                            'label'=>'Diskon (%)',
                            'value'=>isset($model->discount) ? Formatter::formatNumberForCurrency($model->discount) : "-",
                        ),                        
                        // array(
                        //     'label'=>'Status Delete',
                        //     'value'=>isset($model->is_delete) ? "Aktif" : "Tidak Aktif",
                        // ),
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                    <?php // echo CHtml::link(Yii::t('mds','{icon} Ubah',array('{icon}'=>'<i class="fa fa-edit"></i>')),$this->createUrl('update',array('id'=>$model->id,)), array('class'=>'btn btn-success')); ?>
                    <?php echo CHtml::link(Yii::t('mds','{icon} Daftar Produk',array('{icon}'=>'<i class="fa fa-list"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>
