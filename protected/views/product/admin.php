<?php
$this->breadcrumbs=array(
            'Products'=>array('index'),
            'Manage',
    );

$this->menu=array(
    array('label'=>'List Product','url'=>array('index')),
    array('label'=>'Create Product','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('product-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Daftar <small>Produk</small>
        </h1>
        <?php 
            // if(isset($_GET['id'])){
                $this->widget('booster.widgets.TbAlert');            
            // }
        ?>
        <?php            
            if(count($modProduct) > 0){
                foreach($modProduct as $i=>$product){  
                    $tgl = date('Y-m-d');
                    $expire_date = $product->expire_date;
                    $warning = '';

                    $tgl_kadaluarsa_awal_1 = strtotime($tgl.' + 1 month');
                    $tgl_kadaluarsa_awal_1 = date('Y-m-d',$tgl_kadaluarsa_awal_1);

                    $tgl_kadaluarsa_awal_2 = strtotime($tgl.' + 3 month');
                    $tgl_kadaluarsa_akhir = strtotime($tgl.' + 1 month');

                    $tgl_kadaluarsa_awal_2 = date('Y-m-d',$tgl_kadaluarsa_awal_2);
                    $tgl_kadaluarsa_akhir = date('Y-m-d',$tgl_kadaluarsa_akhir);

                    $tgl_kadaluarsa_awal_3 = strtotime($tgl.' + 3 month');
                    $tgl_kadaluarsa_awal_3 = date('Y-m-d',$tgl_kadaluarsa_awal_3);

                    if($expire_date < $tgl_kadaluarsa_awal_1){
                        $warning = " <div class='alert alert-danger'>
                                    <a href='#' class='close' data-dismiss='alert'>×</a>
                                    <strong>Peringatan!</strong> Produk ".$product->name." akan kadaluarsa kurang dari 1 bulan lagi.
                                </div>";
                    }else if(($expire_date <= $tgl_kadaluarsa_awal_2 ) && ($expire_date >= $tgl_kadaluarsa_akhir)){
//                        $warning = " <div class='alert alert-warning'>
//                                    <strong>Peringatan!</strong> Produk ".$product->name." akan kadaluarsa 1-3 bulan lagi.
//                                </div>";
                    }          
                    
                    // echo $warning;
                    if($product->stock <= $product->min_stock){    
        ?>

            <?php //$this->widget('booster.widgets.TbAlert'); ?>
                <!-- <div class="alert alert-info">
                    <strong>Peringatan!</strong> Produk <?php echo $product->name; ?> sudah mencapai stok minimal = <?php echo $product->min_stock; ?>. Silahkan lakukan pembelian produk
                </div> -->
                    <?php } ?>
                   
            <?php } ?>
        <?php } ?>        
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo CHtml::link('Pencarian','#',array('class'=>'search-button btn')); ?>
                <div class="search-form" style="display:none">
                        <?php $this->renderPartial('_search',array(
                        'model'=>$model,
                )); ?>
                </div><!-- search-form -->
            </div>
            <div class="panel-body" style="overflow:scroll;max-width:1200px;">
            <?php if (Yii::app()->user->getState('role_id') == Params::ROLE_GUDANG) { ?>
                <?php $this->widget('booster.widgets.TbGridView',array(
                    'id'=>'product-grid',
                    'dataProvider'=>$model->searchProduct(),
                    'filter'=>$model,
                    'template'=>"{summary}\n{items}\n{pager}",
                    'itemsCssClass'=>'table table-striped table-bordered table-hover',
                    'columns'=>array(
                        array(
                            'header'=>'No.',
                            'value' => '($this->grid->dataProvider->pagination) ?
                                            ($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
                                            : ($row+1)',
                            'type'=>'raw',
                            'htmlOptions'=>array('style'=>'text-align:right;width:5%;'),
                        ),
                        array(
                            'header'=>'Kategori',
                            'name'=>'category_name',
                            'type'=>'raw',
                            'value'=>'isset($data->category_id) ? $data->category->name : "-"',
                        ),
                        array(
                            'header'=>'Nama',
                            'name'=>'name',
                            'type'=>'raw',
                            'value'=>'isset($data->name) ? $data->Name : "-"',
                        ),
                        'barcode',
                        'unit',
                        'stock',
                        array(
                            'header'=>'Harga Jual',
                            'name'=>'price_sale',
                            'type'=>'raw',
                            'value'=>'isset($data->price_sale) ? "Rp. ".Formatter::formatNumberForCurrency($data->price_sale) : "0"',
                        ),
                        array(
                            'header'=>'Diskon (%)',
                            'name'=>'discount',
                            'type'=>'raw',
                            'value'=>'isset($data->discount) ? Formatter::formatNumberForCurrency($data->discount) : "0"',
                        ),
                        array(
                            'header'=>'Tanggal Kadaluarsa',
                            'name'=>'expire_date',
                            'type'=>'raw',
                            'filter'=>$this->widget('DateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'expire_date',
                                        'mode'=>'date',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT,
                                        ),
                                        'htmlOptions'=>array('readonly'=>false, 'class' => 'form-control dtPicker3', 'readonly'=>false, 'style'=>'width:160px;','id'=>'expire_date','placeholder'=>"--Pilih--"),
                                        ),true
                                    ),
                            'value'=>'isset($data->expire_date) ? Formatter::formatDateTimeForUser($data->expire_date) : "-"',
                        ),
                        /*
                        'expire_date',
                        'stock',
                        'price',
                        'price_sale',
                        'is_delete',
                        */
                        array(
                            'header'=>Yii::t('zii','Detail'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{view}',
                            'buttons'=>array(
                                'view' => array(),
                             ),
            			),
			            array(
                            'header'=>Yii::t('zii','Ubah'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{update}',
                            'buttons'=>array(
                                    'update' => array(),
                             ),
            			),
                        array(
                            'header'=>Yii::t('zii','Hapus'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{delete}',
                            'buttons'=>array(
                                 'delete'=> array(                                    
                                    'url'=>'Yii::app()->createUrl("/'.Yii::app()->controller->id.'/nonActive",array("id"=>$data->id))',
                                ),
                            )
                        ),
                        array(
                            'header'=>'Cetak Label <br>'.CHtml::checkBox("printSemua", false, array("class"=>"inputFormTabel currency span2","onkeypress"=>"return $(this).focusNextInputField(event)", "onclick"=>"printSemua();")),
                            'value'=>'CHtml::checkBox("Product[id][]", false, array("value"=>$data->id,"class"=>"inputFormTabel printlabel span2","onkeypress"=>"return $(this).focusNextInputField(event)"))',
                            'type'=>'raw',
                        ),
                    ),
                    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                        jQuery(\'#expire_date\').datepicker(jQuery.extend({
                        showMonthAfterYear:false}, 
                        jQuery.datepicker.regional[\'id\'], 
                       {\'dateFormat\':\'dd M yy\',\'maxDate\':\'d\',\'timeText\':\'Waktu\',\'hourText\':\'Jam\',\'minuteText\':\'Menit\',
                       \'secondText\':\'Detik\',\'showSecond\':true,\'timeOnlyTitle\':\'Pilih Waktu\',\'timeFormat\':\'hh:mms\',
                       \'changeYear\':true,\'changeMonth\':true,\'showAnim\':\'fold\',\'yearRange\':\'-80y:+50y\'})); 
                    jQuery(\'#expire_date_date\').on(\'click\', function(){jQuery(\'#expire_date\').datepicker(\'show\');});
                }',
                    
                )); ?>
            </div>
            <div class='panel-footer'>
                <?php echo Chtml::link('<i class="entypo-plus"></i> Tambah',$this->createUrl('/product/create'), array('class' => 'btn btn-success')); ?>
                <?php echo Chtml::link('<i class="fa fa-print"></i> Cetak Label','#', array('class' => 'btn btn-info','onclick'=>"printLabel('PRINT');return false",)); ?>                
                <?php echo Chtml::link('<i class="fa fa-print"></i> Laporan Stok','#', array('class' => 'btn btn-info','onclick'=>"printStok('PRINT');return false",)); ?>                
            </div>
            <?php } elseif (Yii::app()->user->getState('role_id') == Params::ROLE_OWNER) {
                $this->widget('booster.widgets.TbGridView',array(
                    'id'=>'product-grid',
                    'dataProvider'=>$model->searchProduct(),
                    'filter'=>$model,
                    'template'=>"{summary}\n{items}\n{pager}",
                    'itemsCssClass'=>'table table-striped table-bordered table-hover',
                    'columns'=>array(
                        array(
                            'header'=>'No.',
                            'value' => '($this->grid->dataProvider->pagination) ?
                                            ($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
                                            : ($row+1)',
                            'type'=>'raw',
                            'htmlOptions'=>array('style'=>'text-align:right;width:5%;'),
                        ),
                        array(
                            'header'=>'Kategori',
                            'name'=>'category_name',
                            'type'=>'raw',
                            'value'=>'isset($data->category_id) ? $data->category->name : "-"',
                        ),
                        array(
                            'header'=>'Nama',
                            'name'=>'name',
                            'type'=>'raw',
                            'value'=>'isset($data->name) ? $data->Name : "-"',
                        ),
                        'barcode',
                        'unit',
                        'stock',
                        array(
                            'header'=>'Harga Jual',
                            'name'=>'price_sale',
                            'type'=>'raw',
                            'value'=>'isset($data->price_sale) ? "Rp. ".Formatter::formatNumberForCurrency($data->price_sale) : "0"',
                        ),
                        array(
                            'header'=>'Diskon (%)',
                            'name'=>'discount',
                            'type'=>'raw',
                            'value'=>'isset($data->discount) ? Formatter::formatNumberForCurrency($data->discount) : "0"',
                        ),
                        array(
                            'header'=>'Tanggal Kadaluarsa',
                            'name'=>'expire_date',
                            'type'=>'raw',
                            'filter'=>$this->widget('DateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'expire_date',
                                        'mode'=>'date',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT,
                                        ),
                                        'htmlOptions'=>array('readonly'=>false,'class' => 'form-control dtPicker3', 'readonly'=>false, 'style'=>'width:160px;','id'=>'expire_date','placeholder'=>"--Pilih--"),
                                        ),true
                                    ),
                            'value'=>'isset($data->expire_date) ? Formatter::formatDateTimeForUser($data->expire_date) : "-"',
                        ),
                        array(
                            'header'=>Yii::t('zii','Detail'),
                            'class'=>'booster.widgets.TbButtonColumn',
                            'template'=>'{view}',
                            'buttons'=>array(
                                'view' => array(),
                             ),
                        ),
                    ),
                    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                            jQuery(\'#expire_date\').datepicker(jQuery.extend({
                            showMonthAfterYear:false}, 
                            jQuery.datepicker.regional[\'id\'], 
                           {\'dateFormat\':\'dd M yy\',\'maxDate\':\'d\',\'timeText\':\'Waktu\',\'hourText\':\'Jam\',\'minuteText\':\'Menit\',
                           \'secondText\':\'Detik\',\'showSecond\':true,\'timeOnlyTitle\':\'Pilih Waktu\',\'timeFormat\':\'hh:mms\',
                           \'changeYear\':true,\'changeMonth\':true,\'showAnim\':\'fold\',\'yearRange\':\'-80y:+50y\'})); 
                        jQuery(\'#expire_date_date\').on(\'click\', function(){jQuery(\'#expire_date\').datepicker(\'show\');});
                    }',
                )); ?>
            </div>
        <?php } ?>       
    </div>     
</div>
<?php $this->renderPartial('_jsFunctions',array('model'=>$model)); ?>