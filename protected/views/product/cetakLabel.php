<table border="0" align="center" width="100%">
    <tr>
        <td>
            <div class="row">
                <?php 
                    foreach($model as $key=>$detail){
                ?>
                        <div class="col-md-3" style='width:270px;margin: 10px;float:left;'>
                            <table border="2">
                                <tr>
                                    <td>
                                        <table style="font-family:arial black;word-wrap: break-word;" width="270px" height="50px">
                                            <tr>
                                                <td style="text-align:left;font-size:8pt;" colspan="2"><strong><?php echo isset($detail->name) ? $detail->name : ""; ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left;font-size:15pt;" colspan="2"><strong>Rp. <?php echo isset($detail->price_sale) ? str_replace(",",".",Formatter::formatNumberForCurrency($detail->price_sale)) : "0"; ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left;font-size:8pt;"><strong><?php echo date('d/m/Y'); ?></strong></td>
                                                <td style="text-align:right;font-size:8pt;"><?php echo isset($detail->barcode) ? $detail->barcode : ""; ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                <?php } ?>
            </div>
        </td>
    </tr>
</table>