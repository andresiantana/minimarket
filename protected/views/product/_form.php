<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'product-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus' => '#'.CHtml::activeId($model,'barcode'),
        'htmlOptions'=>array('class'=>'form','onsubmit'=>'return requiredCheck(this);' ,'onKeyPress'=>'return disableKeyPress(event);', 'enctype' => 'multipart/form-data'),
)); ?>

    <p class="help-block">Inputan dengan tanda <span class="required">*</span> harus diisi.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'barcode',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50,'onblur'=>'checkBarcode(this);','onkeyup'=>"return $(this).focusNextInputField(event)",)))); ?>
    
    <?php echo $form->dropDownListGroup($model,'category_id', array('widgetOptions'=>array('data'=>CHtml::listData(Category::model()->findAll('is_delete = 0 order by name ASC'),'id','name'),'htmlOptions'=>array('class'=>'span5','empty'=>'--Pilih Kategori--')))); ?>

    <?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

    <?php echo $form->textFieldGroup($model,'unit',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
    
    <div class="form-group ">
        <?php echo $form->labelEx($model,'expire_date', array('class'=>'col-sm-3 control-label')) ?>
        <div class="col-sm-3">
        <?php   
            $model->expire_date = isset($model->expire_date) ? $format->formatDateTimeForUser($model->expire_date) : "";
            $this->widget('DateTimePicker',array(
                'model'=>$model,
                'attribute'=>'expire_date',
                'mode'=>'date',
                'options'=> array(
                    'dateFormat'=>Params::DATE_FORMAT,
                ),
                'htmlOptions'=>array(
                    //'readonly'=>true,
                    'class'=>'span3 form-control',
                ),
            ));
            $model->expire_date = isset($model->expire_date) ? $format->formatDateTimeForDb($model->expire_date) : "";
        ?>
        </div>
    </div>

    <?php echo $form->textFieldGroup($model,'stock',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5 integer','style'=>'text-align:left;')))); ?>

    <?php echo $form->textFieldGroup($model,'price_sale',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5 integer','style'=>'text-align:left;')))); ?>
    
    <?php echo $form->textFieldGroup($model,'discount',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5 integer','style'=>'text-align:left;','onblur'=>'cekDiskon(this);')))); ?>
    
    <?php echo $form->textFieldGroup($model,'min_stock',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5 integer','style'=>'text-align:left;')))); ?>

    <?php echo $form->textFieldGroup($model,'min_purchase',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5 integer','style'=>'text-align:left;')))); ?>

    <?php if(isset($id)){ ?>
        <?php echo $form->checkBoxGroup($model,'is_delete',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
    <?php } ?>

<div class="form-group">
    <div class="col-lg-4">
        <?php $this->widget('booster.widgets.TbButton', array(
                'buttonType'=>'submit',
                'context'=>'primary',
                'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
                'htmlOptions'=>array(
                    'onkeypress'=>'formSubmit(this,event)',
                )
        )); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Daftar Produk',array('{icon}'=>'<i class="fa fa-list"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

<?php $this->renderPartial('_jsFunctions',array('model'=>$model)); ?>
