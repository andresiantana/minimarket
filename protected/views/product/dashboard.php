<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Data <small>Statistik</small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
        <ol class="breadcrumb">
          <?php $profile = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET); ?>
          <li class="active">Berikut ini adalah data statistik minimarket <b><?php echo isset($profile->name) ? strtoupper($profile->name) : "CAHAYA MUDA"; ?></b> :</li>
        </ol>
    </div>
</div>

<?php $this->renderPartial('grafik',array('model'=>$model, 'dataBarLineChart'=>$dataBarLineChart,'graphs' => $graphs)); ?>

<div class="row">
    <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="panel panel-primary text-center no-boder bg-color-green green">
            <div class="panel-left center green">
                <i class="fa fa-calendar fa-5x"></i>
            </div>
            <div class="panel-right">
               <h3><font color="green"><?php echo isset($total_1) ? $total_1 : 0; ?></font> Produk</h3>
               <strong>Diatas 3 bulan akan kadaluarsa</strong>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="panel panel-primary text-center no-boder bg-color-brown">
            <div class="panel-left pull-left brown">
                <i class="fa fa-calendar fa-5x"></i>
            </div>
            <div class="panel-right">
                <h3><font color="orange"><?php echo isset($total_2) ? $total_2 : 0; ?></font> Produk</h3>
                <strong>Antara 1 - 3 bulan akan kadaluarsa</strong>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="panel panel-primary text-center no-boder bg-color-red">
            <div class="panel-left pull-left red">
                <i class="fa fa fa-calendar fa-5x"></i>
            </div>
            <div class="panel-right">
               <h3><font color="red"><?php echo isset($total_3) ? $total_3 : 0; ?></font> Produk</h3>
               <strong>Dibawah 1 bulan akan kadaluarsa </strong>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="panel panel-primary text-center no-boder bg-color-blue">
            <div class="panel-left pull-left blue">
                <i class="fa fa-shopping-cart fa-5x"></i>
            </div>
            <div class="panel-right">
                <h3><?php echo 'Rp. '.Formatter::formatNumberForCurrency($sale); ?>,-</h3>
               <strong>Total keseluruhan transaksi penjualan</strong>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="panel panel-primary text-center no-boder bg-color-purple">
              <div class="panel-left pull-left purple">
                <i class="fa fa-shopping-cart fa-5x"></i>
                </div>
            <div class="panel-right">
               <h3><?php echo 'Rp. '.Formatter::formatNumberForCurrency($order); ?>,-</h3>
               <strong>Total keseluruhan transaksi pembelian</strong>
            </div>
        </div>
    </div>
</div>