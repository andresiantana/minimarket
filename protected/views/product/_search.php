<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type'=>'horizontal',
        'id'=>'search-form',
	'htmlOptions'=>array('class'=>'form'),
)); ?>

        <?php echo $form->dropDownListGroup($model,'category_id', array('widgetOptions'=>array('data'=>CHtml::listData(Category::model()->findAll(),'id','name'),'htmlOptions'=>array('class'=>'span5','empty'=>'--Pilih Kategori--')))); ?>

        <?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

        <?php echo $form->textFieldGroup($model,'barcode',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
        
        <div class="form-group">
            <?php echo CHtml::label('Tgl. Kadaluarsa','',array('class'=>'col-sm-3 control-label')); ?>
            <div class="col-sm-3">
                <?php echo $form->dropDownList($model,'periode_kadaluarsa',array('1'=>'< 1 Bulan','2'=>'1-3 Bulan','3'=>'> 3 Bulan'), array('class'=>'form-control','empty'=>'--Pilih Periode--')); ?>
            </div>
        </div>
	<div class="form-group">
            <div class="col-lg-4">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Cari',
		)); ?>
            </div>
	</div>

<?php $this->endWidget(); ?>
