<script type="text/javascript">
    function checkBarcode(obj){
        var barcode = obj.value;
        var id = '<?php echo isset($_GET['id']) ? $_GET['id'] : null; ?>';
        if(id == ''){            
            if(barcode != ''){
                $.ajax({
                    type:'POST',
                    url:'<?php echo $this->createUrl('setBarcode'); ?>',
                    data: {barcode:barcode},//
                    dataType: "json",
                    success:function(data){
                        if(data.status != ''){
                            alert(data.pesan);
                            $('#<?php echo CHtml::activeId($model,'barcode'); ?>').val("");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
                });
            }
        }
    }
    
    function PrintPriceTag(obj){
        parent = $(obj).parents("tr");
        if ($("#cetaklabel").is(":checked")) {
             var idBrg = parent.find("#id").val();
             alert(idBrg);
        }else{

        }        
    }
    
    function printLabel(caraPrint){
        var i = 0;
        $('.printlabel').each(function(){
            if ($(this).is(":checked")) {
                i++;
            }
        })
        
        if(i > 0){
            window.open("<?php echo $this->createUrl('printLabel') ?>&"+$('.printlabel').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px,scrollbars=1');    
        }else{
            alert('Silahkan ceklis Cetak Label pada Produk terlebih dahulu.');
        }
        
    }
    
    function printStok(caraPrint){
        window.open("<?php echo $this->createUrl('printStok'); ?>/"+$('#search-form').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=1100px, scrollbars=yes');
    }

    function printSemua(){
        if ($("#printSemua").is(":checked")) {
            $('.printlabel').attr('checked',true);
        }else{
            $('.printlabel').removeAttr('checked',true);
        }   
    }
    
    function cekDiskon(obj){
        unformatNumberSemua();
        var diskon = $(obj).val();
        if(diskon > 100){
            alert('Nominal Diskon tidak boleh lebih dari 100%');
            $(obj).val(0);
            return false;
        }
    }
    
    $(document).ready(function(){
       formatNumberSemua();         
    });
</script>