<style>
    table{
        margin-bottom: 0px;
    }
    .form-actions{
        padding:4px;
        margin-top:5px;
    }
    .nav-tabs>li>a{display:block; cursor:pointer;}
    .nav-tabs > .active a:hover{cursor:pointer;}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="search-form">
                <?php 
                    $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
                        'action' => Yii::app()->createUrl($this->route),
                        'method' => 'get',
                        'type' => 'horizontal',
                        'id' => 'search-laporan',
                        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
                    ));
                ?>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group ">
                                <?php echo CHtml::label('Periode', 'periode', array('class' => 'col-sm-3 control-label')) ?>
                                <div class="col-sm-3">
                                    <?php echo $form->dropDownList($model, 'periode', array('tahun' => 'Tahun', 'bulan' => 'Bulan', 'custom' => 'Custom'), array('onchange' => 'ubahJnsPeriode();','class'=>'form-control','style'=>'width:100px;')); ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <?php echo CHtml::label('Tipe', 'tipe', array('class' => 'col-sm-3 control-label')) ?>
                                <div class="col-sm-3">
                                    <?php echo $form->dropDownList($model, 'tipe', array('penjualan' => 'Penjualan', 'pembelian' => 'Pembelian', 'laba_rugi' => 'Laba/Rugi'), array('class'=>'form-control','style'=>'width:140px;')); ?>
                                </div>
                            </div>                            
                        </div>  
                        <div class="col-md-4">
                            <div class='form-group custom'>
                                <?php echo CHtml::label('Dari Tanggal', 'dari_tanggal', array('class' => 'col-sm-3 control-label')) ?>
                                <div class="col-sm-3">  
                                    <?php
                                    $this->widget('DateTimePicker', array(
                                            'model' => $model,
                                            'attribute' => 'start_date',
                                            'mode' => 'date',
                                            'options' => array(
                                                    'dateFormat' => Params::DATE_FORMAT,
                                                    'maxDate' => 'd',
                                            ),
                                            'htmlOptions' => array('readonly' => true,'class' => 'form-control dtPicker3', 'readonly'=>false, 'style'=>'width:160px;',
                                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                                    ));
                                    ?>
                                </div> 
                            </div>
                            <div class='form-group bulan'>
                                <?php echo CHtml::label('Dari Bulan', 'dari_tanggal', array('class' => 'col-sm-3 control-label')) ?>
                                <div class="col-sm-3">  
                                    <?php
                                        $this->widget('MyMonthPicker', array(
                                                'model' => $model,
                                                'attribute' => 'start_month',
                                                'options' => array(
                                                        'dateFormat' => Params::MONTH_FORMAT,
                                                ),
                                                'htmlOptions' => array('readonly' => true, 'class' => 'form-control dtPicker3', 'readonly'=>false, 'style'=>'width:160px;',
                                                        'onkeypress' => "return $(this).focusNextInputField(event)"),
                                        ));
                                    ?>
                                </div> 
                            </div>
                            <div class='form-group tahun'>
                                <?php echo CHtml::label('Dari Tahun', 'dari_tanggal', array('class' => 'col-sm-3 control-label')) ?>
                                <div class="col-sm-3">
                                    <?php
                                    echo $form->dropDownList($model, 'start_year', CustomFunction::getTahun(null, null), array('class'=>'form-control', 'onkeypress' => "return $(this).focusNextInputField(event)",'style'=>'width:100px;'));
                                    ?>
                                </div>
                            </div>
                            
                            <!-- tombol sampai dengan -->
                            <div class='form-group custom'>
                                <?php echo CHtml::label('Sampai Dengan', 'sampai_dengan', array('class' => 'col-sm-3 control-label')) ?>
                                <div class="col-sm-3">  
                                    <?php
                                    $this->widget('DateTimePicker', array(
                                            'model' => $model,
                                            'attribute' => 'end_date',
                                            'mode' => 'date',
                                            'options' => array(
                                                    'dateFormat' => Params::DATE_FORMAT,
                                                    'maxDate' => 'd',
                                            ),
                                            'htmlOptions' => array('readonly' => true, 'class' => 'form-control dtPicker3', 'readonly'=>false, 'style'=>'width:160px;',
                                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                                    ));
                                    ?>
                                </div> 
                            </div>
                            <div class='form-group bulan'>
                                <?php echo CHtml::label('Sampai Dengan', 'sampai_dengan', array('class' => 'col-sm-3 control-label')) ?>
                                <div class="col-sm-3">  
                                    <?php
                                    $this->widget('MyMonthPicker', array(
                                            'model' => $model,
                                            'attribute' => 'end_month',
                                            'options' => array(
                                                    'dateFormat' => Params::MONTH_FORMAT,
                                            ),
                                            'htmlOptions' => array('readonly' => true, 'class' => 'form-control dtPicker3', 'readonly'=>false, 'style'=>'width:160px;',
                                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                                    ));
                                    ?>
                                </div> 
                            </div>
                            <div class='form-group tahun'>
                                <?php echo CHtml::label('Sampai Dengan', 'sampai_dengan', array('class' => 'col-sm-3 control-label')) ?>
                                <div class="col-sm-3">
                                    <?php
                                    echo $form->dropDownList($model, 'end_year', CustomFunction::getTahun(null, null), array('onkeypress' => "return $(this).focusNextInputField(event)",'class'=>'form-control','style'=>'width:100px;'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">                            
                            <div class="form-group">
                                <div class="col-lg-4">
                                    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Cari', array('{icon}' => '<i class="fa fa-search"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan')); ?>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
                <div id="Grafik">
                <?php $this->renderPartial('_grafik',array('model'=>$model, 'dataBarLineChart'=>$dataBarLineChart,'graphs' => $graphs)); ?>
                </div>
            </div>
        </div>
    </div>
</div>  

<?php
$this->endWidget();
?>
<script type="text/javascript">
    function refreshForm() {
            window.location.href = "<?php echo Yii::app()->createUrl($this->route); ?>";
    }

    function ubahJnsPeriode() {
            var obj = $("#<?php echo CHtml::activeId($model, 'periode') ?>");
            if (obj.val() == 'custom') {
                    $('.custom').show();
                    $('.bulan').hide();
                    $('.tahun').hide();
            } else if (obj.val() == 'bulan') {
                    $('.custom').hide();
                    $('.bulan').show();
                    $('.tahun').hide();
            } else if (obj.val() == 'tahun') {
                    $('.custom').hide();
                    $('.bulan').hide();
                    $('.tahun').show();
            }
    }
    ubahJnsPeriode();
</script>