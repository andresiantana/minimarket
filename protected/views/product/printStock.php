<style>
    .header-report{
        line-height: 8px;
    }
    .body-report{
        line-height: 2em;
    }
    .tabel-stok tr, .tabel-stok td, .tabel-stok thead, .tabel-stok thead,th{
        border : 1px black solid;
        line-height: 7px !important;
        font-size: 11px !important;
    }
    .table > thead > tr > th{
        border : 1px black solid;
    }
</style>
<?php 
    if(isset($caraPrint)){
        //echo $this->renderPartial('_headerPrint',array('colspan'=>6));      
    }
?>
<div>
    <table align="center" cellpadding="0" cellspacing="0">
        <tr class="header-report">
            <td>
                <h5><b><center><?php echo isset($profile->name) ? strtoupper($profile->name).' TOSERBA' : "CAHAYA MUDA TOSERBA"; ?></center></b></h5>
                <h5><center><?php echo isset($judulLaporan) ? $judulLaporan : null; ?></center></h3>
                <h5><center>Periode: <?php echo isset($periode) ? $periode : null; ?></center></h5>
            </td>
        </tr>
    </table>
    <?php
         $this->widget('booster.widgets.TbGridView',array(
            'id'=>'product-grid',
            'dataProvider'=>$model->searchPrintStok(),
            'template'=>"{items}\n{pager}",
            'itemsCssClass'=>'tabel-stok',
            'columns'=>array(
                array(
                    'header'=>'<center>No.</center>',
                    'value' => '($this->grid->dataProvider->pagination) ?
                                    ($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
                                    : ($row+1)',
                    'type'=>'raw',
                    'htmlOptions'=>array('style'=>'text-align:center;width:5%;'),
                ),
                array(
                    'header'=>'<center>Nama</center>',
                    'type'=>'raw',
                    'value'=>'isset($data->name) ? $data->name : "-"',
                ),
                array(
                    'header'=>'<center>Stok</center>',
                    'type'=>'raw',
                    'value'=>'isset($data->stock) ? $data->stock : "-"',
                    'htmlOptions'=>array('style'=>'text-align:center;width:5%;'),
                ),
                array(
                    'header'=>'<center>Tanggal Kadaluarsa</center>',
                    'type'=>'raw',
                    'value'=>'isset($data->expire_date) ? Formatter::formatDateTimeForUser($data->expire_date) : "-"',
                    'htmlOptions'=>array('style'=>'text-align:center;width:15%;'),
                ),
            ),
        ));
    ?>
</div>
