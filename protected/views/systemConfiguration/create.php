<?php
$this->breadcrumbs=array(
	'System Configurations'=>array('index'),
	'Create',
);

$this->menu=array(
    array('label'=>'List SystemConfiguration','url'=>array('index')),
    array('label'=>'Manage SystemConfiguration','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Tambah <small>Konfigurasi Sistem</small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo $this->renderPartial('_form', array('model'=>$model)); ?></div>
        </div>
    </div>
</div>
