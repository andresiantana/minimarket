<?php
$this->breadcrumbs=array(
	'System Configurations'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('label'=>'List SystemConfiguration','url'=>array('index')),
    array('label'=>'Create SystemConfiguration','url'=>array('create')),
    array('label'=>'Update SystemConfiguration','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete SystemConfiguration','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage SystemConfiguration','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Lihat <small>Konfigurasi Sistem #<?php echo $model->name; ?></small>
        </h1>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        'name',
                        array(
                            'label'=>'Status',
                            'value'=>($model->is_active == 1) ? "Aktif" : "Tidak Aktif",
                        ),
                        array(
                            'label'=>'Create Time',
                            'value'=>isset($model->create_time) ? Formatter::formatDateTimeForUser($model->create_time) : "Tidak diset",
                        ),
                        array(
                            'label'=>'Update Time',
                            'value'=>isset($model->update_time) ? Formatter::formatDateTimeForUser($model->update_time) : "Tidak diset",
                        ),
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                <?php echo CHtml::link(Yii::t('mds','{icon} Konfigurasi Sistem',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>
