<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'system-configuration-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus' => '#name',
        'htmlOptions'=>array('class'=>'form','onKeyPress'=>'return disableKeyPress(event);', 'enctype' => 'multipart/form-data'),
)); ?>

<p class="help-block">Inputan dengan tanda <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('id'=>'name','class'=>'span3','maxlength'=>30,'style'=>'width:250px;')))); ?>
<?php echo $form->textAreaGroup($model,'address',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span3','style'=>'width:250px;height:100px;')))); ?>
<?php echo $form->textFieldGroup($model,'phone',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span3','maxlength'=>22,'style'=>'width:250px;')))); ?>
<div class="form-group ">
    <?php echo $form->labelEx($model,'is_absensi', array('class'=>'col-sm-3 control-label')) ?>
    <div class="col-sm-3">
        <?php echo CHtml::activeCheckBox($model,'is_absensi',array('class'=>'span5', 'onkeyup'=>"return $(this).focusNextInputField(event)","data-toggle"=>"toggle")); ?>
        <font size="2" class="help-block"><span class="required">*On / Off</span> - Fitur absensi pegawai.</font>
    </div>
</div>

<div class="form-group ">
    <?php echo $form->labelEx($model,'is_member', array('class'=>'col-sm-3 control-label')) ?>
    <div class="col-sm-3">
        <?php echo CHtml::activeCheckBox($model,'is_member',array('class'=>'span5', 'onkeyup'=>"return $(this).focusNextInputField(event)","data-toggle"=>"toggle")); ?>
        <font size="2" class="help-block"><span class="required">*On / Off</span> - Fitur Members.</font>
    </div>
</div>
<?php echo $form->textFieldGroup($model,'point_to_idr',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span3','maxlength'=>22,'style'=>'width:250px;')))); ?>
<?php //echo $form->textFieldGroup($model,'value_point',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span3','maxlength'=>22,'style'=>'width:250px;')))); ?>
<div class="form-group ">
    <?php //echo $form->labelEx($model,'is_active', array('class'=>'col-sm-3 control-label')) ?>
    <div class="col-sm-3">
        <?php //echo CHtml::activeCheckBox($model,'is_active',array('class'=>'span5', 'onkeyup'=>"return $(this).focusNextInputField(event)","data-toggle"=>"toggle")); ?>
    </div>
</div>
<?php //if(isset($_GET['id'])){ ?>
    <?php //echo $form->checkBoxGroup($model,'is_delete',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5', 'onkeyup'=>"return $(this).focusNextInputField(event)",)))); ?>
<?php //} ?>
<div class="form-group">
    <div class="col-lg-4">
        <?php $this->widget('booster.widgets.TbButton', array(
                'buttonType'=>'submit',
                'context'=>'primary',
                'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
        )); ?>
        <?php //echo CHtml::link(Yii::t('mds','{icon} Konfigurasi Sistem',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
