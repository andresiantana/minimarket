﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php $profile = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET); ?>
    <title><?php echo isset($profile->name) ? strtoupper($profile->name) : "CAHAYA MUDA"; ?></title>
    <link rel="icon" type="image/*" href="<?php echo Yii::app()->request->baseUrl; ?>/css/images/icon.png">
    <!-- Bootstrap Styles-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/hybrid/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/hybrid/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/hybrid/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/hybrid/assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/themes/hybrid/assets/css/font-apps.css" rel="stylesheet" />
    <!-- Toggle-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-toggle.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/themes/hybrid/assets/js/bootstrap.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.maskMoney.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.maskedinput.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/realtimeClock.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>    
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php $profile = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET); ?>
                <a class="navbar-brand" href=""><i class="fa fa-shopping-cart"></i> <strong><font style="font-size:16px;"><?php echo isset($profile->name) ? strtoupper($profile->name) : "CAHAYA MUDA"; ?></font></strong></a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span><font color="#FFFFFF">Selamat Datang </font><font color="#32C2CD"><b><?php echo ucwords(Yii::app()->user->getState('username')); ?></b></font><font color="#FFFFFF">. Waktu saat ini adalah <span id="jam">
                        <script language="javascript">
                            function jam() {
                                var waktu = new Date();
                                var jam = waktu.getHours();
                                var menit = waktu.getMinutes();
                                var detik = waktu.getSeconds();

                                // var ampm = jam >= 12 ? 'PM' : 'AM';
                                // jam = jam % 12;
                                // jam = jam ? jam : 12; // the hour '0' should be '12'
                                // menit = menit < 10 ? +menit : menit;

                                if (jam < 10) {
                                jam = "0" + jam;
                                }
                                if (menit < 10) {
                                menit = "0" + menit;
                                }
                                if (detik < 10) {
                                detik = "0" + detik;
                                }
                                var jam_div = document.getElementById('jam');
                                jam_div.innerHTML = jam + ":" + menit + ":" + detik + ' WIB';
                                setTimeout("jam()", 1000);
                            }
                            jam();
                        </script>
                    </span>
                    </font></span>
                </li>
                <li>&nbsp;</li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i><i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <?php if(Yii::app()->user->getState('role_id') != Params::ROLE_ADMIN){ ?>
                            <?php if(Yii::app()->user->getState('role_id') != Params::ROLE_OWNER){ ?>
                        <li>
                            <a href="<?php echo $this->createUrl('/site/updateUser',array('id'=>Yii::app()->user->id)); ?>"><i class="fa fa-user fa-fw"></i> Ubah Profil</a>
                        </li>
                            <?php } ?>
                        <li>
                            <a href="<?php echo $this->createUrl('/site/updatePassword',array('id'=>Yii::app()->user->id)); ?>"><i class="fa fa-key fa-fw"></i> Ubah Kata Sandi</a>
                        </li>
                        <?php if(Yii::app()->user->getState('role_id') == Params::ROLE_GUDANG || Yii::app()->user->getState('role_id') == Params::ROLE_KASIR){ ?>
                        <li>
                            <a href="<?php echo $this->createUrl('/site/updateShift',array('id'=>Yii::app()->user->id)); ?>"><i class="fa fa-clock-o fa-fw"></i> Ubah Jam Kerja</a>
                        </li>
                        <?php } ?>
                        <li class="divider"></li>
                        <?php } ?>
                        <li>
                            <a href="<?php echo $this->createUrl('/site/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Keluar</a>
                        </li>
                    </ul>
                </li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div id="sideNav" href=""><i class="fa fa-chevron-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <?php
                        $idModul = ((!empty($this->module->id)) ? $this->module->id : null);
                        $idUser = ((!empty(Yii::app()->user->id)) ? Yii::app()->user->id : null);
                        $menu = Params::menu();
                        $active = '';
                        if(isset($menu)){
                        foreach ($menu as $index => $arrMenu){
                    ?>
                        <li></li>
                        <li>
                            <?php
                                if(isset($arrMenu['sub']) && count($arrMenu['sub']) > 0){
                            ?>
                            <a href="#">
                                <i class="<?php echo $arrMenu['icon']; ?>"></i>
                                <?php echo $arrMenu['label']; ?>
                                <span class="fa fa-chevron-down"></span>
                            </a>
                            <?php } else { ?>
                                <a href="<?php echo $arrMenu['url']; ?>" class="<?php echo ($arrMenu['active'] == 1) ? "active-menu" : ""; ?>">
                                    <i class="<?php echo $arrMenu['icon']; ?>"></i>
                                    <?php echo $arrMenu['label']; ?>
                                </a>
                            <?php } ?>
                            <?php
                                if(isset($arrMenu['sub'])){ ?>
                                <ul class="nav nav-second-level">
                                <?php 
                                    foreach ($arrMenu['sub'] as $i => $arr) { ?>
                                        <li>
                                            <a href="<?php echo $this->createUrl($arr['url']); ?>" class="<?php echo ($arrMenu['active'] == 1) ? "active-menu" : ""; ?>">
                                                <span><?php echo $arr['label']; ?></span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <?php echo $content; ?>
                </div>
                <footer>
                    <p><font size="2"><b class="copyright">&copy; 2017 Sistem Informasi Minimarket </b>| <a href="https://www.linkedin.com/in/andresiantana" target="_blank">Andre Siantana</a></font></p>
                </footer>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- Bootstrap Js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/hybrid/assets/js/bootstrap.min.js"></script>
    <!-- Toggle Js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-toggle.min.js"></script>
    <!-- Metis Menu Js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/hybrid/assets/js/jquery.metisMenu.js"></script>
    <!-- Custom Js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/hybrid/assets/js/custom-scripts.js"></script>
</body>
</html>
<script type="text/javascript">
    <?php if(empty(Yii::app()->user->id)){ ?>
        function noBack()
        {
            window.history.forward()
        }
        noBack();
        window.onload = noBack;
        window.onpageshow = function(evt) { if (evt.persisted) noBack() }
        window.onunload = function() { void (0) }
    <?php } ?>
    
    $(document).ready(function(){
        var path = window.location.href;
        path = path.replace("http://localhost", "");        
        path = decodeURIComponent(path);
        $('.nav a').each(function(){
            var href = $(this).attr('href');
            if(path.substring(0,href.length) === href){
                $(this).addClass('active-menu');
            }
        }); 
    });
</script>