<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.maskMoney.js'); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.maskedinput.js'); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/realtimeClock.js'); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
	
    <?php $profile = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET); ?>
    <title><?php echo isset($profile->name) ? strtoupper($profile->name) : "CAHAYA MUDA"; ?></title>
    <link rel="icon" type="image/*" href="<?php echo Yii::app()->request->baseUrl; ?>/css/images/icon.png">
</head>

<?php
    if(stripos($_GET['r'], 'antrian') !== false){
        echo '<body style="background: transparent;">';
    }else{
        echo '<body style="background:#F5F6F9;padding:7px;">';
    }
?>
<style>
    /*untuk label yg bisa refresh*/
    label.refreshable:hover{
        cursor:pointer;
        color:#0000FF;
        font-weight: bold;
    }
    .container{
        border-color:#fff;
    }
</style>
<body>

<div style="width: 100%;">
    <?php echo $content; ?>    
</div>

</body>
<?php
Yii::app()->clientScript->registerScript('resizeBody','
		document.body.style.height = "10px";
', CClientScript::POS_END);
?>
</html>
<script type="text/javascript">