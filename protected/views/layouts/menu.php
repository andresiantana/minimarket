<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <?php $profile = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET); ?>
    <title><?php echo isset($profile->name) ? strtoupper($profile->name) : "CAHAYA MUDA"; ?></title>
    <link rel="icon" type="image/*" href="<?php echo Yii::app()->request->baseUrl; ?>/css/images/icon.png">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/themes/login/css/style.css">
</head>
<body>
  <div class="container">
      <div class="row">
        <div class="col-sm-4 col-sm-push-4">
          <div class="loader">
            <div class="spinner"></div>
            <div class="spinner2"></div>
          </div>
          <a href="#" class="checkmark">
            <span>
              <span class="s1"></span>
              <span class="s2"></span>
              <span class="s3"></span>
            </span>
          </a>
        <br/><br/><br/><br/>
        <?php echo $content; ?>
        </div>
    </div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js'></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/themes/login/js/index.js"></script>
</body>
</html>