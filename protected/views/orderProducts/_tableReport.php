<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'order-report-grid',
    'dataProvider'=>$model->searchReport(),
//    'filter'=>$model,
    'template'=>"{summary}\n{items}\n{pager}",
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
    'columns'=>array(
        array(
            'header'=>'No.',
            'value' => '($this->grid->dataProvider->pagination) ? 
                            ($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
                            : ($row+1)',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:right;width:5%;'),
        ),
        array(
            'header'=>'Tanggal Pembelian',
            'type'=>'raw',
            'value'=>'isset($data->date) ? Formatter::formatDateTimeForUser($data->date) : ""',
        ),
        array(
            'header'=>'No. Faktur',
            'type'=>'raw',
            'value'=>'isset($data->number) ? $data->number : ""',
        ),
        array(
            'header'=>'Total Harga (Rp.)',
            'type'=>'raw',
            'value'=>'isset($data->total_price) ? Formatter::formatNumberForCurrency($data->total_price) : ""',
            'htmlOptions'=>array('style'=>'text-align:right'),
        ),
        array(
            'header'=>'Total Diskon (Rp.)',
            'type'=>'raw',
            'value'=>'isset($data->total_discount) ? Formatter::formatNumberForCurrency($data->total_discount) : ""',
            'htmlOptions'=>array('style'=>'text-align:right'),
        ),
        array(
            'header'=>'Total Pembayaran (Rp.)',
            'type'=>'raw',
            'value'=>'isset($data->sub_total) ? Formatter::formatNumberForCurrency($data->sub_total) : ""',
            'htmlOptions'=>array('style'=>'text-align:right'),
        ),
        array(
            'header'=>'Pegawai Gudang',
            'type'=>'raw',
            'value'=>'isset($data->employee_id) ? $data->employee->name : ""',
        ),
        array(
            'header'=>'Detail Pembelian',
            'type'=>'raw', 
            'value'=>'CHtml::Link("<i class=\"fa fa-eye\"></i>",Yii::app()->controller->createUrl("orderProducts/detailPembelian",array("id"=>$data->id,"frame"=>1)),
                        array("class"=>"", 
                              "target"=>"iframeDetail",
                              "onclick"=>"$(\"#dialogDetailPembelian\").dialog(\"open\");",
                              "rel"=>"tooltip",
                              "title"=>"Klik untuk lihat detail pembelian",
                        ))',
            'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
        ),
    ),
)); ?>

<?php 
    // Dialog detail pembelian =========================
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
        'id'=>'dialogDetailPembelian',
        'options'=>array(
            'title'=>'Detail Pembelian',
            'autoOpen'=>false,
            'modal'=>true,
            'zIndex'=>1002,
            'width'=>980,
            'height'=>400,
            'resizable'=>false,
        ),
    ));
    ?>
    <iframe src="" name="iframeDetail" width="100%" height="550">
    </iframe>
    <?php
    $this->endWidget();
    //========= end detail pembelian ======================
    ?>