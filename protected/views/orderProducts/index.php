<?php
$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Create',
);

$this->menu=array(
    array('label'=>'List Order','url'=>array('index')),
    array('label'=>'Manage Order','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Transaksi <small>Pembelian</small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo $this->renderPartial('_form', array('model'=>$model,'modDetails'=>$modDetails,'format'=>$format)); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->renderPartial('_jsFunctions', array('model'=>$model,'modDetails'=>$modDetails)); ?>