<div class="form-group ">
    <?php echo CHtml::label('Barcode', 'barcode', array('class'=>'col-sm-3 control-label')); ?>
    <div class="col-sm-3">
        <?php echo CHtml::hiddenField('id','',array()); ?>
        <?php echo CHtml::hiddenField('name','',array()); ?>
    <?php
        $this->widget('JuiAutoComplete', array(
            'name'=>'barcode',
            'id'=>'barcode',
            'source'=>'js: function(request, response) {
                $.ajax({
                    url: "'.$this->createUrl('AutocompleteProduct').'",
                    dataType: "json",
                    data: {
                        barcode: request.term,
                    },
                    success: function (data) {
                         response(data);
                    }
                })
             }',
             'options'=>array(
                   'showAnim'=>'fold',
                   'minLength' => 2,
                   'focus'=> 'js:function( event, ui ) {
                        $(this).val("");
                        return false;
                    }',
                   'select'=>'js:function( event, ui ) {
                        $(this).val(ui.item.value);
                        $("#id").val(ui.item.id);
                        $("#name").val(ui.item.name);
                        $("#barcode").val(ui.item.barcode);
                        addProduct(ui.item.id,ui.item.barcode);
                        return false;
                    }',
            ),
            'htmlOptions'=>array(
                'onkeyup'=>"return $(this).focusNextInputField(event)",
                'onblur'=>"if($(this).val()=='') clearFormProduct(); else addProduct('',this.value)",
                'class'=>'span5 form-control',
                'id'=>'barcode',
            ),
            'tombolDialog'=>array('idDialog'=>'dialogProduct'),
        ));
        ?>
    </div>
</div>
<br>
<table class="table table-bordered table-striped" id="table-product">
    <thead>
        <tr>
            <th>Kategori</th>
            <th>Nama Produk</th>
            <th>Jumlah</th>
            <th>Harga</th>
            <th>Diskon (Rp.)</th>
            <th>Sub Total</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(count($modDetails) > 0){
                $modProduct = $modDetails;
                foreach($modProduct as $i=>$modDetails){
                    echo $this->renderPartial('_rowProductDetails', array('form'=>$form,'model'=>$model,'modDetails'=>$modDetails,'format'=>$format));
                }
            }
        ?>
    </tbody>
</table>
<br/><br/>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="col-sm-6">
            <?php echo $form->labelEx($model,'total_price', array('class'=>'col-sm-6 control-label','style'=>'margin-left:60px;font-size:10pt;')) ?>
        </div>
        <div class="col-sm-6">
            
            <div class="form-group input-group">
                <span class="input-group-addon">Rp.</span>
                <?php echo $form->textField($model,'total_price',array('class'=>'form-control integer','readonly'=>true,'onkeyup'=>"return $(this).focusNextInputField(event)",)); ?>
            </div>
        </div>
        
        <div class="col-sm-6">
            <?php echo $form->labelEx($model,'total_discount', array('class'=>'col-sm-6 control-label','style'=>'margin-left:60px;font-size:10pt;')) ?>
        </div>
        <div class="col-sm-6">
            
            <div class="form-group input-group">
                <span class="input-group-addon">Rp.</span>
                <?php echo $form->textField($model,'total_discount',array('class'=>'form-control integer','readonly'=>true,'onkeyup'=>"return $(this).focusNextInputField(event)",)); ?>
            </div>
        </div>
        
        <div class="col-sm-6">
            <?php echo $form->labelEx($model,'sub_total', array('class'=>'col-sm-6 control-label','style'=>'margin-left:60px;font-size:10pt;')) ?>
        </div>
        <div class="col-sm-6">
            
            <div class="form-group input-group">
                <span class="input-group-addon">Rp.</span>
                <?php echo $form->textField($model,'sub_total',array('class'=>'form-control integer','readonly'=>true,'onkeyup'=>"return $(this).focusNextInputField(event)",)); ?>
            </div>
        </div> 
    </div>
</div>

<?php
//========= Dialog buat cari data produk =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogProduct',
    'options'=>array(
        'title'=>'Daftar Produk',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>980,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modProduct = new Product('searchProduct');
$modProduct->unsetAttributes();
if(isset($_GET['Product'])){
    $modProduct->attributes = $_GET['Product'];
    $modProduct->name = $_GET['Product']['name'];
    $modProduct->category_name = $_GET['Product']['category_name'];
}
$this->widget('booster.widgets.TbGridView',array(
	'id'=>'obatalkes-m-grid',
	'dataProvider'=>$modProduct->searchProduct(),
	'filter'=>$modProduct,
        'template'=>"{summary}\n{items}\n{pager}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"glyphicon glyphicon-check\"></i>","#",array("class"=>"btn-small",
                                "id" => "selectProduct",
                                "onClick" => "
                                    $(\'#id\').val($data->id);
                                    $(\'#name\').val(\'$data->name\');
                                    $(\'#barcode\').val(\'$data->barcode\');
                                    $(\'#dialogProduct\').dialog(\'close\');
                                    addProduct($data->id,\'$data->barcode\');
                                    return false;"
                                    ))',
                ),
                'barcode',
                array(
                    'header'=>'Kategori',
                    'name'=>'category_name',
                    'type'=>'raw',
                    'value'=>'isset($data->category_id) ? ucwords($data->category->name) : "-"',
                ),
                array(
                    'header'=>'Nama',
                    'name'=>'name',
                    'type'=>'raw',
                    'value'=>'isset($data->name) ? ucwords($data->name) : "-"',
                ),
                array(
                    'header'=>'Satuan',
                    'name'=>'unit',
                    'type'=>'raw',
                    'value'=>'isset($data->unit) ? ucwords($data->unit) : "-"',
                ),
                array(
                    'header'=>'Harga Jual',
                    'name'=>'price_sale',
                    'type'=>'raw',
                    'value'=>'isset($data->price_sale) ? "Rp. ".Formatter::formatNumberForCurrency($data->price_sale) : "0"',
                ),
                array(
                    'header'=>'Stock',
                    'name'=>'stock',
                    'type'=>'raw',
                    'value'=>'isset($data->stock) ? Formatter::formatNumberForCurrency($data->stock) : "0"',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
