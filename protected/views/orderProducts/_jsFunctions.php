<script type="text/javascript">
function addProduct(id,barcode)
{
    if(id == ''){
        var id = $('#id').val();
    }
    if(barcode == ''){
        var barcode = $('#barcode').val();
    }
    var quantity = 1;
    $.ajax({
        type:'POST',
        url:'<?php echo $this->createUrl('loadFormOrderProducts'); ?>',
        data: {id:id, barcode:barcode, quantity:quantity},//
        dataType: "json",
        success:function(data){
            if(data.pesan != ''){
                alert(data.pesan);
                clearFormProduct();
                return false;
            }
            var tambahkandetail = true;
            var productyangsama = $("#table-product input[name$='[barcode]'][value='"+barcode+"']");
            if(productyangsama.val()){ //jika ada produk sudah ada di table
                $("#table-product input[name$='[barcode]'][value='"+barcode+"']").each(function(){
                    var qty_sebelumnya = parseFloat($(this).parents('tr').find("input[name$='[quantity]']").val());
                    $(this).parents('tr').find("input[name$='[quantity]']").val(qty_sebelumnya + 1);
                });
                $("#table-product").find('input[name*="[ii]"][class*="integer"]').maskMoney(
                    {"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0}
                );
                renameInputRowProducts($("#table-product"));                    
                hitungTotal();
                clearFormProduct();
                // confirm("Apakah anda akan input ulang product ini?","Perhatian!",
                // function(r){
                //     if(r){
                //         $("#table-product input[name$='[product_id]'][value='"+id+"']").each(function(){
                //             $(this).parents('tr').detach();
                //         });
                //         if(tambahkandetail){
                //                 $('#table-product > tbody').append(data.form);
                //                     $("#table-product").find('input[name*="[ii]"][class*="integer"]').maskMoney(
                //                             {"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0}
                //                     );
                //                 renameInputRowProducts($("#table-product"));                    
                //                 hitungTotal();
                //                 clearFormProduct();
                //         }
                //     }else{
                //         tambahkandetail = false;
                //         clearFormProduct();
                //     }
                // }); 
            }else{
                if(tambahkandetail){
                    $('#table-product > tbody').append(data.form);
                        $("#table-product").find('input[name*="[ii]"][class*="integer"]').maskMoney(
                                {"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0}
                        );
                    renameInputRowProducts($("#table-product"));                    
                    hitungTotal();
                    clearFormProduct();
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
    });
}

function clearFormProduct(){
    $('#id').val("");
    $('#barcode').val("");
    $('#name').val("");
    $('#barcode').focus();
}

function hitungTotal(){
    unformatNumberSemua();
    var total = 0;
    var total_price = 0;
    var total_disc = 0;
    var subtotal = 0;
    var prices = 0;
    $('#table-product tbody tr').each(function(){
        var quantity  = parseInt($(this).find('input[name$="[quantity]"]').val());
        var prices_order  = parseInt($(this).find('input[name$="[prices_order]"]').val());
        var discount_ori  = parseInt($(this).find('input[name$="[discount_ori]"]').val());
        var discount        = parseInt($(this).find('input[name$="[discount]"]').val());
        var total_discount_ori  = parseInt($(this).find('input[name$="[total_discount_ori]"]').val());
        var total_discount  = parseInt($(this).find('input[name$="[total_discount]"]').val());
        
        prices = (prices_order * quantity);       
        
         if(quantity == 0){
            quantity = 1;
            $(this).find('input[name$="[quantity]"]').val(quantity);
        }
        total_discount_ori = total_discount;
        total_discount = total_discount;
        
        subtotal = (prices_order * quantity)  -  (total_discount * quantity);
        
        if(subtotal <= 0){
            subtotal = 0;
        }
        
        $(this).find('input[name$="[sub_total]"]').val(subtotal);
        $(this).find('input[name$="[discount]"]').val(discount);
        $(this).find('input[name$="[total_discount]"]').val(total_discount);
        $(this).find('input[name$="[total_discount_ori]"]').val(total_discount_ori);

        total += subtotal;
        total_price += prices;
        total_disc += total_discount * quantity;
    });
    $("#<?php echo CHtml::activeId($model,'total_price'); ?>").val(total_price);
    $("#<?php echo CHtml::activeId($model,'total_discount'); ?>").val(total_disc);
    $("#<?php echo CHtml::activeId($model,'sub_total'); ?>").val(total);
    formatNumberSemua();
}

function batalPembelian(obj){
    $(obj).parents('tr').detach();
    hitungTotal();
}
/**
* rename input grid
*/ 
function renameInputRowProducts(obj_table){
    var row = 0;
    $(obj_table).find("tbody > tr").each(function(){
        $(this).find("#no_urut").val(row+1);
        $(this).find('span').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("name","["+row+"]["+old_name_arr[2]+"]");
            }
        });
        $(this).find('input,select,textarea').each(function(){ //element <input>
            var old_name = $(this).attr("name").replace(/]/g,"");
            var old_name_arr = old_name.split("[");
            if(old_name_arr.length == 3){
                $(this).attr("id",old_name_arr[0]+"_"+row+"_"+old_name_arr[2]);
                $(this).attr("name",old_name_arr[0]+"["+row+"]["+old_name_arr[2]+"]");
            }
        });
        row++;
    });
    $('#id').val('');
    $('#name').val('');
    $('#quantity').val(1);
}
</script>