<div class="col-md-12">
    <div class="col-md-6">
        <div class="form-group">
            <?php echo $form->labelEx($model,'number', array('class'=>'col-sm-6 control-label')) ?>
            <div class="col-sm-6">
                <?php echo $form->textField($model,'number',array('class'=>'form-control','readonly'=>false,'onkeyup'=>"return $(this).focusNextInputField(event)",)); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model,'date', array('class'=>'col-sm-3 control-label')) ?>
        <?php
            $model->date = isset($model->date) ? $format->formatDateTimeForUser($model->date) : date('d M Y H:i:s');
            $this->widget('DateTimePicker',array(
                'model'=>$model,
                'attribute'=>'date',
                'mode'=>'datetime',
                'options'=> array(
                    'dateFormat'=>Params::DATE_FORMAT,
                ),
                'htmlOptions'=>array(
                    'onkeyup'=>"return $(this).focusNextInputField(event)",
                    'readonly'=>false,
                    'class'=>'span3 form-control',
                ),
            ));
            $model->date = isset($model->date) ? $format->formatDateTimeForDb($model->date) : "";
        ?>
    </div>
</div>