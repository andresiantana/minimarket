<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type'=>'horizontal',
        'id'=>'search-form',
	'htmlOptions'=>array('class'=>'form'),
)); ?>

        <?php echo $form->textFieldGroup($model,'number',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<div class="form-group">
            <div class="col-lg-4">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Cari',
		)); ?>
            </div>
	</div>

<?php $this->endWidget(); ?>
