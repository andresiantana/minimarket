<tr>
    <td>
        <?php echo CHtml::hiddenField('no_urut',0,array('readonly'=>true,'class'=>'span1 integer', 'style'=>'width:20px;')); ?>
        <?php echo CHtml::activeHiddenField($modDetails,'[ii]product_id',array('readonly'=>true,'class'=>'span1')); ?>
        <?php echo CHtml::activeHiddenField($modDetails,'[ii]barcode',array('readonly'=>true,'class'=>'span1')); ?>
        <span name="[ii][category_name]"><?php echo (!empty($modDetails->product_id) ? $modProduct->category->name : "") ?></span>
    </td>
    <td>
        <span name="[ii][name]"><?php echo (!empty($modDetails->product_id) ? $modDetails->product->name : "") ?></span>
    </td>
    <td>
        <?php echo CHtml::activeTextField($modDetails,'[ii]quantity',array('readonly'=>false,'class'=>'col-sm-3 form-control integer','style'=>'width:100px;','onblur'=>'hitungTotal();')); ?>
    </td>
    <td>
        <?php echo CHtml::activeTextField($modDetails,'[ii]prices_order',array('readonly'=>false,'class'=>'col-sm-3 form-control integer','style'=>'width:100px;','onblur'=>'hitungTotal();')); ?>
    </td>
    <td>
        <?php echo CHtml::activeHiddenField($modDetails,'[ii]discount',array('readonly'=>true,'class'=>'col-sm-3 form-control integer','style'=>'width:100px;')); ?>
        <?php echo CHtml::activeHiddenField($modDetails,'[ii]discount_ori',array('readonly'=>true,'class'=>'col-sm-3 form-control integer','style'=>'width:100px;')); ?>
        <?php echo CHtml::activeTextField($modDetails,'[ii]total_discount',array('readonly'=>false,'class'=>'col-sm-3 form-control integer','style'=>'width:100px;','onblur'=>'hitungTotal();')); ?>
        <?php echo CHtml::activeHiddenField($modDetails,'[ii]total_discount_ori',array('readonly'=>true,'class'=>'col-sm-3 form-control integer','style'=>'width:100px;')); ?>
    </td>
    <td>
        <?php echo CHtml::activeTextField($modDetails,'[ii]sub_total',array('readonly'=>true,'class'=>'span2 form-control integer','style'=>'width:100px;')); ?>
    </td>
    <td>
        <a onclick="batalPembelian(this);return false;" rel="tooltip" href="javascript:void(0);" title="Klik untuk membatalkan mutasi obat alkes ini"><i class="glyphicon glyphicon-remove"></i></a>
    </td>
</tr>