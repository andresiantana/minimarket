<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'order-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',        
        'htmlOptions'=>array('class'=>'form','onKeyPress'=>'return disableKeyPress(event);','enctype' => 'multipart/form-data', 'onSubmit'=>'return requiredCheck(this);'),
        'focus' => '#barcode',
)); ?>

<?php echo $form->errorSummary($model); ?>

<fieldset>
    <?php echo $this->renderPartial('_formPembelian', array('form'=>$form,'model'=>$model,'modDetails'=>$modDetails,'format'=>$format)); ?>
</fieldset>

<fieldset>
    <legend>Data Produk</legend>
    <?php echo $this->renderPartial('_formProduk', array('form'=>$form,'model'=>$model,'modDetails'=>$modDetails,'format'=>$format)); ?>
</fieldset>
<br/>
<div class="form-group">
    <div class="col-lg-4">
        <?php $this->widget('booster.widgets.TbButton', array(
                'buttonType'=>'submit',
                'context'=>'primary',
                'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
                'htmlOptions'=>array(
                    'onkeypress'=>'formSubmit(this,event)',
                )
        )); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Daftar Pembelian',array('{icon}'=>'<i class="fa fa-list"></i>')),$this->createUrl('report',array()), array('class'=>'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
