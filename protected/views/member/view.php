<?php
$this->breadcrumbs=array(
	'Members'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('label'=>'List Member','url'=>array('index')),
    array('label'=>'Create Member','url'=>array('create')),
    array('label'=>'Update Member','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete Member','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage Member','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Lihat <small>Member #<?php echo $model->id; ?></small>
        </h1>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        		'id',
		array(
            'label'=>'Tanggal Daftar Member',
            'value'=>isset($model->date) ? Formatter::formatDateTimeForUser($model->date) : "-",
        ),
		'number',
		'name',
		'address',
		'phone_handphone',
		'in_point',
		'out_point',
		'current_point',
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                <?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan Member',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>
