<?php
$this->breadcrumbs=array(
	'Members'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('label'=>'List Member','url'=>array('index')),
    array('label'=>'Create Member','url'=>array('create')),
    array('label'=>'View Member','url'=>array('view','id'=>$model->id)),
    array('label'=>'Manage Member','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Ubah <small>Member <?php echo $model->id; ?></small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>            </div>
        </div>
    </div>
</div>