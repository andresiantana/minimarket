<div class="view">
    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('number')); ?>:</b>
	<?php echo CHtml::encode($data->number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_handphone')); ?>:</b>
	<?php echo CHtml::encode($data->phone_handphone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('in_point')); ?>:</b>
	<?php echo CHtml::encode($data->in_point); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('out_point')); ?>:</b>
	<?php echo CHtml::encode($data->out_point); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('current_point')); ?>:</b>
	<?php echo CHtml::encode($data->current_point); ?>
	<br />

	*/ ?>
</div>