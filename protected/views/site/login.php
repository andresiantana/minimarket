<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="form-horizontal">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
//	'enableClientValidation'=>true,
	'clientOptions'=>array(
            'validateOnSubmit'=>true,
	),
	'focus'=>'#'.CHtml::activeId($model, 'username'),
)); ?>
    <fieldset>
        <?php if(isset($_GET['activation']) && $_GET['activation'] == 0){ ?>
        <div class="alert alert-danger">
            Akun Anda tidak aktif, silahkan hubungi Adminisrator.
        </div>
        <?php } ?>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->textField($model,'username',array('class'=>'form-control','id'=>'inputUsername','placeholder'=>'Nama Pengguna', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model,'username'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->passwordField($model,'password',array('class'=>'form-control','id'=>'inputPassword','placeholder'=>'Kata Sandi', 'autocomplete'=>'off')); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12 text-center">
                <?php echo CHtml::submitButton('Masuk',array('class'=>'btn btn-primary')); ?>
            </div>
        </div>
    </fieldset>
<?php $this->endWidget(); ?>
</div><!-- form -->
