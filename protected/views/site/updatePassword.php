<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Ubah <small>Kata Sandi</small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
            <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
                    'id'=>'user-form',
                    'enableAjaxValidation'=>false,
                    'type'=>'horizontal',
                    'focus'=>'#old_password',
                    'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
            )); ?>

            <p class="help-block">Inputan dengan tanda <span class="required">*</span> harus diisi.</p>

            <?php echo $form->errorSummary($model); ?>
            <div class="form-group">
                <?php echo $form->labelEx($model,'old_password',array('class'=>'col-sm-3 control-label required')); ?>
                <div class="col-md-4">
                    <?php echo $form->passwordField($model,'old_password',array('id'=>'old_password','class'=>'form-control', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>200,'required'=>true)); ?>
                    <?php echo $form->error($model,'old_password'); ?>
                </div>
            </div>
            
            <div class="form-group">
                <?php echo $form->labelEx($model,'new_password', array('class'=>'col-sm-3 control-label')) ?>
                <div class="col-sm-4">
                    <?php echo CHtml::activePasswordField($model,'new_password',array('id'=>'new_password','class'=>'form-control',  'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>200,'required'=>true)); ?>
                </div>
            </div>
            
            <div class="form-group">
                <?php echo $form->labelEx($model,'new_password_repeat', array('class'=>'col-sm-3 control-label')) ?>
                <div class="col-sm-4">
                    <?php echo CHtml::activePasswordField($model,'new_password_repeat',array('id'=>'new_password_repeat','class'=>'form-control',  'onkeypress'=>"return $(this).focusNextInputField(event)",'maxlength'=>200,'required'=>true)); ?>
                </div>
            </div>
            
            <?php echo CHtml::hiddenfield('prevUrl',$prevUrl); ?>  
            <div class="form-group">
                <div class="col-lg-4">
                    <?php $this->widget('booster.widgets.TbButton', array(
                            'buttonType'=>'submit',
                            'context'=>'primary',
                            'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
                    )); ?>
                    <?php
                        if ($model->role_id == Params::ROLE_GUDANG) {
                            $urlCancel = Yii::app()->createUrl('/product/admin');
                        } else if ($model->role_id == Params::ROLE_KASIR) {
                            $urlCancel =  Yii::app()->createUrl('/saleProducts/index');
                        } else if ($model->role_id == Params::ROLE_OWNER) {
                            $urlCancel = Yii::app()->createUrl('/product/dashboard');
                        }
                    ?>
                    <?php echo CHtml::link(Yii::t('mds','{icon} Batal',array('{icon}'=>'<i class="fa fa-ban"></i>')),$urlCancel, array('class'=>'btn btn-danger')); ?>
                </div>
            </div>
            </div>
    <?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<?php
$js = <<< JSCRIPT
   
		
JSCRIPT;
Yii::app()->clientScript->registerScript('kosongkanPassword', $js, CClientScript::POS_READY);
?>
<script type="text/javascript">
$( document ).ready(function() {
    kosongkanPassword();
    $(document).on('change', '#new_password', function() {
      cekPassLama();
      var pass_baru = $('#new_password').val();
      var pass_baru_repeat = $('#new_password_repeat').val();
      if (pass_baru_repeat != '') {
        if (pass_baru != pass_baru_repeat) {
            cekPassBaru();
        }
      }
    });
    $(document).on('change', '#new_password_repeat', function() {
      cekPassBaru();
    });
});
function cekPassLama() {
    var pass_lama = $('#old_password').val();
    var pass_baru = $('#new_password').val();
    if (pass_baru == pass_lama) {
        alert('kata kunci baru tidak boleh sama dengan kata kunci lama');
        $('#new_password').val('');
    }
}	
function cekPassBaru() {
    var pass_baru = $('#new_password').val();
    var pass_baru_repeat = $('#new_password_repeat').val();
    if(pass_baru_repeat != pass_baru){
        alert('ulang kata kunci harus sama dengan kata kunci baru');
        $('#new_password_repeat').val('');
    }
}     
function kosongkanPassword() {
    $('#<?php echo CHtml::activeId($model,'new_password');?>').val('');
    $('#<?php echo CHtml::activeId($model,'old_password');?>').val('');
    $('#<?php echo CHtml::activeId($model,'password');?>').val('');
    $('#<?php echo CHtml::activeId($model,'new_password_repeat');?>').val('');
}
</script>