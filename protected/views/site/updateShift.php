<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Ubah <small>Jam Kerja</small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
            <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
                    'id'=>'user-form',
                    'enableAjaxValidation'=>false,
                    'type'=>'horizontal',
                    'focus'=>'#'.CHtml::activeId($modEmployee,'shift_id'),
                    'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
            )); ?>

            <p class="help-block">Inputan dengan tanda <span class="required">*</span> harus diisi.</p>

            <?php //echo $form->errorSummary(array($modEmployee)); ?>

            <!-- <div class="form-group">
                <?php //echo $form->labelEx($model,'username',array('class'=>'col-sm-3 control-label required')); ?>
                <div class="col-md-3">
                    <?php //echo $form->textField($model,'username',array('class'=>'form-control', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>200,'readonly'=>true)); ?>
                    <?php //echo $form->error($model,'username'); ?>
                </div>
            </div> -->
            <!-- <div class="form-group">
                <?php //echo $form->labelEx($modEmployee,'name',array('class'=>'col-sm-3 control-label required')); ?>
                <div class="col-md-3">
                    <?php //echo $form->textField($modEmployee,'name',array('class'=>'form-control', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>200,'readonly'=>true)); ?>
                    <?php //echo $form->error($modEmployee,'name'); ?>
                </div>
            </div> -->

            <?php echo $form->dropDownListGroup($modEmployee,'shift_id', array('widgetOptions'=>array('data'=>CHtml::listData(Shift::model()->findAll(),'id','NameAndHours'),'htmlOptions'=>array('class'=>'span5','empty'=>'--Pilih--')))); ?>
            <?php //echo $form->error($modEmployee,'shift_id'); ?>
            <?php
                echo CHtml::hiddenfield('prevUrl',$prevUrl);
            ?>
            <div class="form-group">
                <div class="col-lg-4">
                    <?php $this->widget('booster.widgets.TbButton', array(
                            'buttonType'=>'submit',
                            'context'=>'primary',
                            'label'=>$modEmployee->isNewRecord ? 'Simpan' : 'Simpan',
                    )); ?>
                    <?php
                        if ($model->role_id == Params::ROLE_GUDANG) {
                            $urlCancel = Yii::app()->createUrl('/product/admin');
                        } else if ($model->role_id == Params::ROLE_KASIR) {
                            $urlCancel =  Yii::app()->createUrl('/saleProducts/index');
                        }
                    ?>
                    <?php echo CHtml::link(Yii::t('mds','{icon} Batal',array('{icon}'=>'<i class="fa fa-ban"></i>')),$urlCancel, array('class'=>'btn btn-danger')); ?>
                </div>
            </div>
        </div>
    <?php $this->endWidget(); ?>
        </div>
    </div>
</div>