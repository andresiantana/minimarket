<?php
$gets = "";
if(isset($_GET)){
foreach($_GET AS $name => $get){
if($name != "r")
$gets .= "&".$name."=".$get;
}
}
?>
<?php $baseUrl = Yii::app()->createUrl("/");?>
<script type='text/javascript'>
    function setTab(obj){
        $(obj).parents("ul").find("li").each(function(){
            $(this).removeClass("active");
            $(this).attr("onclick","setTab(this);");
        });
        $(obj).addClass("active");
        $(obj).removeAttr("onclick","setTab(this);");
        var tab = $(obj).attr("tab");
        var frameObj = document.getElementById("frame");
        resetIframe(frameObj);
        $(frameObj).attr("src","<?php echo $baseUrl;?>?r="+tab+"<?php echo $gets;?>");
        $(frameObj).parent().addClass("animation-loading");
        $(frameObj).load(function(){
             $(frameObj).parent().removeClass("animation-loading");
             resizeIframe(frameObj);
        });
        return false;
    }
    function resetIframe(obj) {
        obj.style.height = 128 + 'px';
    }
    function resizeIframe(obj) {
        obj.style.height = (obj.contentWindow.document.body.scrollHeight) + 'px';
    }
    
    function getIdPegawai(obj){
        var nik = obj.value;
        $.ajax({
            type:'POST',
            url:'<?php echo $this->createUrl('loadIdPegawai'); ?>',
            data: {nik:nik},//
            dataType: "json",
            success:function(data){
                if(data.pesan != ''){
                    alert(data.pesan);
                    clearNIK();
                    return false;
                }
                $('#Presence_employee_id').val(data.employee_id);
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
        });
    }
    
    function getAbsensiDatang(){
        var nik = $('#Presence_code').val();
        var employee_id = $('#Presence_employee_id').val();
        
        if(nik == ''){
            alert('Scan barcode NIK Pegawai terlebih dahulu.');
            return false;
            $('#Presence_code').focus();
        }
        
        $.ajax({
            type:'POST',
            url:'<?php echo $this->createUrl('savePresenceDatang'); ?>',
            data: {nik:nik, employee_id:employee_id},//
            dataType: "json",
            success:function(data){
                $('#status').html(data.pesan);
                clearNIK();
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
        });
    }
    
    function getAbsensiPulang(){
        var nik = $('#Presence_code').val();
        var employee_id = $('#Presence_employee_id').val();
        
        if(nik == ''){
            alert('Scan barcode NIK Pegawai terlebih dahulu.');
            return false;
            $('#Presence_code').focus();
        }
        
        $.ajax({
            type:'POST',
            url:'<?php echo $this->createUrl('savePresencePulang'); ?>',
            data: {nik:nik, employee_id:employee_id},//
            dataType: "json",
            success:function(data){
                $('#status').html(data.pesan);
                clearNIK();
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
        });
    }

    function clearNIK(){
        $('#Presence_code').val('');
        $('#Presence_employee_id').val('');
    }
</script>
<?php
Yii::app()->clientScript->registerScript('onLoadJs','
    setTab($("#tab-default"));
    resizeIframe(document.getElementById("frame"));
', CClientScript::POS_READY);
?>