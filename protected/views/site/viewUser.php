<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Lihat <small>Profil User #<?php echo $model->username; ?></small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        array(
                            'label'=>'Username',
                            'value'=>isset($model->username) ? $model->username : "",
                        ),
                        array(
                            'label'=>'Posisi User',
                            'value'=>isset($model->role_id) ? $model->role->role_name : "",
                        ),
                        array(
                            'label'=>'Last Login',
                            'value'=>isset($model->last_login) ? $format->formatDateTimeForUser($model->last_login) : "",
                        ),
                        array(
                            'label'=>'Status',
                            'value'=>isset($model->is_active) ? "Aktif" : "Tidak Aktif",
                        ),
                    ),
                )); ?>
                <?php if(isset($modEmployee)){ ?>
                    <?php $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$modEmployee,
                    'attributes'=>array(
                        array(
                            'label'=>'Nama Pegawai',
                            'value'=>isset($modEmployee->name) ? $modEmployee->name : "",
                        ),
                        array(
                            'label'=>'Alamat',
                            'value'=>isset($modEmployee->address) ? $modEmployee->address : "-",
                        ),
                        array(
                            'label'=>'No. Hp',
                            'value'=>isset($modEmployee->handphone) ? $modEmployee->handphone : "-",
                        ),
                    ),
                )); ?>
                <?php } ?>
            </div>
            <div class='panel-footer'>
                <?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan User',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>
