<?php
/* @var $this SiteController */
/* @var $model Presences */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Absensi';
$this->breadcrumbs=array(
	'Absensi',
);
?>
<div class="form-horizontal">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'presences',
//	'enableClientValidation'=>true,
	'clientOptions'=>array(
            'validateOnSubmit'=>true,
	),
	'focus'=>'#'.CHtml::activeId($model, 'code'),
)); ?>
    <fieldset>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->textField($model,'code',array('class'=>'form-control','placeholder'=>'Scan Barcode NIK Pegawai', 'autocomplete'=>'on', 'onkeyup'=>'getIdPegawai(this);','readonly'=>false)); ?>
                <?php echo $form->hiddenField($model,'employee_id',array('class'=>'form-control','readonly'=>false)); ?>
                <?php echo $form->error($model,'code'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12 text-center">
                <?php echo CHtml::button('Datang',array('class'=>'btn btn-primary','type'=>'button','onClick'=>'getAbsensiDatang();')); ?>
                <?php echo CHtml::button('Pulang',array('class'=>'btn btn-primary','type'=>'button','onClick'=>'getAbsensiPulang();')); ?>
            </div>
        </div>
        <div id="status" style="text-align:center;color:red;"></div>
    </fieldset>
<?php $this->endWidget(); ?>
</div><!-- form -->
<?php $this->renderPartial('_jsFunctions',array()); ?>
