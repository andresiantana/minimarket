<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus' => '#user-form div.form-group:first-child div input',
        'htmlOptions'=>array('class'=>'form','onKeyPress'=>'return disableKeyPress(event);', 'enctype' => 'multipart/form-data'),
)); ?>

<p class="help-block">Inputan dengan tanda <span class="required">*</span> harus diisi.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

    <?php echo $form->passwordFieldGroup($model,'password', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

    <?php if($model->role_id != Params::ROLE_ADMIN){ ?>
        <?php echo $form->dropDownListGroup($model,'role_id', array('widgetOptions'=>array('data'=>CHtml::listData(Role::model()->findAll('id !='.Params::ROLE_ADMIN),'id','role_name'),'htmlOptions'=>array('class'=>'span5','empty'=>'--Pilih--','onchange'=>'setPegawai(this);')))); ?>
    <?php } ?>
    <div id="pegawai">
        <?php echo $form->dropDownListGroup($model,'employee_id', array('widgetOptions'=>array('data'=>CHtml::listData(Employee::model()->findAll('is_have_user = 0'),'id','name'),'htmlOptions'=>array('class'=>'span5','empty'=>'--Pilih--')))); ?>
    </div>

    <div class="form-group">
        <?php echo CHtml::label('Status','',array('class'=>'col-md-3 control-label')); ?>
        <div class="col-md-5">
            <?php echo $form->checkBox($model,'is_active')." Aktif"; ?>
        </div>
    </div>

<div class="form-group">
    <div class="col-lg-4">
        <?php $this->widget('booster.widgets.TbButton', array(
                'buttonType'=>'submit',
                'context'=>'primary',
                'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
        )); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan Pengguna',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

<?php
    $this->renderPartial('_jsFunctions',array('model'=>$model));
?>
