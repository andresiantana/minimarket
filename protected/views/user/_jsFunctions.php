<script type="text/javascript">
    $('#pegawai').hide();
    function setPegawai(obj){
        var role_id = obj.value;
        if(role_id != ''){
            $.ajax({
                type:'POST',
                url:'<?php echo $this->createUrl('setRoleName'); ?>',
                data: {role_id:role_id},//
                dataType: "json",
                success:function(data){
                    if(data.name != 'owner'){
                        $('#pegawai').show();
                    }else{
                        $('#pegawai').hide();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
            });
        }else{
            alert('Pilih terlebih dahulu Posisi');
            return false;
        }
    }
</script>