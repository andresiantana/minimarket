<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('label'=>'List User','url'=>array('index')),
    array('label'=>'Create User','url'=>array('create')),
    array('label'=>'Update User','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete User','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage User','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Detail <small>Pengguna #<?php echo $model->username; ?></small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        'id',
                        'username',
                        'password',
                        array(
                            'name'=>'last_login',
                            'value'=>isset($model->last_login) ? Formatter::formatDateTimeForUser($model->last_login) : "",
                        ),
                        array(
                             'name'=>'is_active',
                             'value'=>($model->is_active == 1) ? "Aktif" : "Tidak Aktif",
                         ),
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                    <?php //echo CHtml::link(Yii::t('mds','{icon} Ubah',array('{icon}'=>'<i class="fa fa-edit"></i>')),$this->createUrl('update',array('id'=>$model->id)), array('class'=>'btn btn-success')); ?>
                    <?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan Pengguna',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>
