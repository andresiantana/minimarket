<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type'=>'horizontal',
        'id'=>'search-form',
	'htmlOptions'=>array('class'=>'form'),
)); ?>

    <?php //echo $form->textFieldGroup($model,'id',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

    <?php echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

    <?php echo $form->dropDownListGroup($model,'role_id', array('widgetOptions'=>array('data'=>CHtml::listData(Role::model()->findAll(),'id','role_name'),'htmlOptions'=>array('class'=>'span5','empty'=>'--Pilih--')))); ?>

    <?php echo $form->textFieldGroup($model,'employee_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>

	<div class="form-group">
            <div class="col-lg-4">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Cari',
		)); ?>
            </div>
	</div>

<?php $this->endWidget(); ?>
