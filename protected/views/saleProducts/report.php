<?php
$this->breadcrumbs=array(
            'Orders'=>array('index'),
            'Manage',
    );

$this->menu=array(
    array('label'=>'List Order','url'=>array('index')),
    array('label'=>'Create Order','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('#search-form').submit(function(){
        $.fn.yiiGridView.update('order-report-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Laporan <small>Penjualan</small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <legend>Pencarian</legend>
                <div class="search-form">
                    <?php $this->renderPartial('_searchReport',array(
                         'model'=>$model,
                        'format'=>$format
                    )); ?>
                </div><!-- search-form -->
            </div>
            <div class="panel-body">
                <?php $this->renderPartial('_tableReport',array(
                        'model'=>$model,
                        'format'=>$format
                    )); ?>
            </div>
            <div class='panel-footer'>
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="fa fa-print"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'printReport(\'PRINT\')'))."&nbsp&nbsp"; ?>
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print Total Penjualan',array('{icon}'=>'<i class="fa fa-print"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'printReportTotal(\'PRINT\')'))."&nbsp&nbsp"; ?>
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print Total Penjualan Per Hari',array('{icon}'=>'<i class="fa fa-print"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'printReportTotalHari(\'PRINT\')'))."&nbsp&nbsp"; ?>
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print Per Kategori',array('{icon}'=>'<i class="fa fa-print"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'printPerKategori(\'PRINT\')'))."&nbsp&nbsp"; ?>
            </div>
    </div>
</div>
<script type="text/javascript">
    function printReport(caraPrint)
    {
        window.open("<?php echo $this->createUrl('printReport'); ?>/"+$('#search-form').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=1100px, scrollbars=yes');
    }  
    function printReportTotal(caraPrint)
    {
        window.open("<?php echo $this->createUrl('printReportTotal'); ?>/"+$('#search-form').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=1100px, scrollbars=yes');
    } 
    function printReportTotalHari(caraPrint)
    {
        window.open("<?php echo $this->createUrl('printReportTotalHari'); ?>/"+$('#search-form').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=1100px, scrollbars=yes');
    }
    function printPerKategori(caraPrint)
    {
        window.open("<?php echo $this->createUrl('printPerKategori'); ?>/"+$('#search-form').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=1100px, scrollbars=yes');
    }   
</script>