<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Transaksi <small>Penjualan</small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo $this->renderPartial('_form', array('model'=>$model,'modDetails'=>$modDetails,'format'=>$format, 'modMember'=>$modMember)); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->renderPartial('_jsFunctions', array('model'=>$model,'modDetails'=>$modDetails,'modMember'=>$modMember)); ?>