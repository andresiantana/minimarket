<div class="form-group ">
    <?php echo CHtml::label('Barcode', 'barcode', array('class'=>'col-sm-3 control-label')); ?>
    <div class="col-sm-3">
        <?php echo CHtml::hiddenField('id','',array()); ?>
        <?php echo CHtml::hiddenField('name','',array()); ?>
    <?php
        $this->widget('JuiAutoComplete', array(
            'name'=>'barcode',
            'id'=>'barcode',
            'source'=>'js: function(request, response) {
                $.ajax({
                    url: "'.$this->createUrl('AutocompleteProduct').'",
                    dataType: "json",
                    data: {
                        barcode: request.term,
                    },
                    success: function (data) {
                         response(data);
                    }
                })
             }',
             'options'=>array(
                   'showAnim'=>'fold',
                   'minLength' => 2,
                   'focus'=> 'js:function( event, ui ) {
                        $(this).val("");
                        return false;
                    }',
                   'select'=>'js:function( event, ui ) {
                        $(this).val(ui.item.value);
                        $("#id").val(ui.item.id);
                        $("#name").val(ui.item.name);
                        $("#barcode").val(ui.item.barcode);
                        addProduct(ui.item.id,ui.item.barcode);
                        return false;
                    }',
            ),
            'htmlOptions'=>array(
                'onkeyup'=>"return $(this).focusNextInputField(event)",
                'onblur'=>"if($(this).val()=='') clearFormProduct(); else addProduct('',this.value)",
                'class'=>'span5 form-control',
                'id'=>'barcode',
            ),
            'tombolDialog'=>array('idDialog'=>'dialogProduct'),
        ));
        ?>
    </div>
</div>
<br>
<table class="table table-bordered table-striped" id="table-product">
    <thead>
        <tr>
            <th>Kategori</th>
            <th>Nama Produk</th>
            <th>Jumlah</th>
            <th>Harga</th>
            <th>Diskon (Rp.)</th>
            <th>Sub Total</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(count($modDetails) > 0){
                $modProducts = $modDetails;
                foreach($modProducts as $i=>$modDetails){
                    $modProduct = Product::model()->findByPk($modDetails->product_id);
                    echo $this->renderPartial('_rowProductDetails', array('form'=>$form,'model'=>$model,'modDetails'=>$modDetails,'format'=>$format,'modProduct'=>$modProduct));
                }
            }
        ?>
    </tbody>
</table>
<br/><br/>
<?php
$is_member = "false";
$class="collapsed";
$height = "height: 0px;";
$collapse = "collapse";
$modKonfig = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET);
if($modKonfig->is_member == true){
    $is_member = "true";
    $class="";
    $height = "";
    $collapse = "collapse in";
}else{
    $is_member = "false";
    $class="collapsed";
    $height = "height: 0px;";
    $collapse = "collapse";
}
?>
<div class="row">
    <?php if($modKonfig->is_member == true){ ?>
    <div class="col-md-6">
        <div id="button">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Punya Member',array('{icon}'=>'<i class="fa fa-save"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'setPunyaMember();'))."&nbsp&nbsp"; ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Tidak Punya Member',array('{icon}'=>'<i class="fa fa-save"></i>')),array('class'=>'btn btn-success', 'type'=>'button','onclick'=>'setTidakPunyaMember();'))."&nbsp&nbsp"; ?>
        </div>
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="<?php echo $class; ?>" aria-expanded="<?php echo $is_member; ?>" style="margin-top-20px;">Members</a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse <?php echo $collapse; ?>" style="<?php echo $height; ?>" aria-expanded="<?php echo $is_member; ?>">
                    <div class="panel-body">                        
                        <div id="punya_member">
                            <div class="form-group ">
                                <?php echo CHtml::label('No. Member', 'numbers', array('class'=>'col-sm-3 control-label')); ?>
                                <div class="col-sm-6">
                                    <?php echo CHtml::activeHiddenField($model,'member_id',array()); ?>
                                <?php
                                    $this->widget('JuiAutoComplete', array(
                                        'name'=>'no_member',
                                        'id'=>'no_member',
                                        'source'=>'js: function(request, response) {
                                            $.ajax({
                                                url: "'.$this->createUrl('AutocompleteMember').'",
                                                dataType: "json",
                                                data: {
                                                    no_member: request.term,
                                                },
                                                success: function (data) {
                                                     response(data);
                                                }
                                            })
                                         }',
                                         'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'focus'=> 'js:function( event, ui ) {
                                                    $(this).val("");
                                                    return false;
                                                }',
                                               'select'=>'js:function( event, ui ) {
                                                    $(this).val(ui.item.value);
                                                    $("#'.CHtml::activeId($model,'member_id').'").val(ui.item.id);
                                                    $("#'.CHtml::activeId($modMember,'current_point').'").val(ui.item.current_point);
                                                    return false;
                                                }',
                                        ),
                                        'htmlOptions'=>array(
                                            'onkeyup'=>"return $(this).focusNextInputField(event)",
                                            'onblur'=>"if($(this).val()=='') clearMember();",
                                            'class'=>'form-control',
                                            'id'=>'no_member',
                                            'style'=>'width:100px;',
                                        ),
                                        'tombolDialog'=>array('idDialog'=>'dialogMember'),
                                    ));
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo CHtml::label('Point Sekarang', 'in_point', array('class'=>'col-sm-3 control-label')); ?>
                                <div class="col-sm-6">            
                                    <?php echo $form->textField($modMember,'current_point',array('class'=>'form-control integer','readonly'=>true,'onkeyup'=>"return $(this).focusNextInputField(event)",)); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo CHtml::label('Point Yang Digunakan', 'out_point', array('class'=>'col-sm-3 control-label')); ?>
                                <div class="col-sm-6">            
                                    <?php echo $form->textField($modMember,'out_point',array('class'=>'form-control integer','readonly'=>false,'onkeyup'=>"return $(this).focusNextInputField(event)",'onblur'=>'setDiskonPoint(this);')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo CHtml::label('Rupiah per Point', 'out_point', array('class'=>'col-sm-3 control-label')); ?>
                                <div class="col-sm-6">            
                                    <div class="form-group input-group" style="margin-left:3px;">
                                        <span class="input-group-addon">Rp.</span>
                                        <?php echo CHtml::textField('konversi_point_rupiah','0',array('class'=>'form-control integer','readonly'=>true,'onkeyup'=>"return $(this).focusNextInputField(event)",'style'=>'width:160px;')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tidak_punya">
                            <?php echo $this->renderPartial('_formMember', array('form'=>$form,'modMember'=>$modMember)); ?>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }else{ ?>
    <div class="col-md-6"></div>
    <?php } ?>
    <div class="col-md-6">
        <div class="col-sm-6">
            <?php echo $form->labelEx($model,'total_price', array('class'=>'col-sm-6 control-label','style'=>'margin-left:60px;font-size:10pt;')) ?>
        </div>
        <div class="col-sm-6">            
            <div class="form-group input-group">
                <span class="input-group-addon">Rp.</span>
                <?php echo $form->textField($model,'total_price',array('class'=>'form-control integer','readonly'=>true,'onkeyup'=>"return $(this).focusNextInputField(event)",)); ?>
            </div>
        </div>
        
        <div class="col-sm-6">
            <?php echo $form->labelEx($model,'total_discount', array('class'=>'col-sm-6 control-label','style'=>'margin-left:60px;font-size:10pt;')) ?>
        </div>
        <div class="col-sm-6">            
            <div class="form-group input-group">
                <span class="input-group-addon">Rp.</span>
                <?php echo $form->textField($model,'total_discount',array('class'=>'form-control integer','readonly'=>true,'onkeyup'=>"return $(this).focusNextInputField(event)",)); ?>
            </div>
        </div>        

        <div class="col-sm-6">
            <?php echo $form->labelEx($model,'sub_total', array('class'=>'col-sm-6 control-label','style'=>'margin-left:60px;font-size:10pt;')) ?>
        </div>
        <div class="col-sm-6">            
            <div class="form-group input-group">
                <span class="input-group-addon">Rp.</span>
                <?php echo $form->textField($model,'sub_total',array('class'=>'form-control integer','readonly'=>true,'onkeyup'=>"return $(this).focusNextInputField(event)",)); ?>
            </div>
        </div>
        
        <div class="col-sm-6">
            <?php echo $form->labelEx($model,'money_cash', array('class'=>'col-sm-6 control-label','style'=>'margin-left:60px;font-size:10pt;')) ?>
        </div>
        <div class="col-sm-6">            
            <div class="form-group input-group">
                <span class="input-group-addon">Rp.</span>
                <?php echo $form->textField($model,'money_cash',array('class'=>'form-control integer','readonly'=>false,'onkeyup'=>"return $(this).focusNextInputField(event)",'onblur'=>'hitungKembalian();')); ?>
            </div>
        </div>
        
        <div class="col-sm-6">
            <?php echo $form->labelEx($model,'money_back', array('class'=>'col-sm-6 control-label','style'=>'margin-left:60px;font-size:10pt;')) ?>
        </div>
        <div class="col-sm-6">            
            <div class="form-group input-group">
                <span class="input-group-addon">Rp.</span>
                <?php echo $form->textField($model,'money_back',array('class'=>'form-control integer','readonly'=>false,'onkeyup'=>"return $(this).focusNextInputField(event)")); ?>
            </div>
        </div>

        <?php if($modKonfig->is_member == true){ ?>
        <div class="col-sm-6">
            <?php echo $form->labelEx($model,'total_discount_point', array('class'=>'col-sm-6 control-label','style'=>'margin-left:60px;font-size:10pt;')) ?>
        </div>
        <div class="col-sm-6">            
            <div class="form-group input-group">
                <span class="input-group-addon">Rp.</span>
                <?php echo $form->textField($model,'total_discount_point',array('class'=>'form-control integer','readonly'=>true,'onkeyup'=>"return $(this).focusNextInputField(event)",)); ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<?php
//========= Dialog buat cari data produk =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogProduct',
    'options'=>array(
        'title'=>'Daftar Produk',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>980,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modProduct = new Product('searchProduct');
$modProduct->unsetAttributes();
if(isset($_GET['Product'])){
    $modProduct->attributes = $_GET['Product'];
    $modProduct->name = $_GET['Product']['name'];
    $modProduct->category_name = $_GET['Product']['category_name'];
}
$this->widget('booster.widgets.TbGridView',array(
	'id'=>'obatalkes-m-grid',
	'dataProvider'=>$modProduct->searchProduct(),
	'filter'=>$modProduct,
        'template'=>"{summary}\n{items}\n{pager}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"glyphicon glyphicon-check\"></i>","#",array("class"=>"btn-small",
                                "id" => "selectProduct",
                                "onClick" => "
                                    $(\'#id\').val($data->id);
                                    $(\'#name\').val(\'$data->name\');
                                    $(\'#barcode\').val(\'$data->barcode\');
                                    $(\'#dialogProduct\').dialog(\'close\');
                                    addProduct($data->id,\'$data->barcode\');
                                    return false;"
                                    ))',
                ),
                'barcode',
                array(
                    'header'=>'Kategori',
                    'name'=>'category_name',
                    'type'=>'raw',
                    'value'=>'isset($data->category_id) ? ucwords($data->category->name) : "-"',
                ),
                array(
                    'header'=>'Nama',
                    'name'=>'name',
                    'type'=>'raw',
                    'value'=>'isset($data->name) ? ucwords($data->name) : "-"',
                ),
                array(
                    'header'=>'Satuan',
                    'name'=>'unit',
                    'type'=>'raw',
                    'value'=>'isset($data->unit) ? ucwords($data->unit) : "-"',
                ),
                array(
                    'header'=>'Harga Jual',
                    'name'=>'price_sale',
                    'type'=>'raw',
                    'value'=>'isset($data->price_sale) ? "Rp. ".Formatter::formatNumberForCurrency($data->price_sale) : "0"',
                ),
                array(
                    'header'=>'Stock',
                    'name'=>'stock',
                    'type'=>'raw',
                    'value'=>'isset($data->stock) ? Formatter::formatNumberForCurrency($data->stock) : "0"',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>

<?php
//========= Dialog buat cari data produk =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogMember',
    'options'=>array(
        'title'=>'Daftar Member',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>980,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modMember = new Member('search');
$modMember->unsetAttributes();
if(isset($_GET['Member'])){
    $modMember->attributes = $_GET['Member'];
}
$this->widget('booster.widgets.TbGridView',array(
    'id'=>'member-m-grid',
    'dataProvider'=>$modMember->search(),
    'filter'=>$modMember,
    'template'=>"{summary}\n{items}\n{pager}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"glyphicon glyphicon-check\"></i>","#",array("class"=>"btn-small",
                                "id" => "selectProduct",
                                "onClick" => "
                                    $(\'#no_member\').val($data->number);
                                    $(\'#'.CHtml::activeId($model,'member_id').'\').val($data->id);
                                    $(\'#'.CHtml::activeId($modMember,'current_point').'\').val($data->current_point);
                                    $(\'#dialogMember\').dialog(\'close\');
                                    return false;"
                                    ))',
                ),
                array(
                    'header'=>'Nama',
                    'name'=>'name',
                    'type'=>'raw',
                    'value'=>'isset($data->name) ? ucwords($data->name) : "-"',
                ),
                array(
                    'header'=>'No. Member',
                    'name'=>'number',
                    'type'=>'raw',
                    'value'=>'isset($data->number) ? ucwords($data->number) : "-"',
                ),
                array(
                    'header'=>'Alamat',
                    'name'=>'address',
                    'type'=>'raw',
                    'value'=>'isset($data->address) ? ucwords($data->address) : "-"',
                ),
                array(
                    'header'=>'No. Telp/No. Hp.',
                    'name'=>'phone_handphone',
                    'type'=>'raw',
                    'value'=>'isset($data->phone_handphone) ? ucwords($data->phone_handphone) : "-"',
                ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<script type="text/javascript">
$('#punya_member').hide();
$('#tidak_punya').hide();
$('#accordion').hide();
$('#konversi_point_rupiah').val(<?php echo isset($modKonfig->point_to_idr) ? $modKonfig->point_to_idr : 0; ?>);
$('#tidak_punya').find(".required").addClass("not-required").removeClass("required");
function setPunyaMember(){
    $('#accordion').show();
    $('#punya_member').show();
    $('#tidak_punya').hide();
    $('#tidak_punya').find(".required").addClass("not-required").removeClass("required");
}
function setTidakPunyaMember(){
    $('#tidak_punya').find(".not-required").addClass("required").removeClass("not-required");
    $('#accordion').show();
    $('#tidak_punya').show();
    $('#punya_member').hide();
}

$(document).ready(function(){
    $(".members").click(function(){
        $("p").slideToggle();
    });
});
</script>