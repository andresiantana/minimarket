<style>
    td, th{
        font-size: 27pt !important;
        letter-spacing: -1px !important;
		height:11px;
    }
	tr{
		height:10px;
	}
    body{
        width:7.9cm;
    }
    .produk{
        line-height: 1.8em;
    }
    .footer-produk{
        line-height: 2.5em;
    }
    .footer-struk{
        line-height: 3em;
    }
    .header-struk{
        line-height: 3em;
    }
</style>
<?php $profile = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET); ?>
<table width="100%" cellpadding="0" cellspacing="0">
    <thead>
        <tr class="header-struk">
            <td style="text-align:center;" colspan="5">
                <b><?php echo isset($profile->name) ? strtoupper($profile->name) : "CAHAYA MUDA TOSERBA"; ?></b><br>
                <?php echo isset($profile->address) ? $profile->address : "Jl. Raya Majalangu Watukumpul, Pemalang"; ?><br>
                <?php echo isset($profile->phone) ? "Telp. ". $profile->phone : "Telp. 082322965854"; ?>
            </td>
        </tr>
        <tr class="header-struk">
            <td colspan="5" style="border-top: 2px solid #000000"></td>
        </tr>
        <tr class="header-struk">
            <td>Kasir : <?php echo isset($model->employee_id) ? $model->employee->name : ""; ?></td>
            <td colspan="4" style="text-align:right;">Tgl : <?php echo isset($model->date) ? $format->formatDateTimeId($model->date) : ""; ?></td>
        </tr>
        <tr class="header-struk">
            <td colspan="5" style="text-align:right;">No. Faktur : <?php echo isset($model->number) ? $model->number: ""; ?></td>
        </tr>
        <tr style="text-align:center;">
            <th>Produk</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Diskon</th>
            <th>Total Harga</th>            
        </tr>
    </thead>
    <tbody>        
        <?php
            $total = 0;
            $total_discount = 0;
            $total_harga = 0;
            if(count($modDetail) > 0){
                foreach($modDetail as $i=>$detail){
                    $total_harga += $detail->quantity * $detail->prices_sale;
                    $total += $detail->sub_total;
                    $total_discount += $detail->total_discount;
        ?>
        <tr class="produk">
            <td colspan="5"><?php echo isset($detail->product_name) ? $detail->product_name : ""; ?></td>            
        </tr>
		<tr class="produk">
			<td></td>
			<td style="text-align:center;"><?php echo isset($detail->quantity) ? Formatter::formatNumberForCurrency($detail->quantity) : "0"; ?></td>
            <td style="text-align:right;"><?php echo isset($detail->prices_sale) ? Formatter::formatNumberForCurrency($detail->prices_sale,0,'','.') : "0"; ?></td>
            <td style="text-align:center;"><?php echo isset($detail->total_discount) ? Formatter::formatNumberForCurrency($detail->total_discount,0,'','.') : "0"; ?></td>
            <td style="text-align:right;"><?php echo isset($detail->sub_total) ? Formatter::formatNumberForCurrency($detail->sub_total,0,'','.') : "0"; ?></td>
		</tr>
        <?php } ?>
        <?php } ?>
        <tr class="produk">
            <td colspan="5" style="border-top: 2px solid #000000"></td>
        </tr>
    </tbody>
    <tfoot>
        <tr class="footer-produk">
            <td>Total Item <b><?php echo count($modDetail); ?></b></td>
            <td>&nbsp;</td>
            
            <td style="text-align:right;">Total</td>
            <td>: Rp.</td>
            <td colspan="2" style="text-align:right;"><?php echo Formatter::formatNumberForCurrency($total_harga,0,'','.'); ?></td>
        </tr>
        <tr class="footer-produk">
            <td></td>
            <td></td>
            
            <td style="text-align:right;">Diskon</td>
            <td>: Rp.</td>
            <td colspan="2" style="text-align:right;"><?php echo Formatter::formatNumberForCurrency($total_discount,0,'','.'); ?></td>
        </tr>
        <tr class="footer-produk">
            <td></td>
            <td></td>
            
            <td style="text-align:right;">Diskon Point</td>
            <td>: Rp.</td>
            <td colspan="2" style="text-align:right;"><?php echo Formatter::formatNumberForCurrency($model->total_discount_point,0,'','.'); ?></td>
        </tr>
        <tr class="footer-produk">
            <td></td>
            <td></td>
            
            <td style="text-align:right;">Bayar</td>
            <td>: Rp.</td>
            <td colspan="2" style="text-align:right;"><?php echo Formatter::formatNumberForCurrency($model->money_cash,0,'','.'); ?></td>
        </tr>
        <tr class="footer-produk">
            <td></td>
            <td></td>
            
            <td style="text-align:right;">Kembali</td>
            <td>: Rp.</td>
            <td colspan="2" style="text-align:right;"><?php echo Formatter::formatNumberForCurrency($model->money_back,0,'','.'); ?></td>
        </tr>
        <tr class="footer-produk">
            <td colspan="5" style="border-top: 2px solid #000000"></td>
        </tr>
        <tr class="footer-struk">
            <td colspan="5" style="text-align:center;">
                Terima Kasih Atas Kunjungan Anda <br>
                Barang yang sudah dibeli <br>
                Tidak bisa dikembalikan
            </td>
        </tr>
    </tfoot>
</table>