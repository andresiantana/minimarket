<?php 
    if(isset($caraPrint)){
        echo $this->renderPartial('_headerPrint',array('colspan'=>6));      
    }
?>
<table class="table table-bordered">
    <tr>
        <th>Tanggal Penjualan</th>
        <td><?php echo isset($model->date) ? Formatter::formatDateTimeForUser($model->date) : ""; ?></td>
        
        <th>Posisi Pegawai</th>
        <td><?php echo isset($model->employee_id) ? $model->employee->name : ""; ?> <?php echo isset($model->role_id) ? "(".$model->role->role_name.")" : ""; ?></td>
    </tr>
        <th>No. Faktur</th>
        <td><?php echo isset($model->number) ? $model->number : ""; ?></td>
        
        <th>Shift</th>
        <td><?php echo isset($model->shift_id) ? $model->shift->NameAndHours : ""; ?></td>
    </tr>
</table>
<br>
<legend>Detail Penjualan</legend>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No.</th>
            <th>Kategori</th>
            <th>Nama</th>
            <th>Quantity</th>
            <th>Satuan</th>
            <th>Harga (Rp.)</th>
            <th>Diskon (Rp.)</th>
            <th>Sub Total (Rp.)</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if(count($modDetails) > 0){
                $total_quantity = 0;
                $total_harga = 0;
                $total_discount = 0;
                $subtotal = 0;
                foreach($modDetails as $i=>$detail){
                    // total detail
                    $total_quantity += $detail->quantity;
                    $total_harga += $detail->prices_sale;
                    $total_discount += $detail->total_discount;
                    $subtotal += $detail->sub_total;
                    $discount = $detail->total_discount / $total_quantity;
        ?>
        <tr>
            <td><?php echo ($i+1); ?></td>
            <td><?php echo isset($detail->product->category->name) ? $detail->product->category->name : ""; ?></td>
            <td><?php echo isset($detail->product_name) ? $detail->product_name : ""; ?></td>
            <td style="text-align:center;"><?php echo isset($detail->quantity) ? $detail->quantity : 0; ?></td>
            <td style="text-align:center;"><?php echo isset($detail->product_unit) ? ucwords($detail->product_unit) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($detail->prices_sale) ? Formatter::formatNumberForCurrency($detail->prices_sale) : 0; ?></td>
            <td style="text-align:right;"><?php echo isset($detail->total_discount) ? Formatter::formatNumberForCurrency($detail->total_discount) : 0; ?></td>
            <td style="text-align:right;"><?php echo isset($detail->sub_total) ? Formatter::formatNumberForCurrency($detail->sub_total) : 0; ?></td>
        </tr>
        <?php 
                }
            } 
        ?>
    </tbody>
    <tfoot>
        <tr style="font-weight: bold;">
            <td colspan="3" style="text-align:right;"><b>Total</b></td>
            <td style="text-align:center;"><?php echo isset($total_quantity) ? Formatter::formatNumberForCurrency($total_quantity) : ""; ?></td>
            <td style="text-align:right;"></td>
            <td style="text-align:right;"><?php echo isset($total_harga) ? Formatter::formatNumberForCurrency($total_harga) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($total_discount) ? Formatter::formatNumberForCurrency($total_discount) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($subtotal) ? Formatter::formatNumberForCurrency($subtotal) : ""; ?></td>
        </tr>
    </tfoot>
</table>
<?php if(!isset($caraPrint)){ ?>
<div class='panel-footer'>
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="fa fa-print"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'printDetail(\'PRINT\')'))."&nbsp&nbsp"; ?>
</div>
<script type="text/javascript">
    function printDetail(caraPrint)
    {
        window.open('<?php echo $this->createUrl('detailPenjualan',array('id'=>$model->id)); ?>'+'&caraPrint='+caraPrint,'printwin','left=100,top=100,width=480,height=640');
    }    
</script>
<?php } ?>
