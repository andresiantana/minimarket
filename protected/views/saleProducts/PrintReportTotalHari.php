<style>
    .header-report{
        line-height: 0em;
    }
    .body-report{
        line-height: 2em;
    }
</style>
<?php 
    if(isset($caraPrint)){
        echo $this->renderPartial('_headerPrint',array('colspan'=>6));      
    }
?>
<table align="center" cellpadding="0" cellspacing="0">
    <tr class="header-report">
        <td>
            <h3><center><?php echo isset($judulLaporan) ? $judulLaporan : null; ?></center></h3><br>
            <h5><center>Periode: <?php echo isset($periode) ? $periode : null; ?></center></h5>
        </td>
    </tr>
</table>
<table class="table table-bordered">
    <thead>
        <tr style="text-align:center;">
            <th>No.</th>
            <th>Tgl.</th>
            <th>Penjualan</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if(count($model) > 0){
                $total = 0;
                foreach($model as $i=>$data){
                	// total laporan
                    $total += $data->sub_total;
        ?>
        <tr>
            <td style="text-align: left;"><?php echo ($i+1); ?></td>
            <td style="text-align: left;"><?php echo isset($data->date) ? Formatter::formatDateTimeForUser($data->date) : ""; ?></td>
            <td style="text-align: right;"><?php echo isset($data->sub_total) ? Formatter::formatNumberForCurrency($data->sub_total) : ""; ?></td>
        </tr>
        <?php
            }
        } 
        ?>
    </tbody>
    <tbody>
        <tr style="font-weight: bold;">
            <td colspan="2" style="text-align:right;"><b>Total</b></td>
            <td style="text-align:right;"><?php echo isset($total) ? Formatter::formatNumberForCurrency($total) : ""; ?></td>
        </tr>
    </tbody>
</table>
