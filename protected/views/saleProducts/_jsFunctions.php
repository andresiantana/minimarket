<script type="text/javascript">
    function addProduct(id,barcode)
    {
        if(id == ''){
            var id = $('#id').val();
        }
        if(barcode == ''){
            var barcode = $('#barcode').val();
        }
        var quantity = 1;
        $.ajax({
            type:'POST',
            url:'<?php echo $this->createUrl('loadFormSaleProducts'); ?>',
            data: {id:id, barcode:barcode, quantity:quantity},//
            dataType: "json",
            success:function(data){
                if(data.pesan != ''){
                    alert(data.pesan);
                    clearFormProduct();
                    return false;
                }
                var tambahkandetail = true;
                var productyangsama = $("#table-product input[name$='[barcode]'][value='"+barcode+"']");
                if(productyangsama.val()){ //jika ada produk sudah ada di table
                    $("#table-product input[name$='[barcode]'][value='"+barcode+"']").each(function(){
                        var qty_sebelumnya = parseFloat($(this).parents('tr').find("input[name$='[quantity]']").val());
                        $(this).parents('tr').find("input[name$='[quantity]']").val(qty_sebelumnya + 1);
                    });
                    $("#table-product").find('input[name*="[ii]"][class*="integer"]').maskMoney(
                        {"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0}
                    );
                    renameInputRowProducts($("#table-product"));                    
                    hitungTotal();
                    clearFormProduct();
                    // confirm("Apakah anda akan input ulang product ini?","Perhatian!",
                    // function(r){
                    //     if(r){
                    //         $("#table-product input[name$='[product_id]'][value='"+id+"']").each(function(){
                    //             $(this).parents('tr').detach();
                    //         });
                    //         if(tambahkandetail){
                    //                 $('#table-product > tbody').append(data.form);
                    //                     $("#table-product").find('input[name*="[ii]"][class*="integer"]').maskMoney(
                    //                             {"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0}
                    //                     );
                    //                 renameInputRowProducts($("#table-product"));                    
                    //                 hitungTotal();
                    //                 clearFormProduct();
                    //         }
                    //     }else{
                    //         tambahkandetail = false;
                    //         clearFormProduct();
                    //     }
                    // }); 
                }else{
                    if(tambahkandetail){
                        $('#table-product > tbody').append(data.form);
                            $("#table-product").find('input[name*="[ii]"][class*="integer"]').maskMoney(
                                    {"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0}
                            );
                        renameInputRowProducts($("#table-product"));                    
                        hitungTotal();
                        clearFormProduct();
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
        });
    }

    function clearFormProduct(){
        $('#id').val("");
        $('#barcode').val("");
        $('#name').val("");
        $('#barcode').focus();
    }

    function setDiskonPoint(obj){
        unformatNumberSemua();
        var jumlah_point = parseInt(obj.value);
        var diskon_point = parseInt($('#konversi_point_rupiah').val());
        var total_diskon = jumlah_point * (diskon_point * (1/100));

        if(total_diskon < 0){
            total_diskon = 0;
        }
        $("#<?php echo CHtml::activeId($model,'total_discount_point'); ?>").val(total_diskon);
        hitungTotal();
    }

    function hitungTotal(){
        unformatNumberSemua();
        var total = 0;
        var total_price = 0;
        var total_disc = 0;
        var subtotal = 0;
        var prices = 0;
        $('#table-product tbody tr').each(function(){
            var quantity  = parseInt($(this).find('input[name$="[quantity]"]').val());
            var stockMax = parseInt($(this).find('input[name$="[stock]"]').val());
            var prices_sale  = parseInt($(this).find('input[name$="[prices_sale]"]').val());
            var discount_ori  = parseInt($(this).find('input[name$="[discount_ori]"]').val());
            var discount        = parseInt($(this).find('input[name$="[discount]"]').val());
            var min_purchase        = parseInt($(this).find('input[name$="[min_purchase]"]').val());
            var total_discount_ori  = parseInt($(this).find('input[name$="[total_discount_ori]"]').val());
            var total_discount  = parseInt($(this).find('input[name$="[total_discount]"]').val());

            // qty yg dipesan minimum 1      
            if(quantity == 0){
                quantity = 1;
                $(this).find('input[name$="[quantity]"]').val(quantity);
            }

            // qty yg dipesan tdk boleh melibihi stock maximum produk yg ada
            if (quantity > stockMax) {

                // alert validasi jml stock tersedia
                alert("Maaf, jumlah stok produk yang tersedia hanya " + stockMax);

                // set qty beli per produk sama dgn maximum stock
                $(this).find('input[name$="[quantity]"]').val(stockMax);
                quantity = stockMax;
            }

            prices = (prices_sale * quantity);

            if(min_purchase > 0 && discount_ori > 0){
                if(quantity >= min_purchase){
                    discount_ori = discount_ori;
                }else{
                    discount_ori = 0;
                }
            }else{
                discount_ori = 0;
            }
            discount = discount_ori * quantity;
            
            total_discount = (prices_sale * (discount/100));
            total_discount_ori = (prices_sale * (discount_ori/100));
            subtotal = (prices_sale * quantity)  - total_discount;

            if(subtotal <= 0){
                subtotal = 0;
            }

            $(this).find('input[name$="[sub_total]"]').val(subtotal);
            $(this).find('input[name$="[discount]"]').val(discount);
            $(this).find('input[name$="[total_discount]"]').val(total_discount);
            $(this).find('input[name$="[total_discount_ori]"]').val(total_discount_ori);
            
            total += subtotal;
            total_price += prices;
            total_disc += total_discount;
        });
        var total_discount_point  = parseInt($("#<?php echo CHtml::activeId($model,'total_discount_point'); ?>").val());
        if(total_discount_point > 0){
            total = total-total_discount_point;
        }
        $("#<?php echo CHtml::activeId($model,'total_price'); ?>").val(total_price);
        $("#<?php echo CHtml::activeId($model,'total_discount'); ?>").val(total_disc);
        $("#<?php echo CHtml::activeId($model,'sub_total'); ?>").val(total);
        $("#<?php echo CHtml::activeId($model,'money_cash'); ?>").val(total);
        formatNumberSemua();
    }

    function hitungKembalian(){
        unformatNumberSemua();
        var total_kembalian = 0;
        var money_cash = $("#<?php echo CHtml::activeId($model,'money_cash'); ?>").val();
        var subtotal = $("#<?php echo CHtml::activeId($model,'sub_total'); ?>").val();

        total_kembalian = money_cash - subtotal;
        if(total_kembalian < 0){
            total_kembalian = 0;
        }
        var subtotal = $("#<?php echo CHtml::activeId($model,'money_back'); ?>").val(total_kembalian);
        formatNumberSemua();

    }

    function batalPenjualan(obj){
        $(obj).parents('tr').detach();
        hitungTotal();
    }
    /**
    * rename input grid
    */ 
    function renameInputRowProducts(obj_table){
        var row = 0;
        $(obj_table).find("tbody > tr").each(function(){
            $(this).find("#no_urut").val(row+1);
            $(this).find('span').each(function(){ //element <input>
                var old_name = $(this).attr("name").replace(/]/g,"");
                var old_name_arr = old_name.split("[");
                if(old_name_arr.length == 3){
                    $(this).attr("name","["+row+"]["+old_name_arr[2]+"]");
                }
            });
            $(this).find('input,select,textarea').each(function(){ //element <input>
                var old_name = $(this).attr("name").replace(/]/g,"");
                var old_name_arr = old_name.split("[");
                if(old_name_arr.length == 3){
                    $(this).attr("id",old_name_arr[0]+"_"+row+"_"+old_name_arr[2]);
                    $(this).attr("name",old_name_arr[0]+"["+row+"]["+old_name_arr[2]+"]");
                }
            });
            row++;
        });
        $('#id').val('');
        $('#name').val('');
        $('#quantity').val(1);
    }
    
    /**
    * print struk
    */
    function printStruk()
    {
        var myWindow = window.open('<?php echo $this->createUrl('printStruk',array('id'=>(isset($_GET['id']) ? $_GET['id'] : ''))); ?>','printwin','left=100,top=100,width=480,height=640');

        if (myWindow) {
            setTimeout(function() { 
                myWindow.close();
                window.location.href='<?php echo $this->createUrl('index'); ?>';
            }, 1000); // close window in 1 sec.
        }
    }
    
    /**
    * print struk terakhir
    */
    function printStrukTerakhir()
    {
        window.open('<?php echo $this->createUrl('printStrukTerakhir',array()); ?>','printwin','left=100,top=100,width=480,height=640')
    }

    function saveMember()
    {
        $('#btnSaveMember').addClass('disabled');
        var number = $('#<?php echo CHtml::activeId($modMember,'number'); ?>').val();
        var nama_member = $('#<?php echo CHtml::activeId($modMember,'name'); ?>').val();
        var address = $('#<?php echo CHtml::activeId($modMember,'address'); ?>').val();
        var telp_handphone = $('#<?php echo CHtml::activeId($modMember,'phone_handphone'); ?>').val();
        $.ajax({
            type:'POST',
            url:'<?php echo $this->createUrl('simpanMember'); ?>',
            data: {nama_member:nama_member, address:address, telp_handphone:telp_handphone, number:number},//
            dataType: "json",
            success:function(data){
                if(data.pesan != ''){
                    alert(data.pesan);
                    $.fn.yiiGridView.update('member-m-grid', {});
                    //reset field after save
                    $('#btnSaveMember').removeClass('disabled');
                    $('#<?php echo CHtml::activeId($modMember,'number'); ?>').val('');
                    $('#<?php echo CHtml::activeId($modMember,'name'); ?>').val('');
                    $('#<?php echo CHtml::activeId($modMember,'address'); ?>').val('');
                    $('#<?php echo CHtml::activeId($modMember,'phone_handphone'); ?>').val('');
                    return false;
                }                
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
        });
    }

    function clearMember(){
        $('#<?php echo CHtml::activeId($model,'member_id'); ?>').val('');
        $('#<?php echo CHtml::activeId($modMember,'in_point'); ?>').val('');
        $('#<?php echo CHtml::activeId($modMember,'out_point'); ?>').val('');
        $('#<?php echo CHtml::activeId($modMember,'current_point'); ?>').val('');
        $('#no_member').val('');
    }
    $(document).ready(function(){
        var id = '<?php echo isset($_GET['id']) ? $_GET['id'] : null; ?>';
        var frame ='<?php echo isset($_GET['frame']) ? $_GET['frame'] : null; ?>';
        if(id != '' && frame == ''){
            printStruk();
        }
    });
</script>