<?php $profile = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET); ?>
<table width="100%">
    <tr>
        <td style="text-align:center;">
            <b><?php echo isset($profile->name) ? strtoupper($profile->name) : "CAHAYA MUDA TOSERBA"; ?></b><br>
            <?php echo isset($profile->address) ? $profile->address : "Jl. Raya Majalangu Watukumpul, Pemalang"; ?><br>
            <?php echo "Telp. ".isset($profile->phone) ? $profile->phone : "082322965854"; ?>
        </td>
    </tr>
    <tr>
        <td style="border-top: 2px solid #000000"></td>
    </tr>
</table>