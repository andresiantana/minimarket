<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'sale-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('class'=>'form','onKeyPress'=>'return disableKeyPress(event);','enctype' => 'multipart/form-data', 'onSubmit'=>'return requiredCheck(this);'),
        'focus' => '#barcode',
)); ?>

<?php echo $form->errorSummary($model); ?>

<fieldset>
    <?php echo $this->renderPartial('_formPenjualan', array('form'=>$form,'model'=>$model,'modDetails'=>$modDetails,'format'=>$format)); ?>
</fieldset>

<legend>Data Produk</legend>
<fieldset>
    <?php echo $this->renderPartial('_formProduk', array('form'=>$form,'model'=>$model,'modDetails'=>$modDetails,'format'=>$format, 'modMember'=>$modMember)); ?>
</fieldset>
<br/>
<div class="form-group">
    <div class="col-lg-4">
        <?php 
            if(!isset($_GET['id'])){
                $this->widget('booster.widgets.TbButton', array(
                        'buttonType'=>'submit',
                        'context'=>'primary',
                        'label'=>$model->isNewRecord ? 'Bayar' : 'Bayar',
                        'htmlOptions'=>array(
                            'onkeypress'=>'formSubmit(this,event)',
                        )
                ));
                echo "&nbsp;&nbsp;".CHtml::htmlButton(Yii::t('mds','{icon} Print Struk Terakhir',array('{icon}'=>'<i class="fa fa-print"></i>')),array('class'=>'btn btn-info', 'type'=>'button','onclick'=>'printStrukTerakhir();'))."&nbsp&nbsp"; 
            } else {
                echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="fa fa-print"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'printStruk();'))."&nbsp&nbsp"; 
                echo CHtml::link(Yii::t('mds','{icon} Ulangi Penjualan',array('{icon}'=>'<i class="fa fa-refresh"></i>')),$this->createUrl('index',array()), array('class'=>'btn btn-success')); 
            }
        ?>
        <?php //echo CHtml::link(Yii::t('mds','{icon} Daftar Penjualan',array('{icon}'=>'<i class="fa fa-list"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
