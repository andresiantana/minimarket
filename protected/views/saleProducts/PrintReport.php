<style>
    .header-report{
        line-height: 0em;
    }
    .body-report{
        line-height: 2em;
    }
</style>
<?php 
    if(isset($caraPrint)){
        echo $this->renderPartial('_headerPrint',array('colspan'=>6));      
    }
?>
<table align="center" cellpadding="0" cellspacing="0">
    <tr class="header-report">
        <td>
            <h3><center><?php echo isset($judulLaporan) ? $judulLaporan : null; ?></center></h3><br>
            <h5><center>Periode: <?php echo isset($periode) ? $periode : null; ?></center></h5>
        </td>
    </tr>
</table>
<table class="table table-bordered">
    <thead>
        <tr style="text-align:center;" class="body-report">
            <th>No.</th>
            <th>No. Faktur / Tanggal</th>
            <th>Kategori</th>
            <th>Nama</th>
            <th>Quantity</th>
            <th>Harga (Rp.)</th>
            <th>Diskon (Rp.)</th>
            <th>Sub Total (Rp.)</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if(count($model) > 0){
                $total_quantity = 0;
                $total_harga = 0;
                $total_diskon = 0;
                $subtotal = 0;
                foreach($model as $i=>$data){
        ?>
        <tr class="body-report">
            <td style="text-align: left;"><?php echo ($i+1); ?></td>
            <td style="text-align: left;" colspan="7">
                <?php echo isset($data->number) ? $data->number : ""; ?> / <?php echo isset($data->date) ? Formatter::formatDateTimeForUser($data->date) : ""; ?> : 
                <b><?php echo isset($data->employee_id) ? $data->employee->name : ""; ?> / <?php echo isset($data->role_id) ? $data->role->role_name : ""; ?></b>
            </td>
        </tr>
        <?php 
            $modDetails = SaleDetails::model()->findAllByAttributes(array('sale_id'=>$data->id));
                if(count($modDetails) > 0){
                    $quantity = 0;
                    $harga = 0;
                    $diskon = 0;
                    $sub_total = 0;
                    foreach($modDetails as $i=>$detail){
                        $quantity += $detail->quantity;
                        $harga += $detail->prices_sale;
                        $diskon += $detail->total_discount;
                        $sub_total += $detail->sub_total;
                        
                        // total laporan
                        $total_quantity += $detail->quantity;
                        $total_harga += $detail->prices_sale;
                        $total_diskon += $detail->total_discount;
                        $subtotal += $detail->sub_total;
        ?>
        <tr>
            <td colspan="2"></td>
            <td><?php echo isset($detail->product->category->name) ? $detail->product->category->name : ""; ?></td>
            <td><?php echo isset($detail->product_name) ? $detail->product_name : ""; ?>/<?php echo isset($detail->product_unit) ? $detail->product_unit : ""; ?></td>
            <td style="text-align: center;"><?php echo isset($detail->quantity) ? $detail->quantity : 0; ?></td>
            <td style="text-align: right;"><?php echo isset($detail->prices_sale) ? Formatter::formatNumberForCurrency($detail->prices_sale) : 0; ?></td>
            <td style="text-align: right;"><?php echo isset($detail->total_discount) ? Formatter::formatNumberForCurrency($detail->total_discount) : ""; ?></td>
            <td style="text-align: right;"><?php echo isset($detail->sub_total) ? Formatter::formatNumberForCurrency($detail->sub_total) : ""; ?></td>
        </tr>       
        <?php 
                    }
                    
        ?>
        <tr style="font-weight: bold;">
            <td colspan="4" style="text-align:right;"><b>Total Per Transaksi</b></td>
            <td style="text-align:center;"><?php echo isset($quantity) ? Formatter::formatNumberForCurrency($quantity) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($harga) ? Formatter::formatNumberForCurrency($harga) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($diskon) ? Formatter::formatNumberForCurrency($diskon) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($sub_total) ? Formatter::formatNumberForCurrency($sub_total) : ""; ?></td>
        </tr>
        <?php
                }
            }
        } 
        ?>
    </tbody>
    <tbody>
        <tr style="font-weight: bold;">
            <td colspan="4" style="text-align:right;"><b>Total</b></td>
            <td style="text-align:center;"><?php echo isset($total_quantity) ? Formatter::formatNumberForCurrency($total_quantity) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($total_harga) ? Formatter::formatNumberForCurrency($total_harga) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($total_diskon) ? Formatter::formatNumberForCurrency($total_diskon) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($subtotal) ? Formatter::formatNumberForCurrency($subtotal) : ""; ?></td>
        </tr>
    </tbody>
</table>
