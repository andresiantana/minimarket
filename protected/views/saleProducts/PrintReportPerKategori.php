<style type="text/css">
    .header-report{
        line-height: 8px;
    }
    table tr, th, td {
        line-height: 10px !important;
        font-size: 11px !important;

    }
    #content {
        display: table;
        float: right;
        margin-top: 10px;
    }

    #pageFooter {
        display: table-footer-group;
    }

    #pageFooter:after {
        counter-increment: page;
        content: counter(page);
    }
    #pageFooter:after {
        counter-increment: page;
        content:"Page " counter(page) " of " counter(page);
        top: 100%;
        white-space: nowrap; 
        z-index: 20;
        -moz-border-radius: 5px; 
        -moz-box-shadow: 0px 0px 4px #222;  
      }
</style>
<?php 
    if(isset($caraPrint)){
        //echo $this->renderPartial('_headerPrint',array('colspan'=>6));      
    }
    $profile = SystemConfiguration::model()->findByPk(Params::DEFAULT_PROFIL_MARKET);
    $footer = array();
?>
<table align="center" cellpadding="0" cellspacing="0">
    <tr class="header-report">
        <td>
            <h5><b><center><?php echo isset($profile->name) ? strtoupper($profile->name).' TOSERBA' : "CAHAYA MUDA TOSERBA"; ?></center></b></h5>
            <h5><center><?php echo isset($judulLaporan) ? $judulLaporan : null; ?></center></h3>
            <h5><center>Periode: <?php echo isset($periode) ? $periode : null; ?></center></h5>
        </td>
    </tr>
</table>
<table class="table table-bordered">
    <thead>
        <tr style="text-align:center;">
            <th>No. Faktur</th>
            <th>Kode</th>
            <th width="32%">Nama Barang</th>
            <th width="15%">Tgl. Jual</th>
            <th>Qty</th>
            <th>Harga</th>
            <!-- <th>Subtotal</th> -->
            <th>Disc</th>
            <th>Subtotal</th>
        </tr>
    </thead>
    <tbody>        
        <?php $group_kategori = array();
            if(count($modelOrder) > 0){                
                foreach($modelOrder AS $i => $datas){
                    $group_kategori[$datas->product->category_id]['category_id'] = $datas->product->category_id;
                    $group_kategori[$datas->product->category_id]['category_name'] = $datas->product->category->name;      
                }
            }else{
        ?>
        <tr>
            <td colspan="9" style="text-align:left;font-style:italic;"><b>Data tidak ditemukan.</b></td>
        </tr>
        <?php } ?>

        <?php
            if(count($group_kategori > 0)){
                $tot_quantity = 0;
                $tot_discount = 0;
                $tot_harga = 0;
                $tot_diskon = 0;
                $tot_harga_quantity = 0;
                $tot_subtotal = 0;
                foreach($group_kategori as $i=>$group){ ?>
                    <tr>
                        <td colspan='9' style='font-weight:bold; text-align:left;'>Kategori Barang: <?php echo $group['category_name']; ?></td>
                    </tr>
                    <?php 
                        $quantity = 0;
                        $harga = 0;
                        $diskon = 0;
                        $sub_total = 0;
                        $harga_quantity = 0;

                        $total_quantity = 0;
                        $total_discount = 0;
                        $total_harga = 0;
                        $total_diskon = 0;
                        $total_harga_quantity = 0;
                        $subtotal = 0;
                        foreach($modelOrder AS $j => $detail){
                            echo $detail->category_id;
                            if($detail->product->category_id == $group['category_id']){
                                $harga_quantity = $detail->quantity * $detail->prices_sale;
                                $diskon = $detail->total_discount;
                                $sub_total = $harga_quantity - $diskon;
                                
                                // total laporan
                                $total_quantity += $detail->quantity;
                                $total_harga += $detail->prices_sale;
                                $total_harga_quantity += $harga_quantity;
                                $total_diskon += $diskon;
                                $subtotal += $sub_total;
                    ?>
                    <tr>
                        <td><?php echo $detail->number; ?></td>
                        <td><?php echo $detail->product->barcode; ?></td>
                        <td><?php echo $detail->product->name; ?>/<?php echo isset($detail->product_unit) ? $detail->product_unit : ""; ?></td>
                        <td><?php echo isset($detail->create_time) ? date('d/M/Y',strtotime($detail->create_time)) : ""; ?></td>
                        <td style="text-align: center;"><?php echo isset($detail->quantity) ? $detail->quantity : 0; ?></td>
                        <td style="text-align: right;"><?php echo isset($detail->prices_sale) ? Formatter::formatNumberForCurrency($detail->prices_sale) : 0; ?></td>
                        <!-- <td style="text-align: right;"><?php echo isset($harga_quantity) ? Formatter::formatNumberForCurrency($harga_quantity) : ""; ?></td> -->
                        <td style="text-align: right;"><?php echo isset($diskon) ? Formatter::formatNumberForCurrency($diskon) : ""; ?></td>
                        <td style="text-align: right;"><?php echo isset($sub_total) ? Formatter::formatNumberForCurrency($sub_total) : ""; ?></td>
                    </tr>
                    <?php } ?>
                <?php } 
                    $tot_quantity += $total_quantity;
                    $tot_harga += $total_harga;
                    $tot_diskon += $total_diskon;
                    $tot_harga_quantity += $total_harga_quantity;
                    $tot_subtotal += $subtotal;
                ?>
                <tr style="font-weight: bold;">
                    <td colspan="4" style="text-align:right;font-style:italic;"><b>Subtotal Kategori <?php echo $group['category_name']; ?></b></td>
                    <td style="text-align:center;"><?php echo isset($total_quantity) ? Formatter::formatNumberForCurrency($total_quantity) : ""; ?></td>
                    <td style="text-align:right;"><?php echo isset($total_harga) ? Formatter::formatNumberForCurrency($total_harga) : ""; ?></td>
                    <!-- <td style="text-align:right;"><?php echo isset($total_harga_quantity) ? Formatter::formatNumberForCurrency($total_harga_quantity) : ""; ?></td> -->
                    <td style="text-align:right;"><?php echo isset($total_diskon) ? Formatter::formatNumberForCurrency($total_diskon) : ""; ?></td>
                    <td style="text-align:right;"><?php echo isset($subtotal) ? Formatter::formatNumberForCurrency($subtotal) : ""; ?></td>
                </tr>
        <?php }
            } ?>
    </tbody>
    <tbody>
        <tr style="font-weight: bold;">
            <td colspan="4" style="text-align:right;"><b>Total Penjualan</b></td>
            <td style="text-align:center;"><?php echo isset($tot_quantity) ? Formatter::formatNumberForCurrency($tot_quantity) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($tot_harga) ? Formatter::formatNumberForCurrency($tot_harga) : ""; ?></td>
            <!-- <td style="text-align:right;"><?php echo isset($tot_harga_quantity) ? Formatter::formatNumberForCurrency($tot_harga_quantity) : ""; ?></td> -->
            <td style="text-align:right;"><?php echo isset($tot_diskon) ? Formatter::formatNumberForCurrency($tot_diskon) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($tot_subtotal) ? Formatter::formatNumberForCurrency($tot_subtotal) : ""; ?></td>
        </tr>
    </tbody>
</table>
<div id="content">
  <div id="pageFooter"></div>
</div>