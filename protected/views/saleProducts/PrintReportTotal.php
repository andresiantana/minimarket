<style>
    .header-report{
        line-height: 0em;
    }
    .body-report{
        line-height: 2em;
    }
</style>
<?php 
    if(isset($caraPrint)){
        echo $this->renderPartial('_headerPrint',array('colspan'=>6));      
    }
?>
<table align="center" cellpadding="0" cellspacing="0">
    <tr class="header-report">
        <td>
            <h3><center><?php echo isset($judulLaporan) ? $judulLaporan : null; ?></center></h3><br>
            <h5><center>Periode: <?php echo isset($periode) ? $periode : null; ?></center></h5>
        </td>
    </tr>
</table>
<table class="table table-bordered">
    <thead>
        <tr style="text-align:center;">
            <th>No.</th>
            <th>No. Faktur / Tanggal</th>
            <th>Tgl. Faktur</th>
            <th>Kasir</th>
            <th>Total Harga (Rp.)</th>
            <th>Total Diskon (Rp.)</th>
            <th>Sub Total (Rp.)</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if(count($model) > 0){
                $total_harga = 0;
                $total_diskon = 0;
				$total_pengeluaran = 0;
				$total_point = 0;
                $subtotal = 0;
				$selisih = 0;
				
				$member_before = '';
                foreach($model as $i=>$data){
					// get total point
					$member_id = isset($data->member_id) ? $data->member_id : '';
					if(!empty($member_id)){
						$member_after = $member_id;
						$modelMember = Member::model()->findByPk($member_id);
						$point = isset($modelMember->out_point) ? $modelMember->out_point : 0;
						if($member_before != $member_after){
							$total_point += $point;
						}
						$member_before = $member_after;
					}
					
					// get total_pengeluaran
					$total_pengeluaran = isset($modelOrder->sub_total) ? $modelOrder->sub_total : 0;
							
                	// total laporan
                    $total_harga += $data->total_price;
                    $total_diskon += $data->total_discount;
                    $subtotal += $data->sub_total;
					
					// selisih pembelian - penjualan
					$selisih = $total_pengeluaran - $subtotal;
					if($selisih <= 0){
						$selisih = 0;
					}
        ?>
        <tr>
            <td style="text-align: left;"><?php echo ($i+1); ?></td>
            <td style="text-align: left;">
                <?php echo isset($data->number) ? $data->number : ""; ?>
            </td>
            <td style="text-align: left;"><?php echo isset($data->date) ? Formatter::formatDateTimeForUser($data->date) : ""; ?></td>
            <td style="text-align: left;"><?php echo isset($data->employee_id) ? $data->employee->name : ""; ?></td>
            <td style="text-align: right;"><?php echo isset($data->total_price) ? Formatter::formatNumberForCurrency($data->total_price) : 0; ?></td>
            <td style="text-align: right;"><?php echo isset($data->total_discount) ? Formatter::formatNumberForCurrency($data->total_discount) : ""; ?></td>
            <td style="text-align: right;"><?php echo isset($data->sub_total) ? Formatter::formatNumberForCurrency($data->sub_total) : ""; ?></td>
        </tr>
        <?php
            }
        } 
        ?>
    </tbody>
    <tbody>
        <tr style="font-weight: bold;">
            <td colspan="4" style="text-align:right;"><b>Total</b></td>
            <td style="text-align:right;"><?php echo isset($total_harga) ? Formatter::formatNumberForCurrency($total_harga) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($total_diskon) ? Formatter::formatNumberForCurrency($total_diskon) : ""; ?></td>
            <td style="text-align:right;"><?php echo isset($subtotal) ? Formatter::formatNumberForCurrency($subtotal) : ""; ?></td>
        </tr>
		<tr style="font-weight: bold;">
            <td colspan="4" style="text-align:right;"><b>Total Pendapatan</b></td>
            <td colspan="3" style="text-align:right;"><?php echo isset($subtotal) ? Formatter::formatNumberForCurrency($subtotal) : ""; ?></td>
        </tr>
		<tr style="font-weight: bold;">
            <td colspan="4" style="text-align:right;"><b>Total Pengeluaran</b></td>
            <td colspan="3" style="text-align:right;"><?php echo isset($total_pengeluaran) ? Formatter::formatNumberForCurrency($total_pengeluaran) : ""; ?></td>
        </tr>		
		<tr style="font-weight: bold;">
            <td colspan="4" style="text-align:right;"><b>Total Diskon</b></td>
            <td colspan="3" style="text-align:right;"><?php echo isset($total_diskon) ? Formatter::formatNumberForCurrency($total_diskon) : ""; ?></td>
        </tr>
		<tr style="font-weight: bold;">
            <td colspan="4" style="text-align:right;"><b>Total Penukaran Point</b></td>
            <td colspan="3" style="text-align:right;"><?php echo isset($total_point) ? Formatter::formatNumberForCurrency($total_point) : ""; ?></td>
        </tr>
		<tr style="font-weight: bold;">
            <td colspan="4" style="text-align:right;"><b>Total Selisih Pendapatan</b></td>
            <td colspan="3" style="text-align:right;"><?php echo isset($selisih) ? Formatter::formatNumberForCurrency($selisih) : 0; ?></td>
        </tr>
    </tbody>
</table>
