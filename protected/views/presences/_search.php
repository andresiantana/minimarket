<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type'=>'horizontal',
        'id'=>'search-form',
	'htmlOptions'=>array('class'=>'form'),
)); ?>

    <div class="row-fluid">
        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model,'start_date', array('class'=>'col-sm-3 control-label')) ?>
                <div class="col-md-6">
                    <?php
                        $model->start_date = isset($model->start_date) ? $format->formatDateTimeForUser($model->start_date) : date('d M Y');
                        $this->widget('DateTimePicker',array(
                            'model'=>$model,
                            'attribute'=>'start_date',
                            'mode'=>'date',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_FORMAT,
                            ),
                            'htmlOptions'=>array(
                                'onkeyup'=>"return $(this).focusNextInputField(event)",
                                'readonly'=>false,
                                'class'=>'span3 form-control',
                            ),
                        ));
                        $model->start_date = isset($model->start_date) ? $format->formatDateTimeForDb($model->start_date) : "";
                    ?>
                </div>
            </div>    
            
            <div class="form-group">
                <?php echo CHtml::activeLabel($model,'employee_id',array('class'=>'col-sm-3 control-label')); ?>
                <div class="col-md-6">
                    <?php echo CHtml::activeDropDownList($model,'employee_id', CHtml::listData(Employee::model()->findAll(),'id','name'),array('class'=>'span5 form-control','empty'=>'--Pilih--')); ?>
                </div>
            </div> 
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model,'end_date', array('class'=>'col-sm-3 control-label')) ?>
                <div class="col-md-6">
                    <?php
                        $model->end_date = isset($model->end_date) ? $format->formatDateTimeForUser($model->end_date) : date('d M Y');
                        $this->widget('DateTimePicker',array(
                            'model'=>$model,
                            'attribute'=>'end_date',
                            'mode'=>'date',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_FORMAT,
                            ),
                            'htmlOptions'=>array(
                                'onkeyup'=>"return $(this).focusNextInputField(event)",
                                'readonly'=>false,
                                'class'=>'span3 form-control',
                            ),
                        ));
                        $model->end_date = isset($model->end_date) ? $format->formatDateTimeForDb($model->end_date) : "";
                    ?>
                </div>
            </div>                     
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-12">
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'context'=>'primary',
                    'label'=>'Cari',
            )); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>
