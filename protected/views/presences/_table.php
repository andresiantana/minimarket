<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'presence-report-grid',
    'dataProvider'=>$model->searchReport(),
    'template'=>"{summary}\n{items}\n{pager}",
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
    'columns'=>array(
        array(
            'header'=>'No.',
            'value' => '($this->grid->dataProvider->pagination) ? 
                            ($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
                            : ($row+1)',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:right;width:5%;'),
        ),
        array(
            'header'=>'Nama Pegawai',
            'type'=>'raw',
            'value'=>'isset($data->employee_id) ? $data->employee->name : ""',
        ),
        array(
            'header'=>'Jam Masuk',
            'type'=>'raw',
            'value'=>'isset($data->in_hours) ? Formatter::formatDateTimeForUser($data->in_hours) : ""',
        ),
        array(
            'header'=>'Jam Keluar',
            'type'=>'raw',
            'value'=>'isset($data->out_hours) ? Formatter::formatDateTimeForUser($data->out_hours) : ""',
        ),
    ),
)); ?>