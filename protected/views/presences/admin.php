<?php
$this->breadcrumbs=array(
            'Presences'=>array('index'),
            'Manage',
    );

$this->menu=array(
    array('label'=>'List Presences','url'=>array('index')),
    array('label'=>'Create Presences','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('#search-form').submit(function(){
        $.fn.yiiGridView.update('presence-report-grid', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Laporan <small>Presensi</small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <legend>Pencarian</legend>
                <div class="search-form">
                    <?php $this->renderPartial('_search',array(
                        'model'=>$model,
                        'format'=>$format
                    )); ?>
                </div><!-- search-form -->
            </div>
            <div class="panel-body">
                <?php $this->renderPartial('_table',array(
                         'model'=>$model,
                        'format'=>$format
                    )); ?>
            </div>
            <div class='panel-footer'>
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="fa fa-print"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'printReport(\'PRINT\')'))."&nbsp&nbsp"; ?>
            </div>
    </div>
</div>
    <script type="text/javascript">
    function printReport(caraPrint)
    {
        window.open("<?php echo $this->createUrl('printReport'); ?>/"+$('#search-form').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=1100px, scrollbars=yes');
    }    
</script>
