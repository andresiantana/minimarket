<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('_headerPrint',array('judulLaporan'=>$judulLaporan));      
?>
<table align="center">
    <tr>
        <td>
            <h3><center><?php echo isset($judulLaporan) ? $judulLaporan : null; ?></center></h3><br>
            <h5><center>Periode: <?php echo isset($periode) ? $periode : null; ?></center></h5>
        </td>
    </tr>
        <td></td>
    </tr>
</table>
<?php $this->widget('booster.widgets.TbGridView',array(
    'id'=>'presence-report-grid',
    'dataProvider'=>$model->searchReportPrint(),
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
    'columns'=>array(
        array(
            'header'=>'No.',
            'value' => '($this->grid->dataProvider->pagination) ? 
                            ($this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1)
                            : ($row+1)',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:right;width:5%;'),
        ),
        array(
            'header'=>'Nama Pegawai',
            'type'=>'raw',
            'value'=>'isset($data->employee_id) ? $data->employee->name : ""',
        ),
        array(
            'header'=>'Jam Masuk',
            'type'=>'raw',
            'value'=>'isset($data->in_hours) ? Formatter::formatDateTimeForUser($data->in_hours) : ""',
        ),
        array(
            'header'=>'Jam Keluar',
            'type'=>'raw',
            'value'=>'isset($data->out_hours) ? Formatter::formatDateTimeForUser($data->out_hours) : ""',
        ),
    ),
)); ?>