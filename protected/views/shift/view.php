<?php
$this->breadcrumbs=array(
	'Shifts'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('label'=>'List Shift','url'=>array('index')),
    array('label'=>'Create Shift','url'=>array('create')),
    array('label'=>'Update Shift','url'=>array('update','id'=>$model->id)),
    array('label'=>'Delete Shift','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage Shift','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Lihat <small>Jam Kerja #<?php echo $model->name; ?></small>
        </h1>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                
            </div>
            <div class="panel-body">
                <?php $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
//                        		'id',
		'name',
		'start_hours',
		'end_hours',
//		'is_delete',
//		'create_time',
//		'update_time',
//		'create_user_id',
//		'update_user_id',
                    ),
                )); ?>
            </div>
            <div class='panel-footer'>
                    <?php // echo CHtml::link(Yii::t('mds','{icon} Ubah',array('{icon}'=>'<i class="fa fa-edit"></i>')),$this->createUrl('update',array('id'=>$model->id)), array('class'=>'btn btn-success')); ?>
                    <?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan Jam Kerja',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
            </div>
        </div>
    </div>
</div>
