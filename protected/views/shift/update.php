<?php
$this->breadcrumbs=array(
	'Shifts'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('label'=>'List Shift','url'=>array('index')),
    array('label'=>'Create Shift','url'=>array('create')),
    array('label'=>'View Shift','url'=>array('view','id'=>$model->id)),
    array('label'=>'Manage Shift','url'=>array('admin')),
);
?>

<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            Ubah <small>Jam Kerja #<?php echo $model->name; ?></small>
        </h1>
        <?php $this->widget('booster.widgets.TbAlert'); ?>
    </div>
</div> 
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                
            </div>
            <div class="panel-body">
                <?php echo $this->renderPartial('_form',array('model'=>$model,'format'=>$format)); ?>
            </div>
        </div>
    </div>
</div>