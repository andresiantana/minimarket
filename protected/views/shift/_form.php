<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'shift-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus' => '#shift-form div.form-group:first-child div input',
        'htmlOptions'=>array('class'=>'form','onKeyPress'=>'return disableKeyPress(event);', 'enctype' => 'multipart/form-data'),
)); ?>

<p class="help-block">Inputan dengan tanda <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>10)))); ?>

    <div class="form-group ">
        <?php echo $form->labelEx($model,'start_hours', array('class'=>'col-sm-3 control-label')) ?>
        <div class="col-sm-3">
        <?php   
            $this->widget('DateTimePicker',array(
                'model'=>$model,
                'attribute'=>'start_hours',
                'mode'=>'time',
                'options'=> array(
//                    'dateFormat'=>Params::DATE_FORMAT,
                ),
                'htmlOptions'=>array(
                    'readonly'=>true,
                    'class'=>'span3 form-control',
                ),
            ));
        ?>
        </div>
    </div>
    
    <div class="form-group ">
        <?php echo $form->labelEx($model,'end_hours', array('class'=>'col-sm-3 control-label')) ?>
        <div class="col-sm-3">
        <?php   
            $this->widget('DateTimePicker',array(
                'model'=>$model,
                'attribute'=>'end_hours',
                'mode'=>'time',
                'options'=> array(
//                    'dateFormat'=>Params::DATE_FORMAT,
                ),
                'htmlOptions'=>array(
                    'readonly'=>true,
                    'class'=>'span3 form-control',
                ),
            ));
        ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-4">
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'context'=>'primary',
                    'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
            )); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Pengaturan Jam Kerja',array('{icon}'=>'<i class="fa fa-gear"></i>')),$this->createUrl('admin',array()), array('class'=>'btn btn-success')); ?>
        </div>
    </div>
<?php $this->endWidget(); ?>
