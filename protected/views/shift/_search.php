<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type'=>'horizontal',
        'id'=>'search-form',
	'htmlOptions'=>array('class'=>'form'),
)); ?>

        <?php echo $form->dropDownListGroup($model,'name', array('widgetOptions'=>array('data'=>CHtml::listData(Shift::model()->findAll(),'name','name'),'htmlOptions'=>array('class'=>'span5','empty'=>'--Pilih--')))); ?>

	<div class="form-group">
            <div class="col-lg-4">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Cari',
		)); ?>
            </div>
	</div>

<?php $this->endWidget(); ?>
