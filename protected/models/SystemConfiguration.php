<?php

/**
 * This is the model class for table "system_configurations".
 *
 * The followings are the available columns in table 'system_configurations':
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property integer $is_absensi
 * @property integer $is_active
 * @property integer $is_delete
 * @property integer $is_member
 * @property double $point_to_idr
 * @property double $value_point
 * @property string $create_time
 * @property string $update_time
 */
class SystemConfiguration extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SystemConfigurations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'system_configurations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, create_time', 'required'),
			array('is_absensi, is_active, is_delete, is_member', 'numerical', 'integerOnly'=>true),
			array('point_to_idr, value_point', 'numerical'),
			array('name', 'length', 'max'=>30),
			array('phone', 'length', 'max'=>22),
			array('address, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, address, phone, is_absensi, is_active, is_delete, is_member, point_to_idr, value_point, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Toko / Minimarket',
			'address' => 'Alamat',
			'phone' => 'No. Telp',
			'is_absensi' => 'Absensi',
			'is_active' => 'Status',
			'is_delete' => 'Is Delete',
			'is_member' => 'Members',
			'point_to_idr' => 'Konversi (Rp.) per Point',
			'value_point' => 'Nilai Point',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('LOWER(name)',strtolower($this->name),true);
		$criteria->compare('LOWER(address)',strtolower($this->address),true);
		$criteria->compare('LOWER(phone)',strtolower($this->phone),true);
		$criteria->compare('is_absensi',$this->is_absensi);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('is_delete',$this->is_delete);
		$criteria->compare('is_member',$this->is_member);
		$criteria->compare('point_to_idr',$this->point_to_idr);
		$criteria->compare('value_point',$this->value_point);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);

		return $criteria;
	}
        
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->criteriaSearch();
                $criteria->limit=10;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=$this->criteriaSearch();
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}