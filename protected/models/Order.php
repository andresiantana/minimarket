<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property string $date
 * @property string $number
 * @property double $total_price
 * @property double $total_discount
 * @property double $sub_total
 * @property integer $employee_id
 * @property integer $role_id
 * @property integer $shift_id
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id
 * @property integer $update_user_id
 */
class Order extends CActiveRecord
{
    public $start_date,$end_date;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'orders';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date, number, employee_id, role_id, shift_id, create_time, create_user_id', 'required'),
            array('employee_id, role_id, shift_id, create_user_id, update_user_id', 'numerical', 'integerOnly'=>true),
            array('total_price, total_discount, sub_total', 'numerical'),
            array('number', 'length', 'max'=>30),
            array('update_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, date, number, total_price, total_discount, sub_total, employee_id, role_id, shift_id, create_time, update_time, create_user_id, update_user_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'employee'=>array(self::BELONGS_TO,'Employee','employee_id'),
            'role'=>array(self::BELONGS_TO,'Role','role_id'),
            'shift'=>array(self::BELONGS_TO,'Shift','shift_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'date' => 'Tanggal Pembelian',
            'number' => 'No. Faktur',
            'total_price' => 'Total Harga',
            'total_discount' => 'Total Diskon',
            'sub_total' => 'Sub Total',
            'employee_id' => 'Pegawai',
            'role_id' => 'Posisi Pegawai',
            'shift_id' => 'Jam Kerja',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'create_user_id' => 'Create User',
            'update_user_id' => 'Update User',
            'start_date' => 'Tanggal Pembelian',
            'end_date' => 'Sampai Dengan',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CdbCriteria that can return criterias.
     */
    public function criteriaSearch()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('LOWER(date)',strtolower($this->date),true);
        $criteria->compare('LOWER(number)',strtolower($this->number),true);
        $criteria->compare('total_price',$this->total_price);
        $criteria->compare('total_discount',$this->total_discount);
        $criteria->compare('sub_total',$this->sub_total);
        $criteria->compare('employee_id',$this->employee_id);
        $criteria->compare('role_id',$this->role_id);
        $criteria->compare('shift_id',$this->shift_id);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('create_user_id',$this->create_user_id);
        $criteria->compare('update_user_id',$this->update_user_id);

        return $criteria;
    }

     /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CdbCriteria that can return criterias.
     */
    public function criteriaSearchReport()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->addBetweenCondition('DATE(date)', $this->start_date, $this->end_date, true);
        $criteria->compare('id',$this->id);
        $criteria->compare('LOWER(number)',strtolower($this->number),true);
        $criteria->compare('total_price',$this->total_price);
        $criteria->compare('total_discount',$this->total_discount);
        $criteria->compare('sub_total',$this->sub_total);
        $criteria->compare('employee_id',$this->employee_id);
        $criteria->compare('role_id',$this->role_id);
        if(!empty($this->shift_id)){
            $criteria->addCondition('shift_id = '.$this->shift_id);
        }
        if(Yii::app()->user->role_id != Params::ROLE_OWNER){
            $criteria->compare('role_id',Yii::app()->user->role_id);
            //$criteria->compare('create_user_id',Yii::app()->user->id);
        }
        
        return $criteria;
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearch();
        $criteria->limit=10;

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }


    public function searchPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearch();
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchReport()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearchReport();
        $criteria->limit=10;

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }


    public function searchPrintReport()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearchReport();
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
    }
}