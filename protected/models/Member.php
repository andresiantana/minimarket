<?php

/**
 * This is the model class for table "members".
 *
 * The followings are the available columns in table 'members':
 * @property integer $id
 * @property string $date
 * @property string $number
 * @property string $name
 * @property string $address
 * @property string $phone_handphone
 * @property double $in_point
 * @property double $out_point
 * @property double $current_point
 */
class Member extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Member the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'members';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('number, name', 'required'),
			array('in_point, out_point, current_point', 'numerical'),
			array('number', 'length', 'max'=>100),
			array('name', 'length', 'max'=>50),
			array('phone_handphone', 'length', 'max'=>30),
			array('date, address, is_delete', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date, number, name, address, phone_handphone, in_point, out_point, current_point, is_delete', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Tgl. Daftar Member',
			'number' => 'No. Member',
			'name' => 'Nama',
			'address' => 'Alamat',
			'phone_handphone' => 'No. Telp/No. Hp',
			'in_point' => 'Poin Masuk',
			'out_point' => 'Poin Keluar',
			'current_point' => 'Poin Sekarang',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('LOWER(date)',strtolower($this->date),true);
		$criteria->compare('LOWER(number)',strtolower($this->number),true);
		$criteria->compare('LOWER(name)',strtolower($this->name),true);
		$criteria->compare('LOWER(address)',strtolower($this->address),true);
		$criteria->compare('LOWER(phone_handphone)',strtolower($this->phone_handphone),true);
		$criteria->compare('in_point',$this->in_point);
		$criteria->compare('out_point',$this->out_point);
		$criteria->compare('current_point',$this->current_point);
		$criteria->addSearchCondition('is_delete', '%0%', false);

		return $criteria;
	}
        
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->criteriaSearch();
        $criteria->limit=10;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
    /**
     * [Search Print]
     * @return [obj]
     */
    public function searchPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearch();
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
    }
}