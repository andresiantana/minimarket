<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $barcode
 * @property string $unit
 * @property string $expire_date
 * @property double $stock
 * @property double $min_stock
 * @property double $min_purchase
 * @property double $price
 * @property double $price_sale
 * @property double $discount
 * @property integer $is_delete
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id
 * @property integer $update_user_id
 */
class Product extends CActiveRecord
{
    public $category_name, $periode_kadaluarsa, $tgl_kadaluarsa_awal, $tgl_kadaluarsa_akhir, $min_purchase;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, name, barcode, unit, expire_date, create_time, create_user_id', 'required'),
			array('category_id, is_delete, create_user_id, update_user_id', 'numerical', 'integerOnly'=>true),
			array('stock, min_stock, min_purchase, price_sale, discount', 'numerical'),
			array('name', 'length', 'max'=>50),
			array('unit', 'length', 'max'=>20),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_id, name, barcode, unit, expire_date, stock, min_stock, min_purchase, price_sale, discount, is_delete, create_time, update_time, create_user_id, update_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'category'=>array(self::BELONGS_TO,'Category','category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Kategori',
			'name' => 'Nama',
			'barcode' => 'Barcode',
			'unit' => 'Satuan',
			'expire_date' => 'Tanggal Kadaluarsa',
			'stock' => 'Stok',
			'min_stock' => 'Min. Stok',
			'min_purchase' => 'Min. Beli',
			'price_sale' => 'Harga Jual',
			'discount' => 'Diskon (%)',
			'is_delete' => 'Is Delete',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_user_id' => 'Create User',
			'update_user_id' => 'Update User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('LOWER(name)',strtolower($this->name),true);
		$criteria->compare('LOWER(barcode)',strtolower($this->barcode),true);
		$criteria->compare('LOWER(unit)',strtolower($this->unit),true);
		$criteria->compare('LOWER(expire_date)',strtolower($this->expire_date),true);
		$criteria->compare('stock',$this->stock);
		$criteria->compare('min_stock',$this->min_stock);
		$criteria->compare('min_purchase',$this->min_purchase);
		$criteria->compare('price_sale',$this->price_sale);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('is_delete',isset($this->is_delete) ? $this->is_delete : 0);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_user_id',$this->update_user_id);

		return $criteria;
	}
        
        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearchProduct()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $tgl = date('Y-m-d');
        $criteria->with = array('category');
        
        if(!empty($this->periode_kadaluarsa)){
            if($this->periode_kadaluarsa == 1){
                $this->tgl_kadaluarsa_awal = strtotime($tgl.' + 1 month');
                $this->tgl_kadaluarsa_awal = date('Y-m-d',$this->tgl_kadaluarsa_awal);
                
                $criteria->addCondition("DATE(t.expire_date) < '".$this->tgl_kadaluarsa_awal."'");
            }else if($this->periode_kadaluarsa == 2){
                $this->tgl_kadaluarsa_akhir = strtotime($tgl.' + 3 month');
                $this->tgl_kadaluarsa_awal = strtotime($tgl.' + 1 month');
                
                $this->tgl_kadaluarsa_awal = date('Y-m-d',$this->tgl_kadaluarsa_awal);
                $this->tgl_kadaluarsa_akhir = date('Y-m-d',$this->tgl_kadaluarsa_akhir);
                
                $criteria->addBetweenCondition('DATE(t.expire_date)', $this->tgl_kadaluarsa_awal, $this->tgl_kadaluarsa_akhir);
            }else if($this->periode_kadaluarsa == 3){
                $this->tgl_kadaluarsa_awal = strtotime($tgl.' + 3 month');
                $this->tgl_kadaluarsa_awal = date('Y-m-d',$this->tgl_kadaluarsa_awal);
                
                $criteria->addCondition("DATE(t.expire_date) > '".$this->tgl_kadaluarsa_awal."'");
            }
        }
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.category_id',$this->category_id);
        if(!empty($this->category_name)){
            $criteria->compare('LOWER(category.name)',strtolower($this->category_name),true);
        }
        if(!empty($this->expire_date)){
            $criteria->addCondition("DATE(t.expire_date) = '".$this->expire_date."'");
        }
		$criteria->compare('LOWER(t.name)',strtolower($this->name),true);
		$criteria->compare('LOWER(t.barcode)',strtolower($this->barcode),true);
		$criteria->compare('LOWER(t.unit)',strtolower($this->unit),true);
		$criteria->compare('t.stock',$this->stock);
		$criteria->compare('t.min_stock',$this->min_stock);
		$criteria->compare('t.min_purchase',$this->min_purchase);
		$criteria->compare('t.price_sale',$this->price_sale);
        $criteria->compare('t.discount',$this->discount);
		$criteria->compare('t.is_delete',isset($this->is_delete) ? $this->is_delete : 0);
        $criteria->compare('LOWER(t.create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(t.update_time)',strtolower($this->update_time),true);
		$criteria->compare('t.create_user_id',$this->create_user_id);
		$criteria->compare('t.update_user_id',$this->update_user_id);
		$criteria->addCondition('t.stock >= 0');
        $criteria->order = 't.stock ASC, t.expire_date ASC';
        
        return $criteria;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->criteriaSearch();
                $criteria->limit=10;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
    public function searchProduct()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->criteriaSearchProduct();
		$criteria->order = 't.stock ASC, t.expire_date ASC';
        // $criteria->order = 't.expire_date ASC';
        $criteria->limit=10;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => [
		        'pageSize' => 30,
		    ],
		));
	}
        
    public function searchPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearch();
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
    }
        
    public function getName(){
        $name = $this->name;
        $tgl = date('Y-m-d');
        $tgl_kadaluarsa_awal_1 = strtotime($tgl.' + 1 month');
        $tgl_kadaluarsa_awal_1 = date('Y-m-d',$tgl_kadaluarsa_awal_1);
        
        $tgl_kadaluarsa_awal_2 = strtotime($tgl.' + 3 month');
        $tgl_kadaluarsa_akhir = strtotime($tgl.' + 1 month');
        
        $tgl_kadaluarsa_awal_2 = date('Y-m-d',$tgl_kadaluarsa_awal_2);
        $tgl_kadaluarsa_akhir = date('Y-m-d',$tgl_kadaluarsa_akhir);
        
        $tgl_kadaluarsa_awal_3 = strtotime($tgl.' + 3 month');
        $tgl_kadaluarsa_awal_3 = date('Y-m-d',$tgl_kadaluarsa_awal_3);

        if($this->expire_date < $tgl_kadaluarsa_awal_1){
            $name = "<a style='color:red;'>".$this->name."</a>";
        }else if(($this->expire_date <= $tgl_kadaluarsa_awal_2 ) && ($this->expire_date >= $tgl_kadaluarsa_akhir)){
            $name = "<a style='color:#F99720;'>".$this->name."</a>";
        }else if($this->expire_date > $tgl_kadaluarsa_awal_3){
            $name = "<a style='color:green;'>".$this->name."</a>";
        }else{
            $name = $this->name;
        }
        
        return $name;
    }

    public function searchPrintStok()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearchProduct();
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
    }
}