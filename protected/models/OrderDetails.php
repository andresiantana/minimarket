<?php

/**
 * This is the model class for table "orders_details".
 *
 * The followings are the available columns in table 'orders_details':
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $product_name
 * @property string $product_barcode
 * @property string $product_unit
 * @property double $quantity
 * @property double $prices_order
 * @property double $discount
 * @property double $total_discount
 * @property double $sub_total
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id
 * @property integer $update_user_id
 */
class OrderDetails extends CActiveRecord
{
        public $name,$category_name,$barcode,$discount_ori,$total_discount_ori,$date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OrderDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, create_time, create_user_id', 'required'),
			array('order_id, product_id, create_user_id, update_user_id', 'numerical', 'integerOnly'=>true),
			array('quantity, prices_order, discount, total_discount, sub_total', 'numerical'),
			array('product_name, product_barcode, product_unit', 'length', 'max'=>50),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, order_id, product_id, product_name, product_barcode, product_unit, quantity, prices_order, discount, total_discount, sub_total, create_time, update_time, create_user_id, update_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'product'=>array(self::BELONGS_TO,'Product','product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
            return array(
                'id' => 'ID',
                'order_id' => 'Pembelian',
                'product_id' => 'Produk',
                'product_name' => 'Nama',
                'product_barcode' => 'Barcode',
                'product_unit' => 'Satuan',
                'quantity' => 'Quantity',
                'prices_order' => 'Harga Pembelian',
                'discount' => 'Diskon',
                'total_discount' => 'Total Diskon',
                'sub_total' => 'Sub Total',
                'create_time' => 'Create Time',
                'update_time' => 'Update Time',
                'create_user_id' => 'Create User',
                'update_user_id' => 'Update User',
            );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CdbCriteria that can return criterias.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('LOWER(product_name)',strtolower($this->product_name),true);
		$criteria->compare('LOWER(product_barcode)',strtolower($this->product_barcode),true);
		$criteria->compare('LOWER(product_unit)',strtolower($this->product_unit),true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('prices_order',$this->prices_order);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('total_discount',$this->total_discount);
		$criteria->compare('sub_total',$this->sub_total);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_user_id',$this->update_user_id);

		return $criteria;
	}
        
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=$this->criteriaSearch();
                $criteria->limit=10;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=$this->criteriaSearch();
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}