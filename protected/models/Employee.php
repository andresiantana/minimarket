<?php

/**
 * This is the model class for table "employees".
 *
 * The followings are the available columns in table 'employees':
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $address
 * @property string $handphone
 * @property integer $shift_id
 * @property integer $is_delete
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property integer $is_have_user
 */
class Employee extends CActiveRecord
{
    public $shift_name;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Employee the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'employees';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, address, handphone, shift_id, create_time, create_user_id', 'required'),
            array('shift_id, is_delete, create_user_id, update_user_id, is_have_user', 'numerical', 'integerOnly'=>true),
            array('name, code', 'length', 'max'=>50),
            array('handphone', 'length', 'max'=>22),
            array('update_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, code, address, handphone, shift_id, is_delete, create_time, update_time, create_user_id, update_user_id, is_have_user', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'shift'=>array(self::BELONGS_TO,'Shift','shift_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code' => 'No. Pegawai',
            'name' => 'Nama',
            'address' => 'Alamat',
            'handphone' => 'No. Hp',
            'shift_id' => 'Jam Kerja',
            'is_delete' => 'Is Delete',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'create_user_id' => 'Create User',
            'update_user_id' => 'Update User',
            'is_have_user' => 'Is Have User',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CdbCriteria that can return criterias.
     */
    public function criteriaSearch()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('shift');

        $criteria->compare('t.id',$this->id);
        $criteria->compare('LOWER(t.code)',strtolower($this->code),true);
        $criteria->compare('LOWER(t.name)',strtolower($this->name),true);
        $criteria->compare('LOWER(t.address)',strtolower($this->address),true);
        $criteria->compare('LOWER(t.handphone)',strtolower($this->handphone),true);
        $criteria->compare('t.shift_id',$this->shift_id);
        if(!empty($this->shift_name)){
            $criteria->compare('LOWER(shift.name)',strtolower($this->shift_name),true);
        }
        $criteria->compare('t.is_delete',isset($this->is_delete) ? $this->is_delete : 0);
        $criteria->compare('LOWER(t.create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(t.update_time)',strtolower($this->update_time),true);
        $criteria->compare('t.create_user_id',$this->create_user_id);
        $criteria->compare('t.update_user_id',$this->update_user_id);
        $criteria->compare('t.is_have_user',$this->is_have_user);

        return $criteria;
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearch();
        $criteria->limit=10;

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }


    public function searchPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearch();
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));
    }

    public function findAllByRole()
    {
        //criteria disamakan dengan dokter_v
        $criteria = new CDbCriteria();
        $criteria->join = 'JOIN users ON users.employee_id = t.id';
        $criteria->addCondition("users.role_id=".Params::ROLE_KASIR);
        $criteria->order = 'name';
        return Employee::model()->findAll($criteria);
    }
}