<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $last_login
 * @property integer $is_active
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property integer $role_id
 * @property integer $employee_id
 * @property integer $is_delete
 */
class User extends CActiveRecord
{
    public $password_repeat, $old_password, $new_password, $new_password_repeat, $role_name, $employee_name;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, password, create_time, create_user_id, role_id', 'required'),
            array('is_active, create_user_id, update_user_id, role_id, employee_id, is_delete', 'numerical', 'integerOnly'=>true),
            array('username', 'length', 'max'=>50),
            array('last_login, update_time', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, username, password, last_login, is_active, create_time, update_time, create_user_id, update_user_id, role_id, employee_id, is_delete', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'role'=>array(self::BELONGS_TO,'Role','role_id'),
            'employee'=>array(self::BELONGS_TO,'Employee','employee_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Nama Pengguna',
            'password' => 'Kata Sandi',
            'last_login' => 'Masuk Terakhir',
            'is_active' => 'Status',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'create_user_id' => 'Create User',
            'update_user_id' => 'Update User',
            'role_id' => 'Posisi',
            'employee_id' => 'Pegawai',
            'is_delete' => 'Is Delete',
            'old_password' => 'Kata Sandi Lama',
            'new_password' => 'Kata Sandi Baru',
            'new_password_repeat' => 'Ulangi Kata Sandi Baru',
            'employee_name' => 'Pegawai'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CdbCriteria that can return criterias.
     */
    public function criteriaSearch()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('role','employee');
        $criteria->compare('t.id',$this->id);
        $criteria->compare('LOWER(t.username)',strtolower($this->username),true);
        $criteria->compare('LOWER(t.password)',strtolower($this->password),true);
        $criteria->compare('LOWER(t.last_login)',strtolower($this->last_login),true);
        $criteria->compare('LOWER(t.create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(t.update_time)',strtolower($this->update_time),true);
        $criteria->compare('t.create_user_id',$this->create_user_id);
        $criteria->compare('t.update_user_id',$this->update_user_id);
        $criteria->compare('t.role_id',$this->role_id);
        if(!empty($this->role_name)){
            $criteria->compare('LOWER(role.name)',strtolower($this->role_name),true);
        }
        if(!empty($this->employee_name)){
            $criteria->compare('LOWER(employee.name)',strtolower($this->employee_name),true);
        }
        $criteria->compare('t.employee_id',$this->employee_id);
        $criteria->compare('t.is_delete',isset($this->is_delete) ? $this->is_delete : 0);

        return $criteria;
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearch();
        $criteria->limit=10;

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }


    public function searchPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=$this->criteriaSearch();
        $criteria->limit=-1;

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
    }

    protected function afterValidate() {
        parent::afterValidate();
        if(($this->getScenario() == 'insert'))
        {
            $this->password = $this->encrypt($this->password);

        }else if($this->getScenario() == 'changePassword') {
            $this->password = $this->encrypt($this->password);
        }
    }

    public function encrypt($value) {
        return md5($value);
    }
}