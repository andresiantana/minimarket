function date_time()
{
    date = new Date;
    var year = date.getFullYear();
    var month = date.getMonth();
    var months = new Array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agus', 'Sep', 'Okt', 'Nop', 'Des');
    var d = date.getDate();
    var day = date.getDay();
    var days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
    var h = date.getHours();
    if(h<10)
    {
        h = "0"+h;
    }
    var m = date.getMinutes();
    if(m<10)
    {
        m = "0"+m;
    }
    var s = date.getSeconds();
    if(s<10)
    {
        s = "0"+s;
    }
    var date = ""+d+"";
    if(date.length === 1)
        d = '0'+d;
    var result = d+' '+months[month]+' '+year+' '+h+':'+m+':'+s;
    $("input[class*='realtime']").val(result);
    $("div[class*='realtime']").html(result);
    return true;
}
