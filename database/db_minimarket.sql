-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2017 at 11:21 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_minimarket`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `is_delete` tinyint(1) DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `is_delete`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 'Makanan Ringan', 0, '2016-12-05 07:23:32', NULL, 4, NULL),
(2, 'Obat-Obatan', 1, '2016-12-05 11:50:23', NULL, 4, NULL),
(3, 'Sembako', 0, '2016-12-05 11:51:22', NULL, 4, NULL),
(4, 'Makanan Berat', 0, '2016-12-05 11:53:33', NULL, 4, NULL),
(5, 'Permen', 1, '2016-12-05 12:06:35', NULL, 4, NULL),
(6, 'Minuman', 0, '2017-04-26 17:05:16', NULL, 13, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `code` varchar(30) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `handphone` varchar(22) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `is_delete` tinyint(1) DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `is_have_user` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `code`, `name`, `address`, `handphone`, `shift_id`, `is_delete`, `create_time`, `update_time`, `create_user_id`, `update_user_id`, `is_have_user`) VALUES
(7, '20170102', 'Ana', 'Jl. Trunojoyo No. 108', '0897898989', 1, 0, '2016-12-11 17:38:50', NULL, 1, NULL, 1),
(8, '20170401', 'Budi', 'Jl. Sultan Agung No. 1', '087898878888', 2, 0, '2016-12-11 17:39:20', NULL, 1, NULL, 1),
(9, '20170402', 'Anton', 'Jl. Tirtayasa No. 10', '08989898989', 2, 0, '2016-12-11 17:39:41', NULL, 1, NULL, 1),
(10, '20170403', 'Bella', 'Jl. Gunung Agung No. 50', '08789898909', 1, 0, '2016-12-11 17:40:04', NULL, 1, NULL, 1),
(12, '20170101', 'Ronny', 'JL. ABC', '798080', 1, 0, '2017-01-09 16:17:03', NULL, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `number` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` text,
  `phone_handphone` varchar(30) DEFAULT NULL,
  `in_point` double DEFAULT '0',
  `out_point` double DEFAULT '0',
  `current_point` double DEFAULT '0',
  `is_delete` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `date`, `number`, `name`, `address`, `phone_handphone`, `in_point`, `out_point`, `current_point`, `is_delete`) VALUES
(1, '2017-06-26', '201706260001', 'Andre', 'Jl. ABCD', 'Andre', 2, 0, 2, 0),
(2, '2017-06-27', '201706270001', 'Andre S', 'Jl. ABC', '0228787878', 34, 7, 27, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `number` varchar(30) NOT NULL,
  `total_price` double NOT NULL DEFAULT '0',
  `total_discount` double NOT NULL DEFAULT '0',
  `sub_total` double NOT NULL DEFAULT '0',
  `employee_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders_details`
--

CREATE TABLE `orders_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `product_barcode` varchar(50) DEFAULT NULL,
  `product_unit` varchar(50) DEFAULT NULL,
  `quantity` double NOT NULL DEFAULT '0',
  `prices_order` double NOT NULL DEFAULT '0',
  `discount` double NOT NULL DEFAULT '0',
  `total_discount` double NOT NULL DEFAULT '0',
  `sub_total` double NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `presences`
--

CREATE TABLE `presences` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `in_hours` datetime DEFAULT NULL,
  `out_hours` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presences`
--

INSERT INTO `presences` (`id`, `employee_id`, `in_hours`, `out_hours`) VALUES
(5, 7, '2017-04-24 04:38:02', '2017-04-24 10:36:49'),
(6, 8, '2017-04-24 10:35:41', '2017-04-24 10:36:55'),
(7, 9, '2017-04-24 10:35:47', '2017-04-24 10:37:02'),
(8, 10, '2017-04-24 10:35:55', '2017-04-24 10:37:08'),
(11, 7, '2017-06-28 11:18:26', '2017-06-28 11:18:48');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `barcode` text NOT NULL,
  `unit` varchar(20) NOT NULL,
  `expire_date` date NOT NULL,
  `stock` double NOT NULL DEFAULT '0',
  `min_stock` double DEFAULT '0',
  `price_sale` double NOT NULL DEFAULT '0',
  `discount` double DEFAULT '0',
  `is_delete` tinyint(1) DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `barcode`, `unit`, `expire_date`, `stock`, `min_stock`, `price_sale`, `discount`, `is_delete`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 1, 'Beng-Beng ', '8886001038011', 'Pcs', '2017-07-01', 71, 20, 2000, 5, 0, '2016-12-05 07:25:11', '2016-12-05 19:30:03', 4, 4),
(2, 1, 'Tango Vanilla Milk WAFER 8g', '8991102385114', 'Pcs', '2017-07-06', 91, 20, 500, 10, 0, '2016-12-05 07:26:19', '2016-12-05 19:30:32', 4, 4),
(3, 1, 'Richoco Nabati Chocolate Wafer 8g', '8993175532891', 'Pcs', '2017-04-19', -99, 20, 500, NULL, 0, '2016-12-05 07:27:41', '2016-12-09 06:45:44', 4, 7),
(4, 1, 'Tango Chocolate WAFER 8g', '8991102385084', 'Pcs', '2017-06-29', 121, 20, 500, 0, 0, '2016-12-05 07:28:59', NULL, 4, NULL),
(5, 1, 'Richeese Nabati Cheese Wafer 8g', '8993175531863', 'Pcs', '2017-01-31', -150, 20, 500, 10, 0, '2016-12-05 07:30:04', '2017-04-24 17:49:47', 4, 11),
(6, 1, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', '2017-04-17', 29, 20, 1200, 0, 0, '2016-12-05 07:31:18', '2016-12-31 11:40:47', 4, 13),
(7, 1, 'GO! Egg Roll Wafer Rolls 10g', '8994834000553', 'Pcs', '2017-12-31', 99, 20, 500, 0, 0, '2016-12-05 07:32:30', NULL, 4, NULL),
(8, 1, 'Richeese Rolls 8g', '8993175532419', 'Pcs', '2017-04-12', -167, 20, 500, 0, 0, '2016-12-05 07:34:03', NULL, 4, NULL),
(9, 1, 'Fullo Wafer Roll full of Vanilla Milk 9.5g', '8991102381017', 'Pcs', '2017-07-07', -170, 20, 500, 0, 0, '2016-12-05 07:35:28', NULL, 4, NULL),
(10, 1, 'Deka WAFER ROLL Choco Nut 8.5g', '8995077603310', 'Pcs', '2017-08-17', 70, 20, 500, 0, 0, '2016-12-05 07:36:56', NULL, 4, NULL),
(11, 1, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', '2017-01-04', -28, 39, 500, 5, 0, '2016-12-05 07:38:35', '2016-12-31 14:46:09', 4, 13),
(12, 1, 'Chocolatos CHOCOLATE DRINK 28g', '8992775913000', 'Pcs', '2017-11-11', 101, 20, 2000, 0, 0, '2016-12-05 07:39:49', NULL, 4, NULL),
(13, 5, 'Alba Pastilles 100gr', '8997004100015', 'Pcs', '2018-12-01', 100, 20, 7800, 0, 1, '2016-12-05 12:08:49', NULL, 4, NULL),
(14, 4, 'Makanan Artline', '4974052800986', 'Pcs', '2018-04-01', 123, 1, 10000, 5, 0, '2017-04-14 09:26:15', '2017-04-16 12:41:12', 11, 13),
(15, 4, 'Makanan Tipe-X', '8998838050002', 'Pcs', '2018-04-01', 110, 1, 5000, 5, 0, '2017-04-14 09:27:43', '2017-04-16 12:44:56', 11, 13),
(16, 4, 'Makanan Stabilo', '9556091141623', 'Pcs', '2018-04-07', 100, 1, 6000, 0, 0, '2017-04-16 09:37:31', NULL, 11, NULL),
(17, 6, 'Nescafe Kotak 200ml', '8992696425316', 'pack', '2017-11-28', 77, 10, 5000, 0, 0, '2017-04-26 17:06:52', NULL, 13, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `role_name` varchar(20) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `role_name`, `is_delete`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 'administrator', 'Administrator', 0, '0000-00-00 00:00:00', NULL, 0, NULL),
(2, 'owner', 'Owner', 0, '0000-00-00 00:00:00', NULL, 0, NULL),
(3, 'gudang', 'Gudang', 0, '0000-00-00 00:00:00', NULL, 0, NULL),
(4, 'kasir', 'Kasir', 0, '0000-00-00 00:00:00', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `number` varchar(30) NOT NULL,
  `total_price` double NOT NULL DEFAULT '0',
  `total_discount` double NOT NULL DEFAULT '0',
  `total_discount_point` double DEFAULT '0',
  `sub_total` double NOT NULL DEFAULT '0',
  `money_cash` double NOT NULL DEFAULT '0',
  `money_back` double NOT NULL DEFAULT '0',
  `total_item` double NOT NULL DEFAULT '0',
  `employee_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `date`, `number`, `total_price`, `total_discount`, `total_discount_point`, `sub_total`, `money_cash`, `money_back`, `total_item`, `employee_id`, `role_id`, `shift_id`, `member_id`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, '2017-04-24 17:36:01', 'CHM1704240001', 3500, 50, 0, 3450, 3450, 0, 0, 7, 4, 1, NULL, '2017-04-24 12:36:02', NULL, 10, NULL),
(2, '2017-04-24 18:40:17', 'CHM1704240002', 51000, 250, 0, 50750, 50750, 0, 0, 7, 4, 1, NULL, '2017-04-24 13:40:18', NULL, 10, NULL),
(3, '2017-04-24 19:18:49', 'CHM1704240003', 15000, 250, 0, 14750, 14750, 0, 0, 7, 4, 1, NULL, '2017-04-24 14:18:50', NULL, 10, NULL),
(4, '2017-04-24 22:37:16', 'CHM1704240004', 1500, 75, 0, 1425, 2000, 575, 0, 7, 4, 1, NULL, '2017-04-24 17:37:17', NULL, 10, NULL),
(5, '2017-04-24 22:39:34', 'CHM1704240005', 50000, 2500, 0, 47500, 50000, 2500, 0, 7, 4, 1, NULL, '2017-04-24 17:39:35', NULL, 10, NULL),
(6, '2017-04-26 21:56:17', 'CHM1704260001', 10000, 500, 0, 9500, 9500, 0, 0, 7, 4, 1, NULL, '2017-04-26 16:56:19', NULL, 10, NULL),
(7, '2017-04-26 22:07:31', 'CHM1704260002', 5000, 0, 0, 5000, 5000, 0, 0, 7, 4, 1, NULL, '2017-04-26 17:07:32', NULL, 10, NULL),
(8, '2017-04-26 22:09:43', 'CHM1704260003', 15000, 500, 0, 14500, 14500, 0, 0, 7, 4, 1, NULL, '2017-04-26 17:09:44', NULL, 10, NULL),
(9, '2017-05-06 11:16:48', 'CHM1705060001', 1500, 75, 0, 1425, 1425, 0, 0, 10, 4, 1, NULL, '2017-05-06 11:17:12', NULL, 12, NULL),
(10, '2017-05-06 11:17:17', 'CHM1705060002', 500, 25, 0, 475, 475, 0, 0, 10, 4, 1, NULL, '2017-05-06 11:17:39', NULL, 12, NULL),
(11, '2017-05-06 11:21:19', 'CHM1705060003', 1500, 100, 0, 1400, 1400, 0, 0, 10, 4, 1, NULL, '2017-05-06 11:21:32', NULL, 12, NULL),
(12, '2017-05-06 17:55:05', 'CHM1705060004', 500, 25, 0, 475, 475, 0, 0, 10, 4, 1, NULL, '2017-05-06 12:55:06', NULL, 12, NULL),
(13, '2017-05-06 17:55:27', 'CHM1705060005', 500, 0, 0, 500, 500, 0, 0, 10, 4, 1, NULL, '2017-05-06 12:55:28', NULL, 12, NULL),
(14, '2017-05-06 18:03:28', 'CHM1705060006', 1000, 75, 0, 925, 925, 0, 0, 10, 4, 1, NULL, '2017-05-06 13:03:28', NULL, 12, NULL),
(15, '2017-05-06 18:03:56', 'CHM1705060007', 500, 25, 0, 475, 475, 0, 0, 10, 4, 1, NULL, '2017-05-06 13:03:57', NULL, 12, NULL),
(16, '2017-05-06 18:04:56', 'CHM1705060008', 1000, 50, 0, 950, 950, 0, 0, 10, 4, 1, NULL, '2017-05-06 13:04:57', NULL, 12, NULL),
(17, '2017-06-17 13:31:59', 'CHM1706170001', 300000, 12500, 0, 287500, 287500, 0, 0, 7, 4, 1, NULL, '2017-06-17 08:32:00', NULL, 10, NULL),
(18, '2017-06-16 13:33:54', 'CHM1706160001', 300000, 2500, 0, 297500, 297500, 0, 0, 10, 4, 1, NULL, '2017-06-16 08:33:55', NULL, 12, NULL),
(19, '2017-06-26 06:11:03', 'CHM1706260001', 1200, 0, 0, 1200, 1200, 0, 0, 10, 4, 1, 1, '2017-06-26 01:11:04', NULL, 12, NULL),
(20, '2017-06-26 06:26:26', 'CHM1706260002', 1200, 0, 500, 700, 700, 0, 0, 10, 4, 1, 1, '2017-06-26 01:26:27', NULL, 12, NULL),
(21, '2017-06-27 07:06:25', 'CHM1706270001', 1200, 0, 0, 1200, 1200, 0, 0, 10, 4, 1, NULL, '2017-06-27 02:06:25', NULL, 12, NULL),
(22, '2017-06-27 07:20:55', 'CHM1706270002', 6000, 0, 0, 6000, 6000, 0, 0, 10, 4, 1, 2, '2017-06-27 02:20:56', NULL, 12, NULL),
(23, '2017-06-27 07:21:45', 'CHM1706270003', 6000, 0, 500, 5500, 5500, 0, 0, 10, 4, 1, 2, '2017-06-27 02:21:46', NULL, 12, NULL),
(24, '2017-06-27 07:23:47', 'CHM1706270004', 1200, 0, 500, 700, 700, 0, 0, 10, 4, 1, 2, '2017-06-27 02:23:48', NULL, 12, NULL),
(25, '2017-06-27 07:24:56', 'CHM1706270005', 2000, 100, 500, 1400, 1400, 0, 0, 10, 4, 1, 2, '2017-06-27 02:24:57', NULL, 12, NULL),
(26, '2017-06-27 07:26:28', 'CHM1706270006', 1200, 0, 500, 700, 700, 0, 0, 10, 4, 1, 2, '2017-06-27 02:26:29', NULL, 12, NULL),
(27, '2017-06-27 07:27:10', 'CHM1706270007', 2000, 100, 500, 1400, 1400, 0, 0, 10, 4, 1, 2, '2017-06-27 02:27:11', NULL, 12, NULL),
(28, '2017-06-27 07:27:55', 'CHM1706270008', 2000, 100, 500, 1400, 1400, 0, 0, 10, 4, 1, 2, '2017-06-27 02:27:56', NULL, 12, NULL),
(29, '2017-06-28 15:28:48', 'CHM1706280001', 1200, 0, 0, 1200, 1200, 0, 0, 10, 4, 1, 2, '2017-06-28 10:28:49', NULL, 12, NULL),
(30, '2017-06-28 15:31:52', 'CHM1706280002', 1200, 0, 0, 1200, 1200, 0, 0, 10, 4, 1, NULL, '2017-06-28 10:31:53', NULL, 12, NULL),
(31, '2017-06-28 15:44:23', 'CHM1706280003', 12000, 0, 500, 11500, 11500, 0, 0, 10, 4, 1, 2, '2017-06-28 10:44:23', NULL, 12, NULL),
(32, '2017-06-28 15:45:34', 'CHM1706280004', 10000, 500, 0, 9500, 9500, 0, 0, 10, 4, 1, NULL, '2017-06-28 10:45:35', NULL, 12, NULL),
(33, '2017-06-28 16:20:37', 'CHM1706280005', 12000, 0, 0, 12000, 12000, 0, 0, 10, 4, 1, NULL, '2017-06-28 11:20:38', NULL, 12, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales_details`
--

CREATE TABLE `sales_details` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `product_barcode` varchar(50) DEFAULT NULL,
  `product_unit` varchar(50) DEFAULT NULL,
  `quantity` double NOT NULL DEFAULT '0',
  `prices_sale` double NOT NULL DEFAULT '0',
  `discount` double NOT NULL DEFAULT '0',
  `total_discount` double NOT NULL DEFAULT '0',
  `sub_total` double NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_details`
--

INSERT INTO `sales_details` (`id`, `sale_id`, `product_id`, `product_name`, `product_barcode`, `product_unit`, `quantity`, `prices_sale`, `discount`, `total_discount`, `sub_total`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 1, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 2, 500, 10, 50, 950, '2017-04-24 12:36:02', NULL, 10, NULL),
(2, 1, 5, 'Richeese Nabati Cheese Wafer 8g', '8993175531863', 'Pcs', 5, 500, 0, 0, 2500, '2017-04-24 12:36:02', NULL, 10, NULL),
(3, 2, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 10, 500, 50, 250, 4750, '2017-04-24 13:40:19', NULL, 10, NULL),
(4, 2, 5, 'Richeese Nabati Cheese Wafer 8g', '8993175531863', 'Pcs', 20, 500, 0, 0, 10000, '2017-04-24 13:40:19', NULL, 10, NULL),
(5, 2, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 30, 1200, 0, 0, 36000, '2017-04-24 13:40:19', NULL, 10, NULL),
(6, 3, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 10, 500, 50, 250, 4750, '2017-04-24 14:18:50', NULL, 10, NULL),
(7, 3, 5, 'Richeese Nabati Cheese Wafer 8g', '8993175531863', 'Pcs', 20, 500, 0, 0, 10000, '2017-04-24 14:18:50', NULL, 10, NULL),
(8, 4, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 3, 500, 15, 75, 1425, '2017-04-24 17:37:17', NULL, 10, NULL),
(9, 5, 14, 'Makanan Artline', '4974052800986', 'Pcs', 5, 10000, 25, 2500, 47500, '2017-04-24 17:39:35', NULL, 10, NULL),
(10, 6, 14, 'Makanan Artline', '4974052800986', 'Pcs', 1, 10000, 5, 500, 9500, '2017-04-26 16:56:19', NULL, 10, NULL),
(11, 7, 17, 'Nescafe Kotak 200ml', '8992696425316', 'pack', 1, 5000, 0, 0, 5000, '2017-04-26 17:07:32', NULL, 10, NULL),
(12, 8, 14, 'Makanan Artline', '4974052800986', 'Pcs', 1, 10000, 5, 500, 9500, '2017-04-26 17:09:44', NULL, 10, NULL),
(13, 8, 17, 'Nescafe Kotak 200ml', '8992696425316', 'pack', 1, 5000, 0, 0, 5000, '2017-04-26 17:09:44', NULL, 10, NULL),
(14, 9, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 3, 500, 15, 75, 1425, '2017-05-06 11:17:13', NULL, 12, NULL),
(15, 10, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 1, 500, 5, 25, 475, '2017-05-06 11:17:39', NULL, 12, NULL),
(16, 11, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 2, 500, 10, 50, 950, '2017-05-06 11:21:33', NULL, 12, NULL),
(17, 11, 5, 'Richeese Nabati Cheese Wafer 8g', '8993175531863', 'Pcs', 1, 500, 10, 50, 450, '2017-05-06 11:21:33', NULL, 12, NULL),
(18, 12, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 1, 500, 5, 25, 475, '2017-05-06 12:55:06', NULL, 12, NULL),
(19, 13, 8, 'Richeese Rolls 8g', '8993175532419', 'Pcs', 1, 500, 0, 0, 500, '2017-05-06 12:55:28', NULL, 12, NULL),
(20, 14, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 1, 500, 5, 25, 475, '2017-05-06 13:03:28', NULL, 12, NULL),
(21, 14, 5, 'Richeese Nabati Cheese Wafer 8g', '8993175531863', 'Pcs', 1, 500, 10, 50, 450, '2017-05-06 13:03:28', NULL, 12, NULL),
(22, 15, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 1, 500, 5, 25, 475, '2017-05-06 13:03:57', NULL, 12, NULL),
(23, 16, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 2, 500, 10, 50, 950, '2017-05-06 13:04:58', NULL, 12, NULL),
(24, 17, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 100, 500, 500, 2500, 47500, '2017-06-17 08:32:00', NULL, 10, NULL),
(25, 17, 5, 'Richeese Nabati Cheese Wafer 8g', '8993175531863', 'Pcs', 200, 500, 2000, 10000, 90000, '2017-06-17 08:32:00', NULL, 10, NULL),
(26, 17, 8, 'Richeese Rolls 8g', '8993175532419', 'Pcs', 300, 500, 0, 0, 150000, '2017-06-17 08:32:00', NULL, 10, NULL),
(27, 18, 11, 'Chocolatos WAFER ROLL 9g', '8992775311608', 'Pcs', 100, 500, 500, 2500, 47500, '2017-06-16 08:33:55', NULL, 12, NULL),
(28, 18, 3, 'Richoco Nabati Chocolate Wafer 8g', '8993175532891', 'Pcs', 200, 500, 0, 0, 100000, '2017-06-16 08:33:55', NULL, 12, NULL),
(29, 18, 9, 'Fullo Wafer Roll full of Vanilla Milk 9.5g', '8991102381017', 'Pcs', 300, 500, 0, 0, 150000, '2017-06-16 08:33:55', NULL, 12, NULL),
(30, 19, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 1, 1200, 0, 0, 1200, '2017-06-26 01:11:04', NULL, 12, NULL),
(31, 20, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 1, 1200, 0, 0, 1200, '2017-06-26 01:26:28', NULL, 12, NULL),
(32, 21, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 1, 1200, 0, 0, 1200, '2017-06-27 02:06:25', NULL, 12, NULL),
(33, 22, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 5, 1200, 0, 0, 6000, '2017-06-27 02:20:56', NULL, 12, NULL),
(34, 23, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 5, 1200, 0, 0, 6000, '2017-06-27 02:21:46', NULL, 12, NULL),
(35, 24, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 1, 1200, 0, 0, 1200, '2017-06-27 02:23:48', NULL, 12, NULL),
(36, 25, 1, 'Beng-Beng ', '8886001038011', 'Pcs', 1, 2000, 5, 100, 1900, '2017-06-27 02:24:57', NULL, 12, NULL),
(37, 26, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 1, 1200, 0, 0, 1200, '2017-06-27 02:26:29', NULL, 12, NULL),
(38, 27, 1, 'Beng-Beng ', '8886001038011', 'Pcs', 1, 2000, 5, 100, 1900, '2017-06-27 02:27:11', NULL, 12, NULL),
(39, 28, 1, 'Beng-Beng ', '8886001038011', 'Pcs', 1, 2000, 5, 100, 1900, '2017-06-27 02:27:56', NULL, 12, NULL),
(40, 29, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 1, 1200, 0, 0, 1200, '2017-06-28 10:28:49', NULL, 12, NULL),
(41, 30, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 1, 1200, 0, 0, 1200, '2017-06-28 10:31:53', NULL, 12, NULL),
(42, 31, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 10, 1200, 0, 0, 12000, '2017-06-28 10:44:23', NULL, 12, NULL),
(43, 32, 1, 'Beng-Beng ', '8886001038011', 'Pcs', 5, 2000, 25, 500, 9500, '2017-06-28 10:45:35', NULL, 12, NULL),
(44, 33, 6, 'Cheetos Jagung Bakar 15g', '089686600223', 'Pcs', 10, 1200, 0, 0, 12000, '2017-06-28 11:20:38', NULL, 12, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  `start_hours` time NOT NULL,
  `end_hours` time NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`id`, `name`, `start_hours`, `end_hours`, `is_delete`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 'Pagi', '08:00:00', '14:00:00', 0, '2016-12-04 11:17:24', '2016-12-04 13:31:10', 1, 1),
(2, 'Siang', '14:00:00', '21:00:00', 0, '2016-12-04 13:31:34', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `system_configurations`
--

CREATE TABLE `system_configurations` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` text,
  `phone` varchar(22) DEFAULT NULL,
  `is_absensi` tinyint(1) DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `is_member` tinyint(1) DEFAULT '0',
  `point_to_idr` double DEFAULT '0',
  `value_point` double DEFAULT '0',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_configurations`
--

INSERT INTO `system_configurations` (`id`, `name`, `address`, `phone`, `is_absensi`, `is_active`, `is_delete`, `is_member`, `point_to_idr`, `value_point`, `create_time`, `update_time`) VALUES
(1, 'CAHAYA MUDA TOSERBA', 'Jl. Raya Majalangu Watukumpul, Pemalang', '082322965854', 1, 1, 0, 1, 50000, 1, '2017-05-06 09:36:07', '2017-06-28 10:27:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `last_login`, `is_active`, `create_time`, `update_time`, `create_user_id`, `update_user_id`, `role_id`, `employee_id`, `is_delete`) VALUES
(1, 'administrator', 'b5470d093aa1ae5c690eca38686bd4bc', '2017-06-28 10:57:24', 1, '2016-11-25 00:00:00', '2016-12-04 11:46:23', 1, 1, 1, NULL, 0),
(10, 'ana', '276b6c4692e78d4799c12ada515bc3e4', '2017-06-17 08:31:43', 1, '2016-12-11 17:40:20', '2017-06-17 08:27:22', 1, 1, 4, 7, 0),
(11, 'budi', '00dfc53ee86af02e742515cdcf075ed3', '2017-06-28 10:32:42', 1, '2016-12-11 17:40:31', NULL, 1, NULL, 3, 8, 0),
(12, 'bella', 'e7e9ec3723447a642f762b2b6a15cfd7', '2017-06-28 11:20:03', 1, '2016-12-11 17:40:51', '2017-06-17 08:29:31', 1, 1, 4, 10, 0),
(13, 'anton', '784742a66a3a0c271feced5b149ff8db', '2017-04-26 17:10:54', 1, '2016-12-11 17:41:05', NULL, 1, NULL, 3, 9, 0),
(14, 'owner', '72122ce96bfec66e2396d2e25225d70a', '2017-06-28 10:55:08', 1, '2016-12-11 17:41:16', NULL, 1, NULL, 2, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_details`
--
ALTER TABLE `orders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presences`
--
ALTER TABLE `presences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_details`
--
ALTER TABLE `sales_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_configurations`
--
ALTER TABLE `system_configurations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders_details`
--
ALTER TABLE `orders_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `presences`
--
ALTER TABLE `presences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `sales_details`
--
ALTER TABLE `sales_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `system_configurations`
--
ALTER TABLE `system_configurations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
